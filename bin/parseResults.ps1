'Set-ExecutionPolicy RemoteSigned'

$timeZone = Get-WmiObject -Class win32_TimeZone | Format-List [a-z]* | findStr "Caption"
$colon = $timeZone[0].indexOf(":")
$firstRightBracket = $timeZone[0].indexOf(")")
$remainder = $timeZone[0].Substring($firstRightBracket+1).trim(" ")	
$secondRightBracket = $remainder.indexOf(")")
$requiredRightBracket = $firstRightBracket + $secondRightBracket 	
$timeZone = $timeZone[0].Substring($colon+2, $requiredRightBracket+1 - $colon)
write-host "Time Zone: " $timeZone

$currentTime = Get-Date -format F



cd $args[0]

$strFileName = "resultsOutput.properties"
If (Test-Path $strFileName){
	Remove-Item $strFileName
}

$strFileName = ".\target\surefire-reports\TestSuite.txt"
If (Test-Path $strFileName){
	$data = Get-Content $strFileName
	If ($data[3].contains("Tests run: ")) {
		$colon = $data[3].indexOf(":")
		$comma = $data[3].indexOf(",")	
		$difference = $comma - $colon
		$strTotalRunResults = $data[3].Substring($colon+1, $difference-1)
		$intTotalRunResults = [int]$strTotalRunResults	
		$remainder = $data[3].Substring($comma+1).trim(" ")	
	}
	write-host "Total Run Results: " $intTotalRunResults
	
	If ($remainder.contains("Failures: ")) {
		$colon = $remainder.indexOf(":")
		$comma = $remainder.indexOf(",")	
		$difference = $comma - $colon
		$strFailureResults = $remainder.Substring($colon+1, $difference-1)
		$intFailureResults = [int]$strFailureResults	
		$remainder = $remainder.Substring($comma+1).trim(" ")		
	}
	write-host "Failure Results: " $intFailureResults
	
	If ($remainder.contains("Errors: ")) {
		$colon = $remainder.indexOf(":")
		$comma = $remainder.indexOf(",")	
		$difference = $comma - $colon
		$strErrorsResults = $remainder.Substring($colon+1, $difference-1)
		$intErrorResults = [int]$strErrorResults	
		$remainder = $remainder.Substring($comma+1).trim(" ")		
	}
	write-host "Errors Results: " $intErrorResults
	
	If ($remainder.contains("Skipped: ")) {
		$colon = $remainder.indexOf(":")
		$comma = $remainder.indexOf(",")	
		$difference = $comma - $colon
		$strSkippedResults = $remainder.Substring($colon+1, $difference-1)	
		$intSkippedResults = [int]$strSkippedResults	
		$remainder = $remainder.Substring($comma+1).trim(" ")		
	}
	write-host "Skipped Results: " $intSkippedResults
	
	If ($remainder.contains("Time elapsed: ")) {
		$colon = $remainder.indexOf(":")
		If ($remainder.contains("<")) {
			$angledBracket = $remainder.indexOf("<")	
			$difference = $angledBracket - $comma	
			$strTimeElapsedResults = $remainder.Substring($colon+1, $difference-2)		
		}
		Else {
			$strTimeElapsedResults = $remainder.Substring($colon+1)		
		}	
		$remainder = $remainder.Substring($comma+1).trim(" ")		
	}
	write-host "Time elapsed Results: " $strTimeElapsedResults.trim(" ")
	
	$intPassedResults = $intTotalRunResults - $intFailureResults - $intErrorsResults - $intSkippedResults
	write-host "Passed Results: " $intPassedResults
	
	If ($remainder) {
	If ($remainder.contains("<<<")) {
		$angledBracket = $remainder.indexOf("<")	
		$offset = $angledBracket + 3	
		$statusResults = $remainder.Substring($offset)		
	write-host "Status Results: " $statusResults.trim(" ")	
	"#Taken from http://en.wikipedia.org/wiki/.properties"+"`n"+"TotalRun="+$strTotalRunResults.trim(" ")+"`n"+"Failures="+$strFailureResults.trim(" ")+"`n"+"Errors="+$strErrorsResults.trim(" ")+"`n"+"Skipped="+$strSkippedResults.trim(" ")+"`n"+"TimeElapsed="+$strTimeElapsedResults.trim(" ")+"`n"+"Status="+$statusResults.trim(" ") | Out-File resultsOutput.properties -Encoding ASCII -append -width 200
	}
	"#Taken from http://en.wikipedia.org/wiki/.properties"+"`n"+"TotalRun="+$strTotalRunResults.trim(" ")+"`n"+"Failures="+$strFailureResults.trim(" ")+"`n"+"Errors="+$strErrorsResults.trim(" ")+"`n"+"Skipped="+$strSkippedResults.trim(" ")+"`n"+"TimeElapsed="+$strTimeElapsedResults.trim(" ")+"`n"+"Passed="+$intPassedResults+"`n"+"CurrentTime="+$currentTime+"`n"+"TimeZone="+$timeZone | Out-File resultsOutput.properties -Encoding ASCII -append -width 200
	}
	
	sleep 2
	write-host "Sleep for 2 seconds to allow deployment of properties file"
}

$exit