package functionalTest;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
//import org.testng.asserts.SoftAssert;

/*import dataSource.ConfigReader;
import dataSource.Xls_Reader;
import page.GoToTop;
import page.HeaderNew;
import page.HomePage;
import page.Mining;
import page.Search;
import page.ProductEchoSystem;
import tools.Driver;*/

public class ExamplePageTest {/*
	
	public static String PlatformURL = "http://www-pre.amer1.sdl.schneider-electric.com/us/en/tests/se-example-pages/";
	public static String AtHomeURL= PlatformURL+"home/", 
							WorkURL = PlatformURL+"work/",
							PartnerURL=PlatformURL+"partner/",
							CorporateURL = PlatformURL+"corporate/";
	public static final String linkXpath = "//*[@class='main']//*[@id='templates-list']//a";
	
	public static WebDriver driver;
	public static String browser;
	public static String timeout;
//	public static String title;
	public static int priority =2;
//	public static String countryName;
	protected static Hashtable<String, String> Output = new Hashtable<String, String>();
	
	
	@Parameters({"test_platform", "environment"})
	@BeforeSuite
	public static void initialize(@Optional String url, @Optional String env){
		if(url!=null && url.contains("http://")){
			PlatformURL=url;
			AtHomeURL= PlatformURL+"home/";
			WorkURL = PlatformURL+"work/";
			PartnerURL=PlatformURL+"partner/";
			CorporateURL = PlatformURL+"corporate/";
		}
		if(env!=null)
			Driver.environment = env.toUpperCase();
		else
			Driver.environment= HomePage.getPageDomain(PlatformURL).toUpperCase();
		browser = ConfigReader.getPropValues("browser");
		timeout = ConfigReader.getPropValues("timeout");
		driver = Driver.initialize(driver, browser, timeout);
		Output.put("PASSED", " ");
		Output.put("FAILED", " ");
		Driver.OutputStream(":::::::::::::::::::::::::::::::::::::::: START TESTING ::::::::::::::::::::::::::::::::");
		Driver.OutputStream(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		Driver.OutputStream(":::::::::::::::::::::::::::::::::::: Starting "+ browser.toUpperCase() +" Browser ::::::::::::::::::::::::::::::::");
//		Driver.OutputStream("Environment : "+Driver.environment+". baseline URL: ");
	}
	
	@Test(dataProvider="AtHomeData", description = "test AtHome links")
	public static void AtHomeTest(String name, String URL, String countryName){
		if((name==null)||(URL==null)||(countryName==null)){
			Driver.OutputStream("Provided data for atHome is not valid. Skip Testing");
			return;
		}
		boolean status = true;
		startTest(name+" from AtHome");
		driver.get(URL);
		if(Driver.waitToLoad(driver,20, "Schneider")){
			Driver.wait(500);
			HomePage.closeVideo(driver);
			HomePage.closechat(driver);
			Driver.compairImage(driver, "AtHome"+name+"-baseLine");
		}else
			Driver.OutputStream("Page Load Timeout: "+name+" from AtWork", false);
		storeResult(status, name+"AtHome", driver);
		Assert.assertTrue(status, name+"AtHome test PASSED");
	}
	
	@Test(dataProvider="AtWorkData", description = "test AtWork links")
	public static void AtWorkTest(String name, String URL, String countryName){
		if((name==null)||(URL==null)||(countryName==null)){
			Driver.OutputStream("Provided data for atWork is not valid. Skip Testing");
			return;
		}
		boolean status = true;
		startTest(name+" from AtWork");
		driver.get(URL);
		if(Driver.waitToLoad(driver, 20,"Schneider")){
			Driver.wait(500);
			HomePage.closeVideo(driver);
			HomePage.closechat(driver);
			Driver.compairImage(driver, "AtWork"+name+"-baseLine");
		}else
			Driver.OutputStream("Page Load Timeout: "+name+" from AtWork", false);
		storeResult(status, name+"AtWork", driver);
		Assert.assertTrue(status, name+"AtWork test Passed");
	}
	
	@Test(dataProvider="AtPartnerData", description = "test AtWork links")
	public static void AtPartnerTest(String name, String URL, String countryName){
		if((name==null)||(URL==null)||(countryName==null)){
			Driver.OutputStream("Provided data for atPartner is not valid. Skip Testing");
			return;
		}
		boolean status = true;
		startTest(name+" from AtPartner");
		driver.get(URL);
		if(Driver.waitToLoad(driver, 20, "Schneider")){
			Driver.wait(500);
			HomePage.closeVideo(driver);
			HomePage.closechat(driver);
			Driver.compairImage(driver, "AtPartner"+name+"-baseLine");
		}else
			Driver.OutputStream("Page Load Timeout: "+name+" from AtPartner", false);
		storeResult(status, name+"AtPartner", driver);
		Assert.assertTrue(status, name+"AtPartner test PASSED");
	}
	
	@Test(dataProvider="AtCorporateData", description = "test Corporate links")
	public static void AtCorporateTest(String name, String URL, String countryName){
		if((name==null)||(URL==null)||(countryName==null)){
			Driver.OutputStream("Provided data for atPartner is not valid. Skip Testing");
			return;
		}
		boolean status = false;
		startTest(name+" from AtCorporate");
		driver.get(URL);
		if(Driver.waitToLoad(driver, 20, "Schneider")){
			Driver.wait(100);
			HomePage.closeVideo(driver);
			HomePage.closechat(driver);
			status = HomePage.priority1Test(driver, countryName, 2);			
		}else
			Driver.OutputStream("Page Load Timeout: "+name+" from AtPartner", false);
		storeResult(status, name+"AtCorporate", driver);
		Assert.assertTrue(status, name+"AtCorporate test PASSED");
	}
	
	@AfterSuite(alwaysRun=true)
	public static void close(){
		Driver.OutputStream("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		Driver.OutputStream(":::END: HEADER FOOTER TEST FOR WEBMACHINE:::::::::::::::::");
		try{
			Driver.close(driver);
			Driver.OutputStream( "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			Driver.OutputStream( "::::::::::::::::::::: BROWSER "+browser.toUpperCase()+" CLOSED :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			Driver.OutputStream( "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			summariseTest();
			Driver.OutputStream(".........................................................................................................");
		}
		catch(Exception ex){
			Driver.OutputStream("Closing failed!!!!!");
		}			
	}

	public static void summariseTest(){
		Driver.OutputStream("");
		if(Output.get("FAILED").length()>5)
			Driver.OutputStream("Faild test cases are: "+ Output.get("FAILED"));
		else Driver.OutputStream("All test case PASSED");
		Driver.OutputStream(".............................................................................................................");
		Driver.OutputStream("Passed test cases are: "+ Output.get("PASSED"));
		if(Driver.ErrorMassage.length()>5)
			Driver.OutputStream("Error Massage:"+"\n"+Driver.ErrorMassage);
		else Driver.OutputStream("There is no error massage.");
	}
	
	private static void storeResult(boolean status, String name, WebDriver driver){
		String output;
		if(status){
			output = "PASSED";
		}else{
			output = "FAILED";
			Driver.takeScreenShot(driver, "TestFailed:"+name);
		}
		Output.put(output, Output.get(output)+" "+name+", ");
		Driver.OutputStream("------------------------------------------------------------------------------------------------------------");
		Driver.OutputStream("-------------------------------------- "+name+" Test End-----------------------------------------------------");
		Driver.OutputStream("--------------------------------------- Test "+ output+ " ----------------------------------------------------");
		Driver.OutputStream("--------------------------------------------------------------------------------------------------------------");
	}

	private static void startTest(String name){
		Driver.OutputStream("----------------------------------------------------------------------------------------------------------");
		Driver.OutputStream("-------------------------------------- Starting Test "+name+"---------------------------------------------");
		Driver.OutputStream("----------------------------------------------------------------------------------------------------------");
	 }
	
	@DataProvider(name= "AtHomeData")
	public static Object[][] atHomeDataProvider(){
		driver.get(AtHomeURL);
		Driver.waitToLoad(driver, "Schneider");
		List<WebElement> links = Driver.findElements(driver, "Xpath", linkXpath);
		int noOfLinks=0;
		if(links.size()>0){
			noOfLinks = links.size();
			Driver.OutputStream("There is "+noOfLinks+" links in homepage templates.");
		}else{
			Driver.OutputStream("There is no links visible in AtHomeTemplates page.");
			Driver.takeScreenShot(driver, "HomeDataNotAvailable");
			return null;
		}
		String countryName = HomePage.getCountryName(driver);
		Object[][] data = new Object[noOfLinks][3];
		for(int linkNo = 0; linkNo<noOfLinks; linkNo++){
			data[linkNo][0]= links.get(linkNo).getText();
			data[linkNo][1] = links.get(linkNo).getAttribute("href");
			data[linkNo][2] = countryName;
		}
		return data;
	}
	
	@DataProvider(name= "AtWorkData")
	public static Object[][] atWorkDataProvider(){
		driver.get(WorkURL);
		Driver.waitToLoad(driver, "Schneider");
		List<WebElement> links = Driver.findElements(driver, "Xpath", linkXpath);
		int noOfLinks=0;
		if(links.size()>0){
			noOfLinks = links.size();
			Driver.OutputStream("There is "+noOfLinks+" links in AtWork link templates.");
		}else{
			Driver.OutputStream("There is no links visible in AtWork link page.");
			Driver.takeScreenShot(driver, "WorkDataNotAvailable");
			return null;
		}
		String countryName = HomePage.getCountryName(driver);
		Object[][] data = new Object[noOfLinks][3];
		for(int linkNo = 0; linkNo<noOfLinks; linkNo++){
			data[linkNo][0]= links.get(linkNo).getText();
			data[linkNo][1] = links.get(linkNo).getAttribute("href");
			data[linkNo][2] = countryName;
		}
		return data;
	}
	
	@DataProvider(name= "AtPartnerData")
	public static Object[][] atPartnerDataProvider(){
		driver.get(PartnerURL);
		Driver.waitToLoad(driver, "Schneider");
		List<WebElement> links = Driver.findElements(driver, "Xpath", linkXpath);
		int noOfLinks=0;
		if(links.size()>0){
			noOfLinks = links.size();
			Driver.OutputStream("There is "+noOfLinks+" links in AtPartner Page templates.");
		}else{
			Driver.OutputStream("There is no links visible in AtPartner Templates page.");
			Driver.takeScreenShot(driver, "PartnerDataNotAvailable");
			return null;
		}
		String countryName = HomePage.getCountryName(driver);
		Object[][] data = new Object[noOfLinks][3];
		for(int linkNo = 0; linkNo<noOfLinks; linkNo++){
			data[linkNo][0]= links.get(linkNo).getText();
			data[linkNo][1] = links.get(linkNo).getAttribute("href");
			data[linkNo][2] = countryName;
		}
		return data;
	}
	
	@DataProvider(name= "atCorporateData")
	public static Object[][] atCorporateDataProvider(){
		driver.get(CorporateURL);
		Driver.waitToLoad(driver, "Schneider");
		List<WebElement> links = Driver.findElements(driver, "Xpath", linkXpath);
		int noOfLinks=0;
		if(links.size()>0){
			noOfLinks = links.size();
			Driver.OutputStream("There is "+noOfLinks+" links in AtPartner Page templates.");
		}else{
			Driver.OutputStream("There is no links visible in AtPartner Templates page.");
			Driver.takeScreenShot(driver, "PartnerDataNotAvailable");
			return null;
		}
		String countryName = HomePage.getCountryName(driver);
		Object[][] data = new Object[noOfLinks][3];
		for(int linkNo = 0; linkNo<noOfLinks; linkNo++){
			data[linkNo][0]= links.get(linkNo).getText();
			data[linkNo][1] = links.get(linkNo).getAttribute("href");
			data[linkNo][2] = countryName;
		}
		Driver.OutputStream("No of AtPartner link is: "+noOfLinks+".");
		return data;
	}
	
*/}
