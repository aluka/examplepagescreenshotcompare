package se.com.WebMachine.Desktop.FunctionalTest;

import java.util.Hashtable;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import se.com.WebMachine.dataSourceReader.ConfigReader;
import se.com.WebMachine.dataSourceReader.Xls_Reader;
import se.com.WebMachine.Pages.DispatchContent;
import se.com.WebMachine.Pages.HomePage;
import se.com.WebMachine.Pages.Search;
import se.com.WebMachine.Pages.SiteState;
import se.com.WebMachine.Pages.StickyBar;
import se.com.WebMachine.Tools.Driver;

public class SmokeTest {
	/*This class read all the sdl country list from file in src/test/dataSource/testData.xlx
	 * Goto each link. validate coutry name to make sure its the right link.
	 * validate all the SDL basic components like Megamenu, Coutry Selector,Footer, Email Subscription, Socila media links
	 * SchneiderLOGO, Life is On logo, Green Footer, Cerasel Slider, feature Products, Right rail, Search  etc. 
	 * */

	public static Driver driver;
	public static String browser;
	public static String timeout;
	public static String baseURL;
	public static int priority = 1;
	public static String countryName;
	protected static Hashtable<String, String> Output = new Hashtable<String, String>();
//	public static SoftAssert Assert = new SoftAssert();
	
	@Parameters({"aut_browserType"})
	@BeforeSuite
	public static void initialize(@Optional String Browser){
		if(Browser==null)
			browser = ConfigReader.getPropValue("browser");
		else
			browser = Browser;
		timeout = ConfigReader.getPropValue("timeout");
		driver = new Driver(browser, timeout);
		driver.OutputStream(":::::::::::::::::::::::::::::::::::: Starting "+ browser.toUpperCase() +" Browser ::::::::::::::::::::::::::::::::");
		driver = new Driver(browser, timeout);
		Output.put("PASSED", " ");
		Output.put("FAILED", " ");
	}
	
	@Test(dataProvider="dataProviderRead")
	public static void smokeTest(String name, String identifier, String url, String exception){
		boolean status = true;
		if(exception.length()>0){
			driver.OutputStream("Knows Issue. Skipp testing this country "+name);
			return;
		}
		baseURL = url;
		int priority = 2;
		startTest("Smoke Test"+name);
		driver.OutputStream("Going to start testing for "+name+", Identifier "+identifier+", BaseUrl "+ url);
		try{
			Assert.assertTrue(startCountry(driver, baseURL), "Country initialization TimeOut.");
			status = HomePage.schneiderLogoVisible(driver);
			if(status)
				driver.takeScreenShot(name+"Loaded");
			else
				driver.takeScreenShot(name+"Failed");
			status = status & HomePage.validateCountrySelector(driver, 2);
			status = status & HomePage.countryNameValid(driver, identifier);
			status = status & SiteState.testStatPerameters(driver);
			status = status & HomePage.GreenHeaderTest(driver, priority);
//			status = status & HomePage.searchValid(driver, priority);
			status = status & Search.searchTest(driver, "bac", 2);
//			HomePage.closeSubscription(driver, 10);
			if(name.contains("Denmark"))
				driver.OutputStream(name+" email footer missing. Known Error. Intentional. Will skipp testing footer.");
			else
				status = status & HomePage.validateFooter(driver, priority);
			status = status & HomePage.socialMediaLinks(driver, priority);
			status = status & HomePage.greenFooter(driver, priority);
			status = status & StickyBar.validateStickyBar(driver, priority);
			status = status & driver.readServerError();
			status = status & DispatchContent.validateDispatchPageContents(driver);
//			status = status & AtHomeContent.testAtHome(driver);
//			status = status & AtWork.testAtWork(driver);
//			status = status & Partner.testPartner(driver);
		}catch(Exception e){
			status = false;
			driver.OutputStream("Exception triggered. "+e);
			driver.OutputStream("FAILED------------- Country Name: "+ name+"\n Destination page url is: "+ driver.getCurrentUrl(), false);
			driver.takeScreenShot("FAILED-"+name);
		}
		if(status)
			driver.takeScreenShot( name+"-Passsed");
		else
			driver.takeScreenShot(name+"-Failed");
		driver.manage().deleteAllCookies();
		storeResult(status, name);
		summariseTest();
//		status = true;
		Assert.assertTrue(status, name+" Validation");
	}
	
	@AfterSuite(alwaysRun=true)
	public static void close(){
		driver.OutputStream("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		driver.OutputStream(":::END: HEADER FOOTER TEST FOR WEBMACHINE:::::::::::::::::");
		try{
			driver.close();
			driver.OutputStream( "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			driver.OutputStream( "::::::::::::::::::::: BROWSER "+browser.toUpperCase()+" CLOSED :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			driver.OutputStream( "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			summariseTest();
			driver.OutputStream(".........................................................................................................");
		}
		catch(Exception ex){
			driver.OutputStream("Closing failed!!!!!");
		}	
//		Assert.assertAll();
	}
	
	
	@DataProvider(name= "dataProviderRead")
	public static Object[][] dataProviderReadXLS(){
		int noOfCountries = NumOfCountry();
		String path = Xls_Reader.filename;
		String sheetName = "SDLCountry";
		Xls_Reader file = new Xls_Reader(path);
		Object[][] data = new Object[noOfCountries][4];
		for(int CountryNo = 0; CountryNo<noOfCountries; CountryNo++){
			data[CountryNo][0]= file.getCellData(sheetName, 0, CountryNo+2);
			data[CountryNo][1]= file.getCellData(sheetName, 1, CountryNo+2);
			data[CountryNo][2]= file.getCellData(sheetName, 2, CountryNo+2);
			data[CountryNo][3]= file.getCellData(sheetName, 3, CountryNo+2);
		}
		return data;
	}
	

//	@DataProvider(name= "dataProviderRead")
	public static Object[][] dataProviderReadConfig(){
		Object[][] data = new Object[1][4];
		data[0][0]= ConfigReader.getPropValue("name");
		data[0][1]= ConfigReader.getPropValue("country");
		data[0][2]= ConfigReader.getPropValue("baseURL");
		data[0][3]="";
		return data;
	}

	
	private static int NumOfCountry(){
		int number;
		String path = Xls_Reader.filename;
		String sheetName = "SDLCountry";
		Xls_Reader file = new Xls_Reader(path);
		try{
			number = file.getRowCount(sheetName)-1;
			System.out.println("No of raw is: " +number);
		}catch(Exception e){
			System.out.println("File is not accesable.");
			number = 0;
		}
		return number;
	}
	

	private static boolean startCountry(Driver driver, String baseURL){
		driver.get(baseURL);
		driver.OutputStream(":::::::::::::::::::::::::::::::::::::::: START TESTING ::::::::::::::::::::::::::::::::");
		driver.OutputStream(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		boolean loaded =  driver.waitToLoad(20, "Schneider Electric");
		driver.wait(200);
		driver.waitToLoad();
		return loaded;
	}
	
	private static void summariseTest(){
		driver.OutputStream("");
		if(Output.get("FAILED").length()>5)
			driver.OutputStream("Faild test cases are: "+ Output.get("FAILED"));
		else driver.OutputStream("All test case PASSED");
		driver.OutputStream(".............................................................................................................");
		driver.OutputStream("Passed test cases are: "+ Output.get("PASSED"));
		driver.OutputStream("Priority level is: "+ priority + ". And TestSite is: "+ baseURL);
		if(driver.ErrorMassage.length()>5){
			driver.OutputStream("Error Massage:"+"\n"+driver.ErrorMassage);
			driver.OutputStream("For details reason of failing scroll to the specific country report.");
		}
		else driver.OutputStream("There is no error massage.");
//		driver.ErrorMassage = "";
	}
	
	private static void storeResult(boolean status, String name){
		String output;
		if(status){
			output = "PASSED";
		}else{
			output = "FAILED";
		}
		Output.put(output, Output.get(output)+" "+name+", ");
		driver.OutputStream("------------------------------------------------------------------------------------------------------------");
		driver.OutputStream("-------------------------------------- "+name+" Test End-----------------------------------------------------");
		driver.OutputStream("--------------------------------------- Test "+ output+ " ----------------------------------------------------");
		driver.OutputStream("--------------------------------------------------------------------------------------------------------------");
	}

	private static void startTest(String name){
		driver.OutputStream("----------------------------------------------------------------------------------------------------------");
		driver.OutputStream("-------------------------------------- Starting Test "+name+"---------------------------------------------");
		driver.OutputStream("----------------------------------------------------------------------------------------------------------");
	 }

}
