package se.com.WebMachine.Desktop.FunctionalTest;

import java.util.Hashtable;
import java.util.List;


import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
//import org.testng.asserts.SoftAssert;

import se.com.WebMachine.dataSourceReader.ConfigReader;
import se.com.WebMachine.Pages.GoToTop;
import se.com.WebMachine.Pages.HomePage;
import se.com.WebMachine.Pages.InverstorRelation;
import se.com.WebMachine.Pages.Mining;
import se.com.WebMachine.Pages.Press;
import se.com.WebMachine.Pages.Selector;
import se.com.WebMachine.Pages.Sitemap;
import se.com.WebMachine.Pages.Table;
import se.com.WebMachine.Tools.Driver;

public class ExamplePageTest {
	
	public static String PlatformURL = "http://www-sqe1.schneider-electric.com/us/en/tests/se-example-pages/";
	public static String AtHomeURL= PlatformURL+"home/", 
							WorkURL = PlatformURL+"work/",
							PartnerURL=PlatformURL+"partner/",
							CorporateURL = "corporate/";
	public static final String linkXpath = "//*[@class='main']//*[@id='templates-list']//a";
	
	public static Driver driver;
	public static String browser;
	public static String timeout;
//	public static String title;
	public static int priority =2;
//	public static String countryName;
	protected static Hashtable<String, String> Output = new Hashtable<String, String>();
	
	
	@Parameters({"test_platform"})
	@BeforeSuite
	public static void initialize(@Optional String url){
		if(url!=null && url.contains("http://")){
			PlatformURL=url;
			AtHomeURL= PlatformURL+"home/";
			WorkURL = PlatformURL+"work/";
			PartnerURL=PlatformURL+"partner/";
			CorporateURL = PlatformURL+"/corporate/";
		}
		HomePage.getPageDomain(PlatformURL);
		browser = ConfigReader.getPropValue("browser");
		timeout = ConfigReader.getPropValue("timeout");
		driver = new Driver(browser, timeout);
		Output.put("PASSED", " ");
		Output.put("FAILED", " ");
		driver.OutputStream(":::::::::::::::::::::::::::::::::::::::: START TESTING ::::::::::::::::::::::::::::::::");
		driver.OutputStream(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		driver.OutputStream(":::::::::::::::::::::::::::::::::::: Starting "+ browser.toUpperCase() +" Browser ::::::::::::::::::::::::::::::::");
	}
	
	@Test(dataProvider="AtHomeData", description = "test AtHome links")
	public static void AtHomeTest(String name, String URL, String countryName){
		String nameMathode = new Object(){}.getClass().getEnclosingMethod().getName();
		startTest(nameMathode);
		if((name==null)||(URL==null)||(countryName==null)){
			driver.OutputStream("Provided data for atHome is not valid. Skip Testing");
			return;
		}
		boolean status = false;
		driver.get(URL);
		if(driver.waitToLoad(20, "Schneider")){
			driver.wait(500);
			HomePage.closeVideo(driver);
			HomePage.closechat(driver);
			driver.compairImage("AtHome"+name+"-baseLine");
		}else
			driver.OutputStream("Page Load Timeout: "+name+" from AtWork", false);
		storeResult(status, name+"AtHome");
		Assert.assertTrue(status, name+"AtHome test PASSED");
	}
	
	@Test(dataProvider="AtWorkData", description = "test AtWork links")
	public static void AtWorkTest(String name, String URL, String countryName){
		String nameMathode = new Object(){}.getClass().getEnclosingMethod().getName();
		startTest(nameMathode);
		if((name==null)||(URL==null)||(countryName==null)){
			driver.OutputStream("Provided data for atWork is not valid. Skip Testing");
			return;
		}
		boolean status = true;
		driver.get(URL);
		if(driver.waitToLoad( 20,"Schneider")){			
			driver.wait(500);
			HomePage.closeVideo(driver);
			HomePage.closechat(driver);
			driver.compairImage("AtWork"+name+"-baseLine");
		}
		else
			driver.OutputStream("Page Load Timeout: "+name+" from AtWork", false);
		storeResult(status, name+"AtWork");
		Assert.assertTrue(status, name+"AtWork test Passed");
	}
	
	@Test(dataProvider="AtPartnerData", description = "test Partner links")
	public static void AtPartnerTest(String name, String URL, String countryName){
		String nameMathode = new Object(){}.getClass().getEnclosingMethod().getName();
		startTest(nameMathode);
		if((name==null)||(URL==null)||(countryName==null)){
			driver.OutputStream("Provided data for Corporate is not valid. Skip Testing");
			return;
		}
		boolean status = false;
		driver.get(URL);
		if(driver.waitToLoad( 20, "Schneider")){
			driver.wait(500);
			HomePage.closeVideo(driver);
			HomePage.closechat(driver);
			driver.compairImage("AtPartner"+name+"-baseLine");		
		}else
			driver.OutputStream("Page Load Timeout: "+name+" from AtPartner", false);
		storeResult(status, name+"AtPartner");
		Assert.assertTrue(status, name+"AtPartner test PASSED");
	}
	
	@Test(dataProvider="AtCorporateData", description = "test Corporate links")
	public static void AtCorporateTest(String name, String URL, String countryName){
		String nameMathode = new Object(){}.getClass().getEnclosingMethod().getName();
		startTest(nameMathode);
		if((name==null)||(URL==null)||(countryName==null)){
			driver.OutputStream("Provided data for atPartner is not valid. Skip Testing");
			return;
		}
		boolean status = false;
//		startTest(name+" from AtCorporate");
		driver.get(URL);
		if(driver.waitToLoad( 20, "Schneider")){
			driver.wait(500);
			HomePage.closeVideo(driver);
			HomePage.closechat(driver);
			driver.compairImage("AtCroporate"+name+"-baseLine");
		}else
			driver.OutputStream("Page Load Timeout: "+name+" from AtPartner", false);
		storeResult(status, name+"AtCorporate");
		Assert.assertTrue(status, name+"AtCorporate test PASSED");
	}
	
	@AfterSuite(alwaysRun=true)
	public static void close(){
		driver.OutputStream("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		driver.OutputStream(":::END: HEADER FOOTER TEST FOR WEBMACHINE:::::::::::::::::");
		try{
			driver.close();
			driver.OutputStream( "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			driver.OutputStream( "::::::::::::::::::::: BROWSER "+browser.toUpperCase()+" CLOSED :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			driver.OutputStream( "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			summariseTest();
			driver.OutputStream(".........................................................................................................");
		}
		catch(Exception ex){
			driver.OutputStream("Closing failed!!!!!");
		}	
		
	}

	public static void summariseTest(){
		driver.OutputStream("");
		if(Output.get("FAILED").length()>5)
			driver.OutputStream("Faild test cases are: "+ Output.get("FAILED"));
		else driver.OutputStream("All test case PASSED");
		driver.OutputStream(".............................................................................................................");
		driver.OutputStream("Passed test cases are: "+ Output.get("PASSED"));
		if(driver.ErrorMassage.length()>5)
			driver.OutputStream("Error Massage:"+"\n"+driver.ErrorMassage);
		else driver.OutputStream("There is no error massage.");
//		driver.ErrorMassage = "";
	}
	

	public static void storeResult(boolean status, String name){
		String output;
		if(status){
			output = "PASSED";
		}else{
			output = "FAILED";
		}
		Output.put(output, Output.get(output)+" "+name+", ");
		driver.OutputStream("------------------------------------------------------------------------------------------------------------");
		driver.OutputStream("-------------------------------------- "+name+" Test End-----------------------------------------------------");
		driver.OutputStream("--------------------------------------- Test "+ output+ " ----------------------------------------------------");
		driver.OutputStream("--------------------------------------------------------------------------------------------------------------");
	}
	
	private static void startTest(String name){
//		driver.startTest(name);
		driver.OutputStream("----------------------------------------------------------------------------------------------------------");
		driver.OutputStream("-------------------------------------- Starting Test "+name+"---------------------------------------------");
		driver.OutputStream("----------------------------------------------------------------------------------------------------------");
	 }
	
	@DataProvider(name= "AtHomeData")
	public static Object[][] atHomeDataProvider(){
		driver.get(AtHomeURL);
		List<WebElement> links = driver.findElements( "Xpath", linkXpath);
		int noOfLinks=0;
		if(links.size()>0){
			noOfLinks = links.size();
			driver.OutputStream("There is "+noOfLinks+" links in homepage templates.");
		}else{
			driver.OutputStream("There is no links visible in AtHomeTemplates page.");
			driver.takeScreenShot( "HomeDataNotAvailable");
			return null;
		}
		String countryName = HomePage.getCountryName(driver);
		Object[][] data = new Object[noOfLinks][3];
		for(int linkNo = 0; linkNo<noOfLinks; linkNo++){
			data[linkNo][0]= links.get(linkNo).getText();
			data[linkNo][1] = links.get(linkNo).getAttribute("href");
			data[linkNo][2] = countryName;
		}
		driver.OutputStream("No of AtHome link is: "+noOfLinks+".");
		return data;
	}
	
	@DataProvider(name= "AtWorkData")
	public static Object[][] atWorkDataProvider(){
		driver.get(WorkURL);
		List<WebElement> links = driver.findElements( "Xpath", linkXpath);
		int noOfLinks=0;
		if(links.size()>0){
			noOfLinks = links.size();
			driver.OutputStream("There is "+noOfLinks+" links in AtWork link templates.");
		}else{
			driver.OutputStream("There is no links visible in AtWork link page.");
			driver.takeScreenShot( "WorkDataNotAvailable");
			return null;
		}
		String countryName = HomePage.getCountryName(driver);
		Object[][] data = new Object[noOfLinks][3];
		for(int linkNo = 0; linkNo<noOfLinks; linkNo++){
			data[linkNo][0]= links.get(linkNo).getText();
			data[linkNo][1] = links.get(linkNo).getAttribute("href");
			data[linkNo][2] = countryName;
		}
		driver.OutputStream("No of AtWork link is: "+noOfLinks+".");
		return data;
	}
	
	@DataProvider(name= "AtPartnerData")
	public static Object[][] atPartnerDataProvider(){
		driver.get(PartnerURL);
		List<WebElement> links = driver.findElements( "Xpath", linkXpath);
		int noOfLinks=0;
		if(links.size()>0){
			noOfLinks = links.size();
			driver.OutputStream("There is "+noOfLinks+" links in AtPartner Page templates.");
		}else{
			driver.OutputStream("There is no links visible in AtPartner Templates page.");
			driver.takeScreenShot( "PartnerDataNotAvailable");
			return null;
		}
		String countryName = HomePage.getCountryName(driver);
		Object[][] data = new Object[noOfLinks][3];
		for(int linkNo = 0; linkNo<noOfLinks; linkNo++){
			data[linkNo][0]= links.get(linkNo).getText();
			data[linkNo][1] = links.get(linkNo).getAttribute("href");
			data[linkNo][2] = countryName;
		}
		driver.OutputStream("No of AtPartner link is: "+noOfLinks+".");
		return data;
	}
	
	@DataProvider(name= "atCorporateData")
	public static Object[][] atCorporateDataProvider(){
		driver.get(CorporateURL);
		List<WebElement> links = driver.findElements( "Xpath", linkXpath);
		int noOfLinks=0;
		if(links.size()>0){
			noOfLinks = links.size();
			driver.OutputStream("There is "+noOfLinks+" links in AtPartner Page templates.");
		}else{
			driver.OutputStream("There is no links visible in AtPartner Templates page.");
			driver.takeScreenShot( "PartnerDataNotAvailable");
			return null;
		}
		String countryName = HomePage.getCountryName(driver);
		Object[][] data = new Object[noOfLinks][3];
		for(int linkNo = 0; linkNo<noOfLinks; linkNo++){
			data[linkNo][0]= links.get(linkNo).getText();
			data[linkNo][1] = links.get(linkNo).getAttribute("href");
			data[linkNo][2] = countryName;
		}
		driver.OutputStream("No of AtPartner link is: "+noOfLinks+".");
		return data;
	}
	
}
