package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.Mining;
import se.com.WebMachine.Tools.Driver;

public class MiningTest {

	private static final String baseURL = "http://www-sqe1.schneider-electric.com/us/en/tests/se-example-pages/work/mining-solutions-landing.jsp";

	public static void main(String arg[]) {
		/*
		 * Goto Page click on anchor Check the destination is as expected.
		 * 
		 **/
		Driver driver = new Driver("Chrome", "20");
		driver.get(baseURL);
		if (Mining.miningSolutionTest(driver))
			driver.OutputStream("Test Passed");
		else
			driver.OutputStream("Test Failed");
		driver.close();
	}

}
