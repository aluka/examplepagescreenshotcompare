package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.Partner;
import se.com.WebMachine.Tools.Driver;

public class PartnerTest {

	public static void main(String[] arg){
		String url = "http://www.schneider-electric.us/en/";
		Driver driver= new Driver("Chrome", "20");
		driver.get( url);
		if(Partner.testPartner(driver))driver.OutputStream("Partner Content PASSED");
		else	driver.OutputStream("Partner Content Failed", false);
		driver.close();
	}
}
