package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.HomePage;
import se.com.WebMachine.Pages.Search;
import se.com.WebMachine.Tools.Driver;

public class SearchTest {

	public static String url;
	public static String title;

	public static void main(String[] arg) {
		url = "http://www.schneider-electric.com/us/en/";
		Driver driver = new Driver("Chrome", "20");
		driver.get(url);
		if (Search.codeMove(driver) & Search.searchTest(driver, "bac", 3) & HomePage.validateCountrySelector(driver, 2)
				& HomePage.GreenHeaderTestNew(driver, 2))
			driver.OutputStream("Search PASSED");
		else
			driver.OutputStream("Search Failed", false);
		driver.close();
	}
}
