package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.Press;
import se.com.WebMachine.Tools.Driver;

public class PressTest {

	private static String url = "http://www-prd.corp.sdl.schneider-electric.com/te/en/tests/se-example-pages/corporate/press.jsp";

	public static void main(String[] args) {
		Driver driver = new Driver("Chrome", "20");
		driver.get(url);
		// validatePerspective(driver);
		Press.validatePress(driver);
		driver.close();
	}
}
