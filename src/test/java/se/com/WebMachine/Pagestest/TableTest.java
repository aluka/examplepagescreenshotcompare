package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.Table;
import se.com.WebMachine.Tools.Driver;

public class TableTest {

	public static void main(String[] args) {
		String url = "http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/corporate/analyst-coverage.jsp";
		Driver driver = new Driver("Chrome", "20");
		driver.get(url);
		Table.validateTable(driver);
		driver.close();
		driver.quit();
	}
}
