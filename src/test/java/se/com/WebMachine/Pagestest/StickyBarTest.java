package se.com.WebMachine.Pagestest;


import se.com.WebMachine.Pages.StickyBar;
import se.com.WebMachine.Tools.Driver;

public class StickyBarTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url = "http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/work/dispatch_two_column.jsp";
		Driver driver = new Driver("Chrome", "20");
		driver.get(url);
		driver.wait(1000);
//		Header_Footer_Test.baseURL = url;
//		Header_Footer_Test.countryName = "US/EN";
		StickyBar.validateStickyBar(driver, 2);
		driver.close();
	}
}
