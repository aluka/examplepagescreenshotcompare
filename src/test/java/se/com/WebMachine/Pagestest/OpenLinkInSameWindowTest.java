package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.OpenLinkInSameWindow;
import se.com.WebMachine.Tools.Driver;

public class OpenLinkInSameWindowTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Driver driver = new Driver("Chrome", "20");
		driver.get("http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/work/corporate.jsp");
		if (OpenLinkInSameWindow.validateLinkOpenSameWindow(driver))
			driver.OutputStream("PASSED");
		else
			driver.OutputStream("FAILED");
		driver.close();
	}
}
