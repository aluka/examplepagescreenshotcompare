package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.DispatchContent;
import se.com.WebMachine.Tools.Driver;

public class DispatchContentTest {

	public static void main(String[] arg){
		String url = "http://www.schneider-electric.us/en/";
		Driver driver= new Driver("Chrome", "20");
		driver.get( url);
		if(DispatchContent.validateDispatchPageContents(driver))driver.OutputStream("Dispatch Content PASSED");
		else	driver.OutputStream("Dispatch Content Failed", false);
		driver.close();
	}
}
