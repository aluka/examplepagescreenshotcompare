package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.emailpopin;

public class emailpopintest {
	private static final String URL = "http://www-int1.sdl.apc.com/us/en/";
	private static String[] validEmail = { "divyatest@yopmail.com" };
	private static String[] invalidEmail = { "divyatestinvalidyopmail.com", "divyatestinvalidyopmail.com" };

	public static void main(String arg[]) {
		String url = URL;
		String browserName = "Chrome";
		emailpopin.validateSecondSubmission(url, validEmail[0], invalidEmail[0], browserName);
	}
}
