package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.Selector;
import se.com.WebMachine.Tools.Driver;

public class SelectorTest {

	public static void main(String[] args) {
		Driver driver = new Driver();
		String url = "http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/work/support-router.jsp";
		driver.get(url);
		driver.wait(500);
		if (Selector.validateSelectorKeyword(driver)) {
			driver.OutputStream("PASSED");
		} else
			driver.OutputStream("FAILED");
		driver.close();
	}
}
