package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.AtHomeContent;
import se.com.WebMachine.Tools.Driver;

public class AtHomeContentTest {
	
	public static void main(String[] arg){
		String url = "http://www.schneider-electric.us/en/";
		Driver driver= new Driver("Chrome", "20");
		driver.get( url);
		if(AtHomeContent.testAtHome(driver))driver.OutputStream("At-Home Content PASSED");
		else	driver.OutputStream("At-Home Content Failed", false);
		driver.close();
	}
}
