package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.GoToTop;
import se.com.WebMachine.Tools.Driver;

public class GoToTopTest {

	private static String url = "http://www-prd.corp.sdl.schneider-electric.com/te/en/tests/se-example-pages/work/mining-solutions-application-area.jsp";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Driver driver = new Driver("Chrome", "20");
		driver.get(url);
		GoToTop.goToTopValidation(driver);
		driver.close();
	}

}
