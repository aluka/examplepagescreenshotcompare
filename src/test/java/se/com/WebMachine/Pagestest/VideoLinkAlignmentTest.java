package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.VideoLinkAlignment;
import se.com.WebMachine.Tools.Driver;

public class VideoLinkAlignmentTest {
	private static final String baseURL = "http://www-pre.amer1.sdl.schneider-electric.com/us/en/tests/se-example-pages/work/solutions-casestudies.jsp";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Driver driver = new Driver("Chrome", "20");
		driver.get(baseURL);
		if (VideoLinkAlignment.validateLinkAlignment(driver))
			driver.OutputStream("Test Passed");
		else
			driver.OutputStream("Test Failed");
		driver.close();
	}
}
