package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.Perspective;
import se.com.WebMachine.Tools.Driver;

public class PerspectiveTest {

	public static void main(String[] args) {
		String url = "http://www-int1.schneider-electric.com/us/en/work/insights/index.jsp";
		Driver driver = new Driver("Chrome", "20");
		driver.get(url);
		Perspective.validatePerspective(driver);
		driver.close();
	}
}
