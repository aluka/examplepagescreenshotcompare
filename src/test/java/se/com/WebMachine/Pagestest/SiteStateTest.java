package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.SiteState;
import se.com.WebMachine.Tools.Driver;

public class SiteStateTest {

	public static final String GsiteValue = "GTM-WG7926|+@UA-8189219-8";

	public static void main(String[] args) {
		Driver driver = new Driver("Chrome", "20");
		driver.get("http://www.schneider-electric.com/ww/en/");
		driver.wait(500);
		if (SiteState.testStatPerameters(driver))
			driver.OutputStream("PASSED");
		;
		driver.close();
	}
}