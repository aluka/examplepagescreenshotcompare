package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.Sitemap;
import se.com.WebMachine.Tools.Driver;

public class SitemapTest {

	public static void main(String[] args) {
		Driver driver = new Driver("Chrome", "20");
		// GOTO The Link
		driver.get("http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/work/sitemap.jsp");
		if (Sitemap.validateSiteMapContent(driver))
			driver.OutputStream("SiteMap Content Test Passed.");
		else
			driver.OutputStream("SiteMap Content Test Failed.");
		driver.wait(500);
		driver.close();
	}
}
