package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.MultipleEmailSubmitFooter;
import se.com.WebMachine.Tools.Driver;

public class MultipleEmailSubmitFooterTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Driver driver = new Driver("Chrome", "20");
		driver.get(
				"http://www-pre.amer1.sdl.schneider-electric.com/us/en/tests/se-example-pages/corporate/financial-results.jsp");
		if (MultipleEmailSubmitFooter.validateWordWrap(driver)
				& MultipleEmailSubmitFooter.validateDubbleSubmissionEmailFooter(driver))
			driver.OutputStream("PASSED");
		else
			driver.OutputStream("FAILED");
		driver.close();
	}
}
