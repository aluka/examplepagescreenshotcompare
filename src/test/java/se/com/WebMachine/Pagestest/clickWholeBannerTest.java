package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Tools.Driver;
import se.com.WebMachine.Pages.clickWholeBanner;

public class clickWholeBannerTest {

	public static void main(String[] args) {
		Driver driver = new Driver("Chrome", "20");
		driver.get(
				"http://www-pre.amer1.sdl.schneider-electric.com/us/en/tests/se-example-pages/work/dispatch_two_column.jsp");
		clickWholeBanner.validateLinkBanner(driver);
		driver.wait(1500);
		driver.close();
	}
}
