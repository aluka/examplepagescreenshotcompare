package se.com.WebMachine.Pagestest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErrorMessage {
	public static void main(String arg[]) {
		// Start Browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//driver//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// Goto URL
		driver.get(
				"http://www-pre.amer1.sdl.schneider-electric.com/us/en/tests/se-example-pages/work/cj-explore.jsp/test");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Start Testing
		String bodyText = driver.findElement(By.id("page")).getText();
		System.out.println(bodyText);
		if (bodyText.contains("Page Not Found"))
			System.out.println("Test Passed");
		else
			System.out.println("Test Failed");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// End test
		driver.close();
		driver.quit();
	}

}
