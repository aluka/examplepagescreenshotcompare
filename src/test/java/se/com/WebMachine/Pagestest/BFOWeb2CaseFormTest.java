package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.BFOWeb2CaseForm;
import se.com.WebMachine.Tools.Driver;

public class BFOWeb2CaseFormTest {

	public static void main(String[] args) {
		Driver driver = new Driver("Chrome", "20");
		// driver.get("http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/work/marketo-forms/contact-sales-1col-poc.jsp");
		// driver.get("http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/work/contact-sales-form-embedded.jsp");

		// GOTO The Link
		driver.get("http://www-pre.amer1.sdl.schneider-electric.com/us/en/tests/se-example-pages/work/web2case.jsp");
		driver.waitToLoad(20, "Schneider");
		driver.wait(200);
		// driver.wait(500);
		if (BFOWeb2CaseForm.validateForm(driver))
			driver.OutputStream("Support sales data input test passed.");
		else
			driver.OutputStream("Support sales data input test failed.");
		driver.wait(500);
		driver.close();
	}

}
