package se.com.WebMachine.Pagestest;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Pages.AtWork;
import se.com.WebMachine.Pages.HomePage;
import se.com.WebMachine.Tools.Driver;

public class AtWorkTest {
	
	public static void main(String[] arg){
		String url = "http://www.schneider-electric.us/en/";
		Driver driver= new Driver("Chrome", "20");
		driver.get( url);
		if(AtWork.testAtWork(driver))driver.OutputStream("At-Work Content PASSED");
		else	driver.OutputStream("At-Work Content Failed", false);
		driver.close();
	}
}
