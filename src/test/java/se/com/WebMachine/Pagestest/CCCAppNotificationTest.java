package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.CCCAppNotification;
import se.com.WebMachine.Tools.Driver;

public class CCCAppNotificationTest {

	private static String url = "http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/corporate/financial-results.jsp";

	public static void main(String[] args) {
		Driver driver = new Driver("Chrome", "20");
		driver.get(url);
		driver.waitToLoad(20, "Schneider");
		driver.wait(500);
		CCCAppNotification.validateCccAppNoti(driver, "Google");
		driver.close();
	}

}
