package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.SupportToolsMarForm;
import se.com.WebMachine.Tools.Driver;

public class SupportToolsMarFormTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Driver driver = new Driver("Chrome", "20");
		// GOTO The Link
		// driver.get("http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/work/marketo-forms/contact-sales-1col-poc.jsp");
		// driver.get("http://www-int1.schneider-electric.com/us/en/tests/se-example-pages/work/contact-sales-form-embedded.jsp");
		driver.get(
				"http://www-pre.amer1.sdl.schneider-electric.com/us/en/tests/se-example-pages/work/contact-sales-form.jsp");
		if (SupportToolsMarForm.validateForm(driver))
			driver.OutputStream("Support sales data input test passed.");
		else
			driver.OutputStream("Support sales data input test failed.");
		driver.wait(500);
		driver.close();
	}

}
