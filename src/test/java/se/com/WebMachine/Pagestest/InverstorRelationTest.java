package se.com.WebMachine.Pagestest;

import se.com.WebMachine.Pages.InverstorRelation;
import se.com.WebMachine.Tools.Driver;

public class InverstorRelationTest {
	private static String url = "http://www-sqe1.schneider-electric.com/us/en/tests/se-example-pages/corporate/investor-relations.jsp";

	public static void main(String[] args) {
		Driver driver = new Driver("Chrome", "20");
		driver.get(url);
		InverstorRelation.validateInvestorRelations(driver);
		driver.close();
	}

}
