package se.com.WebMachine.Tools;

import java.awt.image.BufferedImage;
//import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.openqa.selenium.Point;
//import Util.ExtendReporter;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import se.com.WebMachine.Util.ExtentReporter;


/** @author Hossain. <p>
 * this class contains few basic driver actions/methods that might use
 * multiple times through out the project those custom build methodes help
 * keep the program stable.
 */
public class Driver implements WebDriver {
	
	private static String environment="INT";
	private final String USERNAME = "hossainelahi1";
	private final String AUTOMATE_KEY = "1cqGi9SqKEBD49pyqGsg";
	private final String URL = "http://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
//	private final String PROXY = "gateway.schneider.zscaler.net:9480";
	/**Contains and accumulate all the error massage generate during running the test suite.*/
	public StringBuilder ErrorMassage = new StringBuilder(" ");
	private static final String screenShoteFolderPath = System.getProperty("user.dir")+"\\reports\\NewImages\\";
	private static String baselineFolderPath = System.getProperty("user.dir")+"\\reports\\BaseLine-"+environment+"\\";
	private static final String currentRunFolderPath = System.getProperty("user.dir")+"\\reports\\CurrentRun\\";
	private static final String diffFolderPath = System.getProperty("user.dir")+"\\reports\\DiffrentImage\\";
	private WebDriver driver;
	

	/**initialize chrome driver with "20" sec timeout.*/	
	public Driver() {
		initialize("Chrome", "20");
	}
	/**initialize driver with specifide timeout.*/
	public Driver(String browser, String timeout) {
		initialize(browser, timeout);
	}
	/**return webdriver instance. null if no driver initialized or closed already.*/
	public WebDriver getInstance() {
		return driver;
	}
	
	private WebDriver initialize(String browser, String timeout) {
 		baselineFolderPath = System.getProperty("user.dir")+"\\reports\\BaseLine-"+environment+"\\";
		ExtentReporter.setupBrowser(browser);
		long time = Long.valueOf(timeout).longValue();
		if (browser.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "//driver//chromedriver.exe");
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable(LogType.BROWSER, Level.SEVERE);
			cap.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			cap.setBrowserName("googlechrome");
			cap.setPlatform(Platform.ANY);
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("ChromeEmu")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "//driver//chromedriver.exe");
			Map<String, String> mobileEmulation = new HashMap<String, String>();
			mobileEmulation.put("deviceName", "Google Nexus 5X");
			Map<String, Object> chromeOptions = new HashMap<String, Object>();
			chromeOptions.put("mobileEmulation", mobileEmulation);
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setBrowserName("googlechrome");
			cap.setPlatform(Platform.ANY);
			cap.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
			driver = new ChromeDriver(cap);
		} else if (browser.equalsIgnoreCase("Firefox")) {
//			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+ "//driver//geckodriver.exe");
 			DesiredCapabilities cap = DesiredCapabilities.firefox();
 	        LoggingPreferences logPrefs = new LoggingPreferences();
 	        logPrefs.enable(LogType.BROWSER, Level.SEVERE);
 	        cap.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
 	        cap.setBrowserName("firefox");
 	        cap.setPlatform(Platform.ANY);
	        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	        cap.setCapability(CapabilityType.ENABLE_PROFILING_CAPABILITY, true);
			/*webdriver.firefox.marionette
			 * webdriver.gecko.driver*/
 			driver = new FirefoxDriver(cap);
 		} else if (browser.equalsIgnoreCase("BrowserStack")) {
			DesiredCapabilities caps = new DesiredCapabilities();
			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable(LogType.BROWSER, Level.SEVERE);
			caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			caps.setCapability("browserstack.debug", "true");
			caps.setCapability("browserstack.local", "true");
			caps.setCapability("browserstack.video", "false");
			caps.setCapability("browserName", "iPhone");
			caps.setCapability("platform", "MAC");
			caps.setCapability("device", "iPhone 5S");
			try {
				driver = new RemoteWebDriver(new URL(URL), caps);
			} catch (MalformedURLException e) {
				OutputStream("Cant start remote browser. Problem with browser stack connection.");
			}
		} else if (browser.equalsIgnoreCase("IExplorer")) {
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "//driver//IEDriverServer.exe");
			DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
			cap.setPlatform(Platform.ANY);
			driver = new InternetExplorerDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
		driver.get("http://www.google.com");
		wait(100);
		boolean code = false;
		int loopCount = 0;
		while (!code) {
			loopCount++;
			String title = driver.getTitle();
			code = title.contains("Google");
			wait(100);
			try {
				if (driver.getTitle().contains("Directory Authentication")) {
					driver.findElement(By.xpath("//input[@name='lognsfc']"))
							.sendKeys("hossain.elahi@non.schneider-electric.com");
					wait(100);
					driver.findElement(By.xpath("//input[@type='submit']")).click();
					wait(100);
				}
			} catch (Exception e) {
			}
			if (loopCount > 20) {
				loopCount = 1;
				driver.get("http://www.google.com");
			}
		}
//		try {
//			FileUtils.cleanDirectory(new File(screenShoteFolderPath));
//		} catch (Exception e) {
//		}
		wait(500);
		return driver;
	}
	/**Wait untile page load complete with title contains specified String.*/
	public void waitToLoad(String title) {
		boolean status = false;
		wait(100);
		String statusDoc;
		JavascriptExecutor jsp = (JavascriptExecutor) driver;
		while (status) {
			String curTitle = driver.getTitle();
			statusDoc = (String) jsp.executeScript("return document.readyState");
			status = curTitle.toLowerCase().contains(title.toLowerCase()) & statusDoc.equalsIgnoreCase("complete");
		}
	}

	/**override driver manage instance*/
	@Override
	public Options manage() {
		return driver.manage();
	}

	/**override driver navigate instance*/
	@Override
	public Navigation navigate() {
		return driver.navigate();
	}
	/**override driver switchTo instance*/
	@Override
	public TargetLocator switchTo() {
		return driver.switchTo();
	}

	/** override getpageSource inctance */
	@Override
	public String getPageSource() {
		return driver.getPageSource();
	}
	
	/**Override getWindowHandle*/
	@Override
	public String getWindowHandle() {
		return driver.getWindowHandle();
	}

	@Override
	public Set<String> getWindowHandles() {
		return driver.getWindowHandles();
	}

	public void navigateTo(String url) {
		navigate().to(url);
		waitToLoad();
	}

	public void refresh() {
		driver.navigate().refresh();
		waitToLoad();
	}

	@Override
	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	@Override
	public String getTitle() {
		return driver.getTitle();
	}

	@Override
	public void get(String url) {
		driver.get(url);
		wait(100);
		waitToLoad();
		wait(100);
		waitToLoad();
	}

	public void get(String url, String title) {
		driver.get(url);
		wait(100);
		waitToLoad();
		wait(100);
		waitToLoad(20, title);
	}

	public boolean waitToLoad(int waitTimeInSec, String title) {
		boolean isLoaded = false;
		wait(100);
		WebDriverWait wait = new WebDriverWait(driver, waitTimeInSec);
		isLoaded = wait.until(ExpectedConditions.titleContains(title)); // "Schneider"
		return isLoaded;
	}

	public boolean waitToLoad() {
		boolean status = false;
		wait(100);
		String statusDoc;
		JavascriptExecutor jsp = (JavascriptExecutor) driver;
		while (status) {
			statusDoc = (String) jsp.executeScript("return document.readyState");
			status = statusDoc.equalsIgnoreCase("complete");
		}
		wait(100);
		// OutputStream("Page "+driver.getTitle()+" loaded.");
		return status;
	}

	public void click(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			if (element != null) {
				element = wait.until(ExpectedConditions.elementToBeClickable(element));
				/*
				 * Actions mouse = new Actions(driver);
				 * mouse.moveToElement(element, element.getSize().width/2,
				 * element.getSize().height/2); mouse.click();
				 * mouse.build().perform();
				 */
				element.click();
			} else {
				OutputStream("Element is null");
			}
		} catch (ElementNotVisibleException e) {
			throw e;
		} catch (WebDriverException e) {
			if (e.getMessage().startsWith("unknown error: " + "Element is not clickable at point")) {
			}
		} catch (Exception e) {
			Reporter.log("Message is : " + e.getMessage(), true);
			e.printStackTrace();
		}
	}

	public void click(WebElement element, int xofset, int yofset) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			if (element != null) {
				element = wait.until(ExpectedConditions.elementToBeClickable(element));
				Actions mouse = new Actions(driver);
				mouse.moveToElement(element, xofset, yofset);
				mouse.click().build().perform();
			} else {
				OutputStream("Element is null");
			}
		} catch (ElementNotVisibleException e) {
			throw e;
		} catch (WebDriverException e) {
			if (e.getMessage().startsWith("unknown error: " + "Element is not clickable at point")) {
			}
		} catch (Exception e) {
			Reporter.log("Message is : " + e.getMessage(), true);
			e.printStackTrace();
		}
	}

	public void click(By location) {
		WebElement element = findElement(location, 20);
		click(element);
	}

	public void click(String locator, String location) {
		click(getBy(locator, location));
	}

	public boolean readServerError() {
		boolean status = true;
		LogEntries logEntries = null;
		try {
			logEntries = driver.manage().logs().get(LogType.BROWSER);
			for (LogEntry entry : logEntries) {
				if (entry.getMessage().contains("zscaler.") | entry.getMessage().contains("TOO_MANY_REDIRECTS")
						| entry.getMessage().contains("chrome-extension") | entry.getMessage().contains("fs.")
						| !(entry.getLevel().toString().contains("SEVERE"))
						| entry.getMessage().contains("c.ooyala.com")
						| entry.getMessage().contains("api.demandbase.com")
						| entry.getMessage().contains("tags.bluekai.com")
						| entry.getMessage().contains("stat_origin.gif"))
					continue;
				OutputStream(entry.getLevel() + " " + entry.getMessage(), false);
				status = false;
			}
			logEntries.getAll().clear();
		} catch (Exception e) {
		}
		return status;
	}

	public By getBy(String locator, String location) {
		By by = null;
		if (locator.equalsIgnoreCase("id")) {
			by = By.id(location);
		} else if (locator.equalsIgnoreCase("xPath")) {
			by = By.xpath(location);
		} else if (locator.equalsIgnoreCase("linkText")) {
			by = By.linkText(location);
		} else if (locator.equalsIgnoreCase("cssSelector")) {
			by = By.cssSelector(location);
		} else if (locator.equalsIgnoreCase("name")) {
			by = By.name(location);
		}
		return by;
	}

	/*
	 * public WebElement findElement(final final By locator, final int
	 * timeoutSeconds) { FluentWait<WebDriver> wait = new
	 * FluentWait<WebDriver>(driver).withTimeout(timeoutSeconds,
	 * TimeUnit.SECONDS).pollingEvery(300,
	 * TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class); return
	 * wait.until(new Function<Web WebElement>() { public WebElement
	 * apply(WebDriver webDriver) { return driver.findElement(locator); } } ); }
	 */
	@Override
	public WebElement findElement(By locator) {
		return findElement(locator, 20);
	}

	public WebElement findElement(final By locator, final int timeoutSeconds) {
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeoutSeconds);
		// wait = wait.pollingEvery(timeoutSeconds, TimeUnit.MILLISECONDS);
		try {
			element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
		} catch (Exception e) {
			// OutputStream("find element exception: "+e);
			// OutputStream(locator+" cant find. ", false);
		}

		return element;
	}

	public WebElement findElement(String locator, String location, int timeoutSeconds) {
		By by = getBy(locator, location);
		return findElement(by, timeoutSeconds);
	}

	public List<WebElement> findElements(String locator, String location) {
		By by = getBy(locator, location);
		return findElements(by);
	}

	@Override
	public List<WebElement> findElements(By by) {
		List<WebElement> elements = null;
		try {
			elements = driver.findElements(by);
		} catch (TimeoutException toe) {
			OutputStream(by + " : No item is available. TimeOut.");
		} catch (Throwable tw) {
			OutputStream(by + " : No item is available. Error massage: " + tw);
		}
		return elements;
	}

	public boolean isElementAvailable(String locator, String location) {
		boolean elemPresent = false;
		elemPresent = isElementAvailable(locator, location, 20);
		return elemPresent;
	}

	public boolean isElementAvailable(String locator, String location, int timeout) {
		boolean elemPresent = false;
		int timeoutSeconds = timeout;
		By by = getBy(locator, location);
		try {
			if (findElement(by, timeoutSeconds).isDisplayed())
				elemPresent = true;
		} catch (Exception e) {
			elemPresent = false;
		}
		return elemPresent;
	}

	public boolean isElementAvailable(By location) {
		boolean status = false;
		try {
			status = (findElement(location, 20).isDisplayed());
		} catch (Exception e) {
			status = false;
		}
		return status;
	}

	public boolean isElementAvailable(By location, int timeout) {
		boolean status = false;
		try {
			status = (findElement(location, timeout).isDisplayed());
		} catch (Exception e) {
			status = false;
		}
		return status;
	}

	public boolean isElementVissible(By location, int timeout) {
		Dimension size = null;
		try {
			size = findElement(location, timeout).getSize();
		} catch (Exception e) {

		}
		if (size == null)
			return false;
		return size.height > 0 && size.width > 0;
	}

	public boolean isElementVissible(By location) {
		return isElementVissible(location, 20);
	}

	public boolean isElementVissible(String locator, String location, int timeout) {
		By by = getBy(locator, location);
		return isElementVissible(by, timeout);
	}

	public boolean checkDomain(String baseURL) {
		String url = driver.getCurrentUrl();
		url = url.substring(7, url.indexOf(".sch"));
		String oldURL = baseURL;
		oldURL = oldURL.substring(7, oldURL.indexOf(".sch"));
		boolean status = url.equalsIgnoreCase(oldURL);
		if (status) {
			OutputStream(
					"Link domain is checked and varrified. current domain is: " + url + " . base domain is: " + oldURL);
		} else {
			OutputStream("Link domain is checked but not varrified!!!!!!!" + driver.getCurrentUrl(), false);
			takeScreenShot("DomainError");
		}

		return status;
	}

	public String getLinkIdentifier(String locator, String location) {
		By by = getBy(locator, location);
		WebElement webElement = driver.findElement(by);
		location = webElement.getAttribute("title").trim().length() > 0 ? webElement.getAttribute("title").trim()
				: webElement.getText().trim();
		// System.out.println(">>>>>>>>in getLinkIdentifier>>>>>>"+location);
		if (location.length() > 0) {
		} else {
			String hReflink = webElement.getAttribute("href");
			if (hReflink != null) {
				int firstIndex = hReflink.contains("www") ? webElement.getAttribute("href").indexOf(".")
						: webElement.getAttribute("href").indexOf("/") + 1;
				String hReflinkRemainder = webElement.getAttribute("href").substring(firstIndex + 1);
				int secondIndex = hReflinkRemainder.indexOf(".");
				location = webElement.getAttribute("href").substring(firstIndex + 1, firstIndex + secondIndex + 1);
			}
		}
		return location;
	}

	public void wait(int milisec) {
		try {
			Thread.sleep(milisec % 100);
			TimeUnit.SECONDS.sleep(milisec / 100);
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}
	}

	public String logTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}

	@Override
	public void quit() {
		driver.close();
		driver.quit();
	}

	@Override
	public void close() {
		driver.close();
	}

	public void OutputStream(String data) {
		Reporter.log(logTime() + " ............... " + data, true);
	}

	public void OutputStream(String data, boolean error) {
		Reporter.log(logTime() + " ............... " + data, true);
		ErrorMassage.append("\n" + data);
	}
	public void MoveTo(By location) {
		MoveTo(findElement(location));
	}
	public void MoveTo(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			if (element != null) {
				wait.until(ExpectedConditions.elementToBeClickable(element));
				Actions mouse = new Actions(driver);
				mouse.moveToElement(element);
				mouse.perform();
				wait(200);
			}
		} catch (ElementNotVisibleException e) {
			OutputStream("Element is not visible..  Error: " + e.getMessage(), false);
		} catch (Exception e) {
			Reporter.log("Message is : " + e.getMessage(), true);
			e.printStackTrace();
		}
	}
	public void MoveTo(String locator, String location) {
		By Loc = getBy(locator, location);
		MoveTo(Loc);
	}

	public void takeScreenShot(String txt) {
		File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(srcFile, new File(screenShoteFolderPath + txt + ".png"));
		} catch (IOException e) {
			OutputStream("Error saving ScreenShot");
		}
	}

	public void takeScreenShot(String locator, String location, String txt) {
		WebElement element = findElement(locator, location, 15);
		if (element == null) {
			OutputStream("Error saving screenShot. Element is not available in the page.");
			return;
		}
		takeScreenShot(element, txt);
	}

	public void takeScreenShot(By location, String txt) {
		WebElement element = findElement(location, 15);
		if (element == null) {
			OutputStream("Error saving screenShot. Element is not available in the page.");
			return;
		}
		takeScreenShot(element, txt);
	}

	public final void takeScreenShot(WebElement element, String txt) {
		try {
			Screenshot screenshot = new AShot().takeScreenshot(driver, element);
			try {
				ImageIO.write(screenshot.getImage(), "PNG", new File(screenShoteFolderPath + txt + ".png"));
			} catch (IOException e) {
				OutputStream("Error saving ScreenShote. IOException.");
			}
		} catch (NoSuchElementException e) {
			OutputStream("Error Saving ScreenShote. NoSuchElement in the page.");
		} catch (Exception e) {
			OutputStream("Error taking screenShote. Unexpected Exception: " + e.getMessage());
		}
	}

	public void compairImage(By location, By ignoreLocation, String txt){
		WebElement element = findElement(location, 15);
		if(element==null){
			OutputStream("Error saving screenShot. Element is not available in the page.");
			return;
		}
		compairImage(element, ignoreLocation, txt);
	}
	
	public void compairImage(WebElement element, By ignoreLocation, String txt){
		if(element==null || (!isElementAvailable(ignoreLocation,10))){
			OutputStream("Defined elements are not visible in the page. cant take screenshot");
			return;
		}
		BufferedImage BuffScreenshot = null;
		BufferedImage diffImage = null;
		Screenshot BaseLine = null;
		ImageDiff diff = null;
		Screenshot screenshot = null;
		try {
			screenshot = new AShot().addIgnoredElement(ignoreLocation).takeScreenshot(driver, element);
			BuffScreenshot = ImageIO.read(new File(baselineFolderPath+txt+".png"));
			if(BuffScreenshot==null){
				OutputStream("There is no baseline image. Going to store this screenshot as baseline.");
				ImageIO.write(screenshot.getImage(), "PNG", new File(baselineFolderPath+txt+".png"));
				return;
			}
		} catch (IOException e) {
//			Driver.OutputStream("Error Reading baseline image. Error: "+e.getMessage());
			OutputStream("There is no baseline image. Going to store this screenshot as baseline.");
			try {
				ImageIO.write(screenshot.getImage(), "PNG", new File(baselineFolderPath+txt+".png"));
			}catch (IOException e1) {}
			return;		
			}
		BaseLine = new Screenshot(BuffScreenshot);
		if(screenshot!=null){
			diff = new ImageDiffer().makeDiff(screenshot, BaseLine);
			diffImage = diff.getMarkedImage();
		}else{
			OutputStream("Screenshot null.");
			return;
		}
		try {
			ImageIO.write(screenshot.getImage(), "PNG", new File(currentRunFolderPath+txt+".png"));
			ImageIO.write(diffImage, "PNG", new File(diffFolderPath+txt+".png"));
		} catch (IOException e) {
			OutputStream("Error saving new image or diff image. Error: " +e.getMessage());
		}
	}
	public void compairImage(WebElement element, String txt){
		if(element==null){
			OutputStream("Defined element are not visible in the page. cant take screenshot");
			return;
		}
		BufferedImage BuffScreenshot = null;
		BufferedImage diffImage = null;
		Screenshot BaseLine = null;
		ImageDiff diff = null;
		Screenshot screenshot = null;
		try {
			screenshot = new AShot().takeScreenshot(driver, element);
			BuffScreenshot = ImageIO.read(new File(baselineFolderPath+txt+".png"));
			if(BuffScreenshot==null){
				OutputStream("There is no baseline image. Going to store this screenshot as baseline.");
				ImageIO.write(screenshot.getImage(), "PNG", new File(baselineFolderPath+txt+".png"));
				return;
			}
		} catch (IOException e) {
//			Driver.OutputStream("Error Reading baseline image. Error: "+e.getMessage());
			OutputStream("There is no baseline image. Going to store this screenshot as baseline.");
			try {
				ImageIO.write(screenshot.getImage(), "PNG", new File(baselineFolderPath+txt+".png"));
			}catch (IOException e1) {}
			return;		
			}
		BaseLine = new Screenshot(BuffScreenshot);
		if(screenshot!=null){
			diff = new ImageDiffer().makeDiff(screenshot, BaseLine);
			diffImage = diff.getMarkedImage();
		}else{
			OutputStream("Screenshot null.");
			return;
		}
		try {
			ImageIO.write(screenshot.getImage(), "PNG", new File(currentRunFolderPath+txt+".png"));
			ImageIO.write(diffImage, "PNG", new File(diffFolderPath+txt+".png"));
		} catch (IOException e) {
			OutputStream("Error saving new image or diff image. Error: " +e.getMessage());
		}
	}

	public void compairImage(String txt){
		File srcFile =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(srcFile, new File(currentRunFolderPath+txt+".png"));
		} catch (IOException e) {
			OutputStream("Error storing current screenshot. Error: "+e.getMessage());
		}
		BufferedImage BuffScreenshot = null;
		BufferedImage BufCurrentRun = null;
		BufferedImage diffImage = null;
		Screenshot BaseLine = null;
		ImageDiff diff = null;
		Screenshot screenshot = null;
		try {
			BufCurrentRun = ImageIO.read(new File(currentRunFolderPath+txt+".png"));
			screenshot = new Screenshot(BufCurrentRun);
			BuffScreenshot = ImageIO.read(new File(baselineFolderPath+txt+".png"));
			if(BuffScreenshot==null){
				OutputStream("There is no baseline image. Going to store this screenshot as baseline.");
				ImageIO.write(screenshot.getImage(), "PNG", new File(baselineFolderPath+txt+".png"));
				return;
			}
		} catch (IOException e) {
//			Driver.OutputStream("Error Reading baseline image. Error: "+e.getMessage());
			OutputStream("There is no baseline image. Going to store this screenshot as baseline.");
			try {
				ImageIO.write(screenshot.getImage(), "PNG", new File(baselineFolderPath+txt+".png"));
			}catch (IOException e1) {}
			return;		
			}
		BaseLine = new Screenshot(BuffScreenshot);
		if(screenshot!=null){
			diff = new ImageDiffer().makeDiff(screenshot, BaseLine);
			diffImage = diff.getMarkedImage();
		}
		try {
			ImageIO.write(diffImage, "PNG", new File(diffFolderPath+txt+".png"));
		} catch (IOException e) {
			OutputStream("Error saving new image or diff image. Error: " +e.getMessage());
		}
	}
	
	public void compairImage(By location,String txt){
		WebElement element = findElement(location, 20);
		compairImage(element,txt);
	}
	
	public void closeMultipleTabs(WebDriver driver){
		ArrayList<String> windowNew = new ArrayList<String>(driver.getWindowHandles());
		for(int no =1; no< windowNew.size(); no ++){
			driver.switchTo().window(windowNew.get(windowNew.size()-no));
			driver.close();
		}
		windowNew = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(windowNew.get(0));
	}

	public void closeMultipleTabs() {
		ArrayList<String> windowNew = new ArrayList<String>(driver.getWindowHandles());
		for (int no = 1; no < windowNew.size(); no++) {
			driver.switchTo().window(windowNew.get(windowNew.size() - no));
			driver.close();
		}
		windowNew = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(windowNew.get(0));
	}

	public void switchTab() {
		ArrayList<String> windowName = new ArrayList<String>(driver.getWindowHandles());
		if (windowName.size() > 1) {
			driver.switchTo().window(windowName.get(windowName.size() - 1));
		}
	}

	public String logDate() {
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		System.out.println(dateFormat.format(cal.getTime()));
		String s = dateFormat.format(cal.getTime());
		System.out.println(s.split(" ")[0] + "_" + s.split(" ")[1]);
		System.out.println(s.split(" ")[0] + s.split(" ")[1].split(":")[0] + "_" + s.split(" ")[1].split(":")[1] + "_"
				+ s.split(" ")[1].split(":")[2]);
		return s.split(" ")[0] + "_" + s.split(" ")[1].split(":")[0] + "_" + s.split(" ")[1].split(":")[1] + "_"
				+ s.split(" ")[1].split(":")[2];
	}

	public boolean isElementPresent(String locator, String location) {
		return isElementPresent(locator, location, 20);
	}

	public boolean isElementPresent(String locator, String location, int timeout) {
		boolean status = false;
		try {
			status = findElements(locator, location).size() > 0;
		} catch (Exception e) {
		}
		return status;
	}

	public void scrollUp(int amount) {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("scroll(0, -" + amount + ");");
		} catch (Exception e) {
		}
	}

	public void scrollDown(int amount) {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("scroll(0, " + amount + ");");
		} catch (Exception e) {
		}
	}

	public void scrollTop() {
		try {
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollTo(0, -document.body.scrollHeight)");
		} catch (Exception e) {

		}
	}

	public void scrollBottom() {
		try {
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		} catch (Exception e) {

		}
	}

	public void scrollToElement(WebElement element) {
		scrollToElement(element, 0);
	}

	public void scrollToElement(WebElement element, int offset) {
		Point location = element.getLocation();
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			// jse.executeScript("return window.title;");
			jse.executeScript("window.scrollTo(0, " + (location.getY() - offset) + ")");
		} catch (Exception e) {
		}
	}

	public int getPageLocation() {
		int location = 0;
		wait(500);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		try {
			Object loc = jse.executeScript("return window.pageYOffset");
			if (loc.getClass().getSimpleName().contains("Double")) {
				location = ((Double) loc).intValue();
			} else {
				location = (int) Long.valueOf(loc.toString()).longValue();
			}
			// System.out.println("Runtime class is:
			// "+loc.getClass().getName()+". Type Name:
			// "+loc.getClass().getTypeName());
			// System.out.println("Simpler name is:
			// "+loc.getClass().getSimpleName()+". Cronical Name:
			// "+loc.getClass().getCanonicalName());
			// location = (int)Long.valueOf(loc.toString()).longValue();
			// location = ((Double)).intValue();
		} catch (Exception e) {
			OutputStream("PageLocation Triggerd ecception: " + e.getMessage());
		}
		return location;
	}

	public void switchtoMainWindow(String baseURL) {
		try {
			ArrayList<String> tabBase = new ArrayList<String>(driver.getWindowHandles());
			int intotalhandles = tabBase.size();
			for (int i = intotalhandles; i != 1; i--) {
				driver.switchTo().window(tabBase.get(i - 1));
				Thread.sleep(100);
				driver.close();
			}
			driver.switchTo().window(tabBase.get(0));
		} catch (NoSuchWindowException nswe) {
			switchtoMainWindow(baseURL);
			driver.switchTo().window(baseURL);
		} catch (Exception e) {
			Reporter.log("Could not switch back to base Window", true);
		}
	}

	public PhantomJSDriverService getPhantomJsService() {
		return new PhantomJSDriverService.Builder().usingAnyFreePort()
				.usingPhantomJSExecutable(new File(System.getProperty("user.dir") + "//driver//phantomjs.exe")).build();
	}
}
