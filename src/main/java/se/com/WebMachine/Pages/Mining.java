package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class Mining {

	private static final String locator = "xpath";
	private static final String mainBodyXpath = "//*[@class='main']";
	private static final String miningAnchorXpath = mainBodyXpath + "//*[@class='anchor-comp mining-landing']";
	private static final By whiteBar = By.id("trail");
	private static final String ceraselContainer = "//section[contains(@id,'slider')]";
	private static final String GreenHeaderXpath = "//*[@class='main-menu']";
	private static String miningSolutionBoxXpath = mainBodyXpath + "//*[@class='image-map']"
			+ "//*[@class='box-solutions']";
	private static String popUpsXpath = miningSolutionBoxXpath + "/div[contains(@class,'popup')]";
	private static int offset;

	public static boolean miningSolutionTest(Driver driver) {
		return minePopUpsValidation(driver) & anchorTagT2Validation(driver) & anchorTagValidateion(driver);
	}

	private static boolean anchorTagT2Validation(Driver driver) {
		boolean status = true;
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		offset = getPageOfset(driver);
		String link, linkText;
		List<WebElement> links = driver.findElements(locator, mainBodyXpath + "//a");
		for (int linkNo = 0; linkNo < links.size(); linkNo++) {
			link = links.get(linkNo).getAttribute("href");
			if (link.contains("#")) {
				linkText = links.get(linkNo).getText();
				if (linkText.contains("\n"))
					linkText = linkText.substring(0, linkText.indexOf("\n"));
				// driver.OutputStream(linkText +" link contains a #tag.
				// Continue Testing");
				link = link.substring((link.indexOf("#") + 1));
				if (link.length() > 3) {
					linkText = linkText.toUpperCase();
					driver.OutputStream(linkText + " is a type 2 anchor tag. Going to click on the link.");
					driver.scrollToElement(links.get(linkNo), offset);
					driver.click(links.get(linkNo));
					driver.wait(500);
					driver.waitToLoad(20, "Schneider");
					driver.wait(100);
					driver.OutputStream("Expected location in the page is: " + link);
					if (tagT2DesValidate(driver, link, offset)) {
						driver.OutputStream(linkText + " test PASSED");
						status = status & true;
					} else {
						driver.OutputStream(linkText + " test FAILED");
						status = status & false;
					}
					driver.navigate().to(url);
					driver.waitToLoad(title);
					driver.wait(100);
					links = driver.findElements(locator, mainBodyXpath + "//a");
				}
			}
		}
		return status;
	}

	private static boolean tagT2DesValidate(Driver driver, String className, int offSet) {
		boolean status = false;
		try {
			WebElement expElem = driver.findElement(By.className(className));
			int expectedPos = expElem.getLocation().getY();
			int accualPos = driver.getPageLocation();
			int offset = getPageOfset(driver);
			status = validateScroll(expectedPos, accualPos + offset, 5);
		} catch (NoSuchElementException e) {
			driver.OutputStream("There is no expected element in the destination page");
		}
		return status;
	}

	public static boolean minePopUpsValidation(Driver driver) {
		boolean status = true;
		offset = getPageOfset(driver);
		int noOfPopUps;
		if (driver.isElementAvailable(locator, miningSolutionBoxXpath, 2)) {
			int noOfImages = driver.findElements(locator, miningSolutionBoxXpath).size();
			for (int imageNo = 1; imageNo <= noOfImages; imageNo++) {
				switchToPopUpImageNo(imageNo);
				driver.scrollToElement(driver.findElement(By.xpath(miningSolutionBoxXpath)), offset);
				driver.OutputStream("There is Mining box in this page. Continue Testing mining image popUps.");
				noOfPopUps = driver.findElements(locator, popUpsXpath).size();
				if (noOfPopUps < 1) {
					driver.OutputStream("There is no popUps icon in this page. Skip Testing");
					return true;
				}
				for (int popUpNo = 1; popUpNo <= noOfPopUps; popUpNo++) {
					status = status & testPopUpNo(driver, popUpNo);
					if (popUpNo % 3 == 0) {
						driver.navigate().refresh();
						driver.wait(100);
						driver.waitToLoad();
					}
				}
			}
			if (status)
				driver.OutputStream("PopUp Test PASSED.");
			else
				driver.OutputStream("Pop up Test FAILED.", false);
		} else {
			driver.OutputStream("There is no Mining Solution box in this page. Skip this Test.");
		}
		return status;
	}

	private static void switchToPopUpImageNo(int no) {
		miningSolutionBoxXpath = mainBodyXpath + "//*[@class='image-map']" + "[" + no + "]"
				+ "//*[@class='box-solutions']";
		popUpsXpath = miningSolutionBoxXpath + "/div[contains(@class,'popup')]";
	}

	private static boolean testPopUpNo(Driver driver, int no) {
		boolean status = true;
		if (clickPopupNo(driver, no)) {
			driver.OutputStream("Clicked on popUp no " + no + ".");
			status = validatePopUpContent(driver, no);
			if (closePopUp(driver, no))
				driver.OutputStream("close the pop up no " + no + ".");
			else
				driver.OutputStream("Could not close pop up no " + no + ".");
		} else {
			driver.OutputStream("Cant click on popUp no " + no + ". Test Failed", false);
			status = false;
		}
		if (status)
			driver.OutputStream("Minning Image Pop up link no " + no + " is validated.");
		else
			driver.OutputStream("Mining Image Pop up link no " + no + " test FAILED", false);
		return status;
	}

	private static boolean clickPopupNo(Driver driver, int popUpNo) {
		driver.click(locator, popUpsXpath + "[" + popUpNo + "]");
		driver.wait(100);
		driver.waitToLoad();
		driver.wait(100);
		return driver.isElementVissible("Xpath", popUpsXpath + "[" + popUpNo + "]//*[contains(@class,'c-popup')]", 2);
	}

	private static boolean validatePopUpContent(Driver driver, int popUpNo) {
		boolean status = false;
		String text = "";
		String xPath = popUpsXpath + "[" + popUpNo + "]//*[contains(@class,'c-popup')]";
		List<WebElement> popUpBoxes = driver.findElements(locator, xPath);
		status = popUpBoxes.size() > 0;
		for (WebElement popUpBox : popUpBoxes) {
			Dimension size = popUpBox.getSize();
			status = status && size.height > 0 && size.width > 0;
			text = text + popUpBox.getText();
		}
		driver.OutputStream("Pop up Link Text is: " + text);
		status = text.length() > 0;
		String url = driver.getCurrentUrl();
		if (!(url.contains("-int") || url.contains("-sqe") || url.contains("-pre")))
			status = status & popLinkTest(driver, xPath);
		return status;
	}

	private static boolean closePopUp(Driver driver, int popUpNo) {
		driver.click(locator, popUpsXpath + "[" + popUpNo + "]/span[@class='close-popup']");
		driver.waitToLoad();
		return !driver.isElementVissible("Xpath", popUpsXpath + "[" + popUpNo + "]/span[@class='close-popup']", 5);
	}

	private static boolean popLinkTest(Driver driver, String xpath) {
		boolean status = true;
		String url = driver.getCurrentUrl();
		String link = "";
		if (driver.isElementAvailable(locator, xpath + "//a", 2)) {
			link = driver.findElement(locator, xpath + "//a", 2).getAttribute("href");
		}
		if (link.contains("http://")) {
			driver.OutputStream("Pop up contains a link. Going to the link.");
			driver.click(locator, xpath);
			driver.wait(100);
			driver.switchTab();
			driver.waitToLoad();
			driver.wait(500);
			status = driver.readServerError();
			driver.closeMultipleTabs();
			driver.navigate().to(url);
		} else {
			driver.OutputStream("Pop up does not contain any link. Continue to the next Pop up.");
			driver.click(locator, xpath);
		}
		return status;
	}

	private static boolean anchorTagValidateion(Driver driver) {
		boolean status = true;
		offset = getPageOfset(driver);
		if (driver.isElementAvailable(locator, miningAnchorXpath, 2)) {
			driver.OutputStream("There is anchor tag in this page. Start testing.");
			int noOfAnchor = driver.findElements(locator, miningAnchorXpath + "/li").size();
			driver.OutputStream("There is " + noOfAnchor + " anchor tag in this page. Continue Testing");
			for (int anchorNo = 1; anchorNo <= noOfAnchor; anchorNo++) {
				status = status & testAnchorNo(driver, anchorNo);
				if (anchorNo % 3 == 0)
					driver.navigate().refresh();
				offset = getPageOfset(driver);
			}
			if (status)
				driver.OutputStream("Achore Functionality Test PASSED");
			else
				driver.OutputStream("Anchore Functionality Test FAILED");
		} else {
			driver.OutputStream("There is no anchor tag in this page.");
		}
		return status;
	}

	private static boolean testAnchorNo(Driver driver, int anchorNo) {
		boolean status = true;
		WebElement anchor = driver.findElement(locator, miningAnchorXpath + "/li[" + anchorNo + "]/a", 5);
		if (anchor == null) {
			driver.OutputStream("Anchor element no " + anchor + " is not accessable. Test FAILED.", false);
			return false;
		}
		driver.scrollToElement(anchor, offset);
		// driver.wait(500);
		String className = anchor.getAttribute("data-scroll");
		className = className.substring(1);
		WebElement destination = driver.findElement(By.className(className));
		if (destination == null) {
			driver.OutputStream("There is no designated destination for " + anchor.getText() + ". Test FAILED", false);
			return false;
		}
		int expected = destination.getLocation().getY();
		driver.click(anchor);
		driver.OutputStream("Clicked on " + anchor.getText());
		driver.wait(300);
		int accual = driver.getPageLocation();
		status = validateScroll(expected, accual + offset, 10);
		if (status)
			driver.OutputStream("Minning anchor name " + anchor.getText() + " went to the right location. Test PASSED");
		else
			driver.OutputStream("Minning anchor name " + anchor.getText() + " Test FAILED");
		return status;
	}

	private static boolean validateScroll(int expected, int accual, int adjustment) {
		System.out.println("expected location:" + expected + ", accual location is: " + accual + ".");
		int difference = Math.abs(expected - accual);
		return difference < adjustment;
	}

	public static int getPageOfset(Driver driver) {
		int offset = 0;
		driver.scrollTop();
		WebElement elem = null;
		if (driver.isElementAvailable(whiteBar, 2)) {
			elem = driver.findElement(whiteBar, 2);
			offset = elem.getLocation().getY() + elem.getSize().height;
		} else if (driver.isElementAvailable(locator, GreenHeaderXpath, 2)) {
			elem = driver.findElement(locator, GreenHeaderXpath, 2);
			offset = elem.getLocation().getY() + elem.getSize().height;
		} else if (driver.isElementAvailable(locator, ceraselContainer, 2)) {
			elem = driver.findElement(locator, ceraselContainer, 2);
			offset = elem.getLocation().getY();
		}
		// driver.OutputStream("Page ofset value is: "+offset);
		return offset;
	}

}
