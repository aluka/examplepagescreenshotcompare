package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.Actions;

import se.com.WebMachine.Tools.Driver;

@SuppressWarnings("unused")
public class SupportToolsMarForm {

	private static final String locator = "Xpath";
	private static final String marketoFormXpath = "//*[contains(@id, 'marketo') or  contains(@class, 'contact-form')]//form";
	private static final String firstName = marketoFormXpath + "//input[@id='FirstName']";
	private static final String lastName = marketoFormXpath + "//input[@id='LastName']";
	private static final String telephoneNO = marketoFormXpath + "//input[@id='Phone']";
	private static final String email = marketoFormXpath + "//input[@id='Email']";
	private static final String company = marketoFormXpath + "//input[@id='Company']";
	private static final String postCode = marketoFormXpath + "//input[@id='PostalCode']";
	private static final String comment = marketoFormXpath + "//*[@id='Description']";
	private static final String country = marketoFormXpath
			+ "//p[not(@class)][7]//div[contains(@class,'selectize-control')]";
	private static final String selectCountry = marketoFormXpath
			+ "//p[not(@class)][7]//div[contains(@class,'selectize-dropdown')]//div[@data-value]";
	private static final String State = marketoFormXpath
			+ "//p[not(@class)][8]//div[contains(@class,'selectize-control')]";
	private static final String StateCountry = marketoFormXpath
			+ "//p[not(@class)][8]//div[contains(@class,'selectize-dropdown')]//div[@data-value]";
	private static final String question1Xpath = marketoFormXpath
			+ "//p[not(@class)][11]//div[contains(@class,'selectize-control')]";
	private static final String selectQuestion1 = marketoFormXpath
			+ "//p[not(@class)][11]//div[contains(@class,'selectize-dropdown')]//div[@data-value]";
	private static final String question2Xpath = marketoFormXpath
			+ "//p[not(@class)][12]//div[contains(@class,'selectize-control')]";
	private static final String selectQuestion2 = marketoFormXpath
			+ "//p[not(@class)][12]//div[contains(@class,'selectize-dropdown')]//div[@data-value]";

	private static final String yesNo = marketoFormXpath + "//p[@class='no-grid']/label";
	private static final String submit = "//input[@class='submit-button']";
	private static final By whiteBar = By.id("trail");
	private static final String ceraselContainer = "//section[contains(@id,'slider')]";
	private static final String GreenHeaderXpath = "//*[@class='main-menu']";
	// private static final String question1Xpath = "";
	private static String successfulMassage = "Thank you";
	// private static String successfulMassage = "Thank you for contacting
	// Schneider Electric";

	public static boolean validateForm(Driver driver) {
		boolean status = true;
		Customer cus = Customer.CusValid1; // Readdata
		inputData(driver, cus); // Input data
		boolean successfull = inputSuccessful(driver); // Check successfull
		status = cus.Valid == successfull;
		if (status)
			driver.OutputStream("Customer care contact form test PASSED");
		else
			driver.OutputStream("Customer Care contact form test FAILED");
		return status;
	}

	public static boolean inputSuccessful(Driver driver) {
		boolean status = false;
		try {
			String text = driver.findElement(By.className("wrap-content"), 20).getText().trim();
			status = text.contains(successfulMassage);
		} catch (NoSuchElementException e) {
			driver.OutputStream("Exception Triggered");
		}
		return status;
	}

	public static void inputData(Driver driver, Customer cus) {
		int timeoutSeconds = 2;
		int pageOfset = getPageOfset(driver);
		HomePage.closechat(driver);
		try {
			if (driver.isElementAvailable(locator, firstName, 5))
				driver.findElement(locator, firstName, timeoutSeconds).sendKeys(cus.FirstName);
			else
				driver.OutputStream("First Name input box is not vissible inside Contact Sales Form.", false);
			if (driver.isElementAvailable(locator, lastName, 1))
				driver.findElement(locator, lastName, timeoutSeconds).sendKeys(cus.LastName);
			else
				driver.OutputStream("Last Name input box is not vissible inside Contact Sales Form.", false);
			if (driver.isElementAvailable(locator, email, 1))
				driver.findElement(locator, email, timeoutSeconds).sendKeys(cus.Email);
			else
				driver.OutputStream("Email input Box is not vissible inside contact sales form.", false);
			if (driver.isElementAvailable(locator, telephoneNO, 1))
				driver.findElement(locator, telephoneNO, timeoutSeconds).sendKeys(cus.Telephone);
			else
				driver.OutputStream("Telephone input box is not visible inside contact sales form.", false);
			if (driver.isElementAvailable(locator, company, 1))
				driver.findElement(locator, company, timeoutSeconds).sendKeys(cus.Company);
			else
				driver.OutputStream("Company input box is not visible inside contact sales form.", false);
			if (driver.isElementAvailable(locator, postCode, 1))
				driver.findElement(locator, postCode, timeoutSeconds).sendKeys(cus.PostCode);
			else
				driver.OutputStream("PostCode input box is not visible inside contact sales form.", false);
			if (driver.isElementAvailable(locator, country, 1)) {
				int countryNo = getDivNo(driver, cus.Country, country);
				// System.out.println("Country No "+countryNo);
				driver.OutputStream("Going to select country name :" + cus.Country);
				driver.click(locator, selectCountry + "[" + countryNo + "]");
			}
			if (driver.isElementAvailable(locator, question1Xpath, 1)) {
				int q1No = getDivNo(driver, cus.Question1, question1Xpath);
				// System.out.println("Quesiont No "+q1No);
				driver.OutputStream("Going to select question :" + cus.Question1 + " out of Question no 1");
				driver.click(locator, selectQuestion1 + "[" + q1No + "]");
			}
			if (driver.isElementAvailable(locator, question2Xpath, 1)) {
				int q2No = getDivNo(driver, cus.Question1, question2Xpath);
				// System.out.println("Quesiont No "+q1No);
				driver.OutputStream("Going to select question :" + cus.Question2 + " out of Question no 2");
				driver.click(locator, selectQuestion2 + "[" + q2No + "]");
			}
			List<WebElement> news = driver.findElements(locator, yesNo);
			if (news.size() > 0) {
				driver.scrollToElement(news.get(0), pageOfset);
				if (cus.NewsLetter)
					driver.click(news.get(0));
				else
					driver.click(news.get(1));
			} else
				driver.OutputStream("Subscription Yes/No is not available.");
			// driver.wait(1000);
			if (driver.isElementAvailable(locator, submit, 1)) {
				WebElement submitButton = driver.findElement(locator, submit, 5);
				driver.scrollToElement(submitButton, pageOfset);
				driver.click(submitButton);
				driver.waitToLoad();
			} else
				driver.OutputStream("Submit button is not vissible in the contact sales form.", false);
			driver.OutputStream("Finish inputting data in the contact sales form.");
		} catch (Exception e) {
			driver.OutputStream("Exception Triggered." + e.getMessage());
		}
		driver.wait(100);
		driver.waitToLoad();
	}

	public static int getPageOfset(Driver driver) {
		int offset = 0;
		driver.scrollTop();
		WebElement elem = null;
		if (driver.isElementAvailable(whiteBar, 2)) {
			elem = driver.findElement(whiteBar, 2);
			offset = elem.getLocation().getY() + elem.getSize().height;
		} else if (driver.isElementAvailable(locator, GreenHeaderXpath, 2)) {
			elem = driver.findElement(locator, GreenHeaderXpath, 2);
			offset = elem.getLocation().getY() + elem.getSize().height;
		} else if (driver.isElementAvailable(locator, ceraselContainer, 2)) {
			elem = driver.findElement(locator, ceraselContainer, 2);
			offset = elem.getLocation().getY();
		}
		return offset;
	}

	private static int getDivNo(Driver driver, String name, String xpath) {
		int number = 0;
		driver.click("xpath", xpath);
		driver.wait(500);
		List<WebElement> countri = driver.findElements(By.xpath(xpath + "/div[2]//div[@data-value]"));
		if (!countri.isEmpty()) {
			for (int i = 0; i < countri.size(); i++) {
				if (countri.get(i).getText().trim().equalsIgnoreCase(name.trim()))
					return i + 1;
				// System.out.println(countri.get(i).getTagName()+ " "+
				// countri.get(i).getText());
			}
		}
		driver.wait(5000);
		return number;
	}

}
