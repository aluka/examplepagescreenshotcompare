package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class InverstorRelation {
	public static String url = "";
	private static final String pageContentXpath = "//*[@class='main']";
	private static final String tabBarXpath = pageContentXpath + "//div[@class='tab-bar']";
	private static final String tabbarList = tabBarXpath + "/ul/li";
	private static final String perspectiveXpath = pageContentXpath + "//section[@class='perspectives']";
	private static String slideContainerXpath = pageContentXpath + "//div[@class='simple-slider']";
	private static String contentSlideListXpath = slideContainerXpath + "//*[@class='slides']/li";
	private static String contentSlideControllerListXpath = slideContainerXpath + "//*[@class='pagination']/li";
	private static final String FinancialResultXpath = pageContentXpath + "//div[@class='financial-results']";
	private static final String FinancialResultContent = FinancialResultXpath + "/div[@class='year-data']";
	private static final String changeFinancialYearXpath = FinancialResultXpath
			+ "//div[contains(@class,'selectize-control')]";
	private static final String selectedYearXpath = changeFinancialYearXpath
			+ "/div[contains(@class,'selectize-input')]/div";
	private static final String yearDropDownListXpath = changeFinancialYearXpath
			+ "/div[contains(@class,'selectize-dropdown')]";
	private static final String financialPresentationXpath = pageContentXpath
			+ "//div[@class='financial-presentations']";
	// private static final String ContactPressXpath =
	// pageContentXpath+"//div[@class='timeline']/div";
	// private static final String ContactFormXpath =
	// pageContentXpath+"//div[@class='customer-contact-form']";

	public static boolean validateInvestorRelations(Driver driver) {
		url = driver.getCurrentUrl();
		boolean status = validateInvestorRelationHome(driver) & validateNewInvestor(driver)
				& validateIndividualShareholder(driver) & validateFinantialResults(driver)
				& validateRagulatoryInfo(driver) & validateShareInfo(driver) // not
																				// completed
				& validateCalenderEvent(driver);
		if (status)
			driver.OutputStream("'Investor-Relations' functionality and content tested and PASSED");
		else
			driver.OutputStream("'Investor-Relations' functionality and content tested and FAILED.", false);
		return status;
	}

	public static boolean validateFinantialResults(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoFinancialResults(driver)) {
			boolean status = validateFinacialTable(driver)
			// & validateSlide(driver)
			;
			// driver.get( url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'Finanatial'. url: " + url);
			return true;
		}
	}

	public static boolean validateNewInvestor(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoNewInvestor(driver)) {
			// there is nothing to test yet in this page.
			boolean status = true;
			// driver.get( url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'New-Investor'. url: " + url);
			return true;
		}
	}

	public static boolean validateIndividualShareholder(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoIndShareHolder(driver)) {
			boolean status = validatePerspective(driver) & validateSlide(driver);
			// driver.get( url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'New-Investor'. url: " + url);
			return true;
		}
	}

	public static boolean validateInvestorRelationHome(Driver driver) {
		String url = driver.getCurrentUrl();
		boolean status = validateTab(driver)
		// there is nothing in the page yet.
		;
		driver.get(url);
		driver.scrollTop();
		return status;
	}

	public static boolean validateRagulatoryInfo(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoRegulatoryInfo(driver)) {
			boolean status = driver.readServerError();
			// There is nothing is this page.
			// validatePerspective(driver)
			// & validateSlide(driver)
			;
			// driver.get( url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'Share Information'. url: " + url);
			return true;
		}
	}

	public static boolean validateCalenderEvent(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoCalenderEvent(driver)) {
			boolean status = validateFinancialPresentation(driver);
			// There is nothing is this page.
			// validatePerspective(driver)
			// & validateSlide(driver)
			;
			// driver.get( url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'Regulatory Information'. url: " + url);
			return true;
		}
	}

	public static boolean validateShareInfo(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoShareInfo(driver)) {
			boolean status = true;
			// There is nothing is this page.
			// validatePerspective(driver)
			// & validateSlide(driver)
			;
			// driver.get( url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'Regulatory Information'. url: " + url);
			return true;
		}
	}

	private static boolean gotoNewInvestor(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 2) && isTabContentChanged(driver, url);
	}

	private static boolean gotoIndShareHolder(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 3) && isTabContentChanged(driver, url);
	}

	private static boolean gotoFinancialResults(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 4) && isTabContentChanged(driver, url);
	}

	private static boolean gotoRegulatoryInfo(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 5) && isTabContentChanged(driver, url);
	}

	private static boolean gotoShareInfo(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 6) && isTabContentChanged(driver, url);
	}

	private static boolean gotoCalenderEvent(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 7) && isTabContentChanged(driver, url);
	}

	private static boolean validateTab(Driver driver) {
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		if (isTabAvailable(driver)) {
			boolean status = true;
			int noOfTab = getNoOfTab(driver);
			driver.OutputStream("There is " + noOfTab + " tabs in this page. Continue testing.");
			for (int tabNo = 2; tabNo <= noOfTab; tabNo++) {
				String tabURL = driver.getCurrentUrl();
				status = status & clickTabNo(driver, tabNo);
				status = status & isTabContentChanged(driver, tabURL);
			}
			if (status)
				driver.OutputStream("Tab functionality tested and varrified. Test PASSED");
			else
				driver.OutputStream("Tab Functionality test FAILED.", false);
			// return to starting page
			driver.get(url);
			driver.wait(100);
			driver.waitToLoad(20, title);
			return status;
		} else {
			driver.OutputStream("There is no tab in this page.");
			return true;
		}
	}

	private static boolean isTabContentChanged(Driver driver, String oldUrl) {
		if (!driver.getCurrentUrl().equals(oldUrl)) {
			driver.OutputStream("Page URL changed. Tab Content changed and varrified.");
			return true;
		} else {
			driver.OutputStream("page URL did not change. Tab content is not varrified.");
			return false;
		}
	}

	private static boolean clickTabNo(Driver driver, int tabNo) {
		WebElement tab = driver.findElement("Xpath", tabbarList + "[" + tabNo + "]", 20);
		if (tab == null) {
			driver.OutputStream(
					"Tab no " + tabNo + " is not available. Element Identifier: " + tabbarList + "[" + tabNo + "]",
					false);
			// driver.OutputStream("");
			return false;
		}
		driver.click(tab);
		driver.waitToLoad();
		driver.wait(100);
		tab = driver.findElement("Xpath", tabbarList + "[" + tabNo + "]", 20);
		if (tab == null) {
			driver.OutputStream(
					"Tab no " + tabNo + " is not available. Element Identifier: " + tabbarList + "[" + tabNo + "]",
					false);
			// driver.OutputStream("");
			return false;
		}
		String className = tab.getAttribute("class");
		if (className.contains("selected")) {
			driver.OutputStream(tab.getText() + " is selected.");
			return true;
		} else {
			// driver.wait(100);
			driver.waitToLoad();
			tab = driver.findElement("Xpath", tabbarList + "[" + tabNo + "]", 20);
			className = tab.getAttribute("class");
			if (className.contains("selected")) {
				driver.OutputStream(tab.getText() + " is selected.");
				return true;
			} else {
				driver.OutputStream(tab.getText() + " is selected.");
				return false;
			}
		}
	}

	private static boolean isTabAvailable(Driver driver) {
		if (driver.isElementAvailable("Xpath", tabBarXpath, 5))
			return driver.findElements("Xpath", tabbarList).size() > 1;
		return false;
	}

	private static int getNoOfTab(Driver driver) {
		return driver.findElements("Xpath", tabbarList).size() - 1;
	}

	private static boolean validateFinancialPresentation(Driver driver) {
		/*
		 * validate h2 text validate p text validate multiple <li> each <li> has
		 * one <a> in there
		 */
		if (driver.isElementAvailable("Xpath", financialPresentationXpath, 5)) {
			driver.OutputStream("Financial data is present in this page. Continue Testing");
			boolean status = false;
			try {
				status = driver.findElement("Xpath", financialPresentationXpath + "//h2", 2).getText().length() > 0
						& driver.findElement("Xpath", financialPresentationXpath + "//p", 2).getText().length() > 0;
			} catch (Exception e) {
				driver.OutputStream(
						"Exception Triggered reading finantial data 'header' and 'intro'. Skip testing. Test Failed.",
						false);
			}
			int noOfDocuments = driver.findElements("Xpath", financialPresentationXpath + "/div/ul/li").size();
			if (noOfDocuments > 0) {
				driver.OutputStream("There is " + noOfDocuments + " documents in this page. Continue Testing.");
				status = validateDocuments(driver, noOfDocuments);
			} else {
				driver.OutputStream("There is no document in this page. Skip testing. Test FAILED.");
				status = false;
			}
			if (status)
				driver.OutputStream("Finacial Documents presentation is tested and PASSED.");
			else
				driver.OutputStream("Financial documents presentation is tested but FAILED.", false);
			return status;
		} else {
			driver.OutputStream("There is no finantial data present in this page. Skip testing.");
			return true;
		}
	}

	private static boolean validateDocuments(Driver driver, int noOfDoc) {
		for (int docNo = 1; docNo <= noOfDoc; docNo++) {
			if (!driver.isElementAvailable("Xpath", financialPresentationXpath + "/div/ul/li[" + docNo + "]//a", 2)) {
				driver.OutputStream("There is no finantial report doc link in link no: " + docNo + ". Test Failed.");
				return false;
			}
		}
		return true;
	}

	private static boolean validatePerspective(Driver driver) {
		if (driver.isElementAvailable("Xpath", perspectiveXpath, 5)) {
			return validatePerspectivesColumn(driver) & validateLoadMore(driver);
		} else {
			driver.OutputStream("There is no perspective in this page. Skip Test.");
			return false;
		}
	}

	private static boolean validateLoadMore(Driver driver) {
		int numberOfArticlesBefore = getNumOfPerspectiveArticles(driver);
		if (!clickLoadMore(driver)) {
			driver.OutputStream("Cant click on 'LOadMore' button. Continue testing.");
			return true;
		}
		int numberOfArticlesAfter = getNumOfPerspectiveArticles(driver);
		if (numberOfArticlesAfter > numberOfArticlesBefore) {
			driver.OutputStream("Perspective articles load more test PASSED");
			return true;
		} else {
			driver.OutputStream("Perspective articles load more test FAILED", false);
			return false;
		}
	}

	private static int getNumOfPerspectiveArticles(Driver driver) {
		return driver.findElements("Xpath", perspectiveXpath + "//article").size();
	}

	private static boolean clickLoadMore(Driver driver) {
		if (!driver.isElementAvailable("Xpath", perspectiveXpath + "/*[@class='load']", 2)) {
			driver.OutputStream("There is no 'loadMore' button in this perspective.");
			return false;
		}
		driver.click("Xpath", perspectiveXpath + "/*[@class='load']");
		driver.wait(100);
		driver.waitToLoad();
		return true;
	}

	private static boolean validatePerspectivesColumn(Driver driver) {
		int size = driver.findElements("Xpath", perspectiveXpath + "/div[@class='column-article']").size();
		if (size == 2) {
			driver.OutputStream("There is 2 column in perspectives. As expected. Test PASSED.");
			return true;
		} else {
			driver.OutputStream("There is " + size + " column in perspectives. Expected is 2. Test FAILED", false);
			return false;
		}
	}

	private static boolean validateFinacialTable(Driver driver) {
		/*
		 * If Finantial table is vissible continue testing
		 */
		if (validateFinantialTableContent(driver)) {
			driver.OutputStream(
					"Finacial result table content varrified. Continue Testing. Going to change financial year.");
			boolean status = validateFinantialTableYear(driver);
			status = status & changeFinacialYear(driver);
			status = status & validateFinantialTableContent(driver);
			if (status) {
				driver.OutputStream(
						"Financial result talbe contetnt varrified for changed year. Finish testing finantial Table. Test PASSED");
				return true;
			} else {
				driver.OutputStream(
						"Finantial Result Table content is not varrified after changing year. Test FAILED.");
				return false;
			}
		} else {
			driver.OutputStream("Financial result table content is not varrified. Test FAILED", false);
			return false;
		}
		// return true;
	}

	private static boolean validateFinantialTableYear(Driver driver) {
		boolean status = true;
		try {
			int selectedYear = readCurrentYearInCalender(driver);
			int[] yearsListed = readListOfYears(driver);
			driver.OutputStream("Selected year is: " + selectedYear);
			// driver.OutputStream("Listed years are: "+yearsListed.length);
			for (int years : yearsListed) {
				status = selectedYear >= years;
			}
			// status = true;
		} catch (Exception e) {
			status = false;
		}
		if (status)
			driver.OutputStream("Selected year is highest in the list. Test Passed.");
		else
			driver.OutputStream("Selected year is not highest in the list. Test Failed.");
		return status;
	}

	private static int readCurrentYearInCalender(Driver driver) {
		try {
			String selectedYear = driver.findElement("Xpath", changeFinancialYearXpath + "//*[@class='item']", 2)
					.getText();
			if (selectedYear != null) {
				return Integer.parseInt(selectedYear);
			} else
				return 0;
		} catch (Exception e) {
			return 0;
		}
	}

	private static int[] readListOfYears(Driver driver) {
		driver.click("Xpath", changeFinancialYearXpath);
		driver.wait(200);
		int NoOfYears = driver.findElements("Xpath", yearDropDownListXpath + "/div/div").size();
		int[] years = new int[NoOfYears];
		// int YearCount= 0;
		List<WebElement> yearsEle = driver.findElements("Xpath", yearDropDownListXpath + "/div/div");
		for (int count = 0; count < yearsEle.size(); count++) {
			if (yearsEle.get(count).getText().contains("Select"))
				continue;
			try {
				// System.out.println("Year: "+yearsEle.get(count).getText());
				years[count] = Integer.parseInt(yearsEle.get(count).getText());
				// YearCount++;
			} catch (Exception e) {
				// YearCount++;
				continue;
			}
		}
		driver.click("Xpath", changeFinancialYearXpath);
		return years;
	}

	private static boolean validateFinantialTableContent(Driver driver) {
		/*
		 * contain one h3 contain one article
		 **/
		if (driver.isElementAvailable("Xpath", FinancialResultXpath, 3)) {
			boolean status = false;
			int tableNo = visibleTableNo(driver);
			if (tableNo == 0) {
				driver.OutputStream("There is no table visible in this page. test FAILED", false);
				return false;
			}
			status = driver.findElement("Xpath", FinancialResultContent + "[" + tableNo + "]" + "//h3", 5).getText()
					.length() > 0;
			status = status & driver.findElement("Xpath", FinancialResultContent + "[" + tableNo + "]" + "//article", 5)
					.getText().length() > 0;
			if (status)
				driver.OutputStream("Financial results header is visible in the page.");
			else
				driver.OutputStream("Financial results header is not visible in the page.", false);
			if (validateTableContent(driver, tableNo))
				driver.OutputStream("Table content is validated.");
			else {
				driver.OutputStream("Table content is not validated.", false);
				status = false;
			}
			return status;
		} else {
			driver.OutputStream("There is no financial result in this page. Skip testing.", false);
			return false;
		}
	}

	private static int visibleTableNo(Driver driver) {
		int noOfTables = driver.findElements("Xpath", FinancialResultContent).size();
		System.out.println("There is " + noOfTables + " table in this page.");
		for (int tableNo = 1; tableNo <= noOfTables; tableNo++) {
			WebElement element = driver.findElement("Xpath", FinancialResultContent + "[" + tableNo + "]", 2);
			if (element == null)
				continue;
			// System.out.println(element);
			// System.out.println("display value:
			// "+element.getCssValue("display"));
			// System.out.println(tableNo+" style value:
			// "+element.getAttribute("style"));
			if (element.getCssValue("display").contains("block")) {
				System.out.println("Visible table no is: " + tableNo);
				return tableNo;
			}
		}
		return 0;
	}

	private static boolean validateTableContent(Driver driver, int tableNo) {
		boolean row1st = true, column1st = true, others = true;
		int noOfColumn = driver
				.findElements("Xpath", FinancialResultContent + "[" + tableNo + "]" + "//table/tbody/tr[1]/th").size();
		int noOfRow = driver.findElements("Xpath", FinancialResultContent + "[" + tableNo + "]" + "//table/tbody/tr")
				.size();
		driver.OutputStream("Table size is " + noOfColumn + "x" + noOfRow + ".");
		boolean status = noOfRow > 0 && noOfColumn > 0;
		// all the content in the 1st row is text
		for (int columnNo = 1; columnNo <= noOfColumn; columnNo++) {
			row1st = row1st & driver.findElement("Xpath",
					FinancialResultContent + "[" + tableNo + "]" + "//table/tbody/tr[1]/th[" + columnNo + "]", 3)
					.getText().length() > 0;
		}
		if (row1st)
			driver.OutputStream("Table header has text in Visible.");
		else
			driver.OutputStream("Not all the content in the header has text visible.", false);
		// all the content in 1st colume is text
		for (int RowNo = 1; RowNo <= noOfRow; RowNo++) {
			column1st = column1st & driver.isElementAvailable("Xpath",
					FinancialResultContent + "[" + tableNo + "]" + "//table/tbody/tr[" + RowNo + "]/td[1]", 3);
			if (column1st)
				System.out.println(driver.findElement("Xpath",
						FinancialResultContent + "[" + tableNo + "]" + "//table/tbody/tr[" + RowNo + "]/td[1]", 3)
						.getText());
		}
		if (column1st)
			driver.OutputStream("Table content time duration info is visible.");
		else
			driver.OutputStream("Not all Table content time duration info is visible.", false);
		// other row and colum box is icon. a link element
		for (int rowCount = 2; rowCount <= noOfRow; rowCount++) {
			for (int columnCount = 2; columnCount <= noOfColumn; columnCount++) {
				others = others & driver.isElementVissible("Xpath", FinancialResultContent + "[" + tableNo + "]"
						+ "//table/tbody/tr[" + rowCount + "]/td[" + columnCount + "]/a", 1);
			}
		}
		if (others)
			driver.OutputStream("Other data is also valid.");
		else
			driver.OutputStream("Other data is not valid. ", false);
		return status & row1st & column1st & others;
	}

	private static boolean changeFinacialYear(Driver driver) {
		// click on selector
		driver.click("Xpath", changeFinancialYearXpath);
		driver.wait(200);
		// driver.waitToLoad(driver);
		// driver.wait(100);
		// select 2nd from the last
		if (driver.isElementAvailable("Xpath", yearDropDownListXpath, 5)) {
			String selectedValue, expectedValue;
			driver.OutputStream("Year menu drop-down. Going to select last year.");
			List<WebElement> years = driver.findElements("Xpath", yearDropDownListXpath + "/div/div");
			// 1st data is select year.
			if (years.size() > 2) {
				expectedValue = years.get(years.size() - 2).getText();
				driver.click(years.get(years.size() - 2)); // size is 1+ of
															// index
			} else {
				expectedValue = years.get(years.size() - 1).getText();
				driver.click(years.get(years.size() - 1));
			}
			driver.wait(100);
			selectedValue = driver.findElement("Xpath", selectedYearXpath, 10).getText();
			if (selectedValue == null || expectedValue == null) {
				driver.OutputStream("Could not read selected year. The year may not been selected.", false);
				return false;
			}
			return selectedValue.contains(expectedValue) || expectedValue.contains(selectedValue);
		} else {
			driver.OutputStream("Year menu did not drop-down.", false);
			return false;
		}
	}

	private static boolean validateSlide(Driver driver) {
		String locator = "Xpath";
		boolean status = true;
		int noOfSlides, noOfControllers;
		if (driver.isElementAvailable(locator, contentSlideListXpath, 2)) {
			int NoOfSlideContainer = getSlideContainerNo(driver);
			if (NoOfSlideContainer > 1) {
				driver.OutputStream(
						"There is " + NoOfSlideContainer + " slide container in this page. Continue testing.");
				for (int containerNo = 1; containerNo <= NoOfSlideContainer; containerNo++) {
					selectSlideContainerNo(containerNo);
					noOfSlides = driver.findElements(locator, contentSlideListXpath).size();
					noOfControllers = driver.findElements(locator, contentSlideControllerListXpath).size();
					driver.OutputStream("There is " + noOfSlides + " slides in this page. Continue with testing.");
					status = (noOfSlides > 0);
					if (noOfSlides > 1)
						status = status & (noOfSlides == noOfControllers);
					for (int slideNo = 1; slideNo <= noOfSlides; slideNo++)
						status = status & validateContentSlideNo(driver, slideNo);
					driver.OutputStream("Slide container no " + containerNo + " is Tested.");
				}
			} else {
				noOfSlides = driver.findElements(locator, contentSlideListXpath).size();
				noOfControllers = driver.findElements(locator, contentSlideControllerListXpath).size();
				driver.OutputStream("There is " + noOfSlides + " slides in this page. Continue with testing.");
				status = (noOfSlides > 0);
				if (noOfSlides > 1)
					status = status & (noOfSlides == noOfControllers);
				for (int slideNo = 1; slideNo <= noOfSlides; slideNo++)
					status = status & validateContentSlideNo(driver, slideNo);
			}
		}
		return status;
	}

	private static int getSlideContainerNo(Driver driver) {
		return driver.findElements("Xpath", slideContainerXpath).size();
	}

	private static void selectSlideContainerNo(int slideNo) {
		slideContainerXpath = pageContentXpath + "//div[@class='simple-slider']" + "[" + slideNo + "]";
		contentSlideListXpath = slideContainerXpath + "//*[@class='slides']/li";
		contentSlideControllerListXpath = slideContainerXpath + "//*[@class='pagination']/li";
	}

	private static boolean validateContentSlideNo(Driver driver, int slideNo) {
		/*
		 * Check for 4 conditions title contains text peragraph contains text
		 * one link is vissible one image is vissible
		 **/
		int pageOfset = HomePage.getPageOfset(driver);
		String locator = "Xpath";
		boolean status = false;
		driver.OutputStream("Start Testing slide no " + slideNo);
		try {
			WebElement controller = driver.findElement(locator, contentSlideControllerListXpath + "[" + slideNo + "]",
					2);
			driver.scrollToElement(controller, pageOfset + 15);
			if (driver.isElementAvailable(locator, contentSlideControllerListXpath + "[" + slideNo + "]", 2))
				clickOnSlideController(driver, slideNo);
			status = driver.findElement(locator, contentSlideListXpath + "[" + slideNo + "]//*[@class='title']", 4)
					.getText().length() > 0
					& driver.findElement(locator, contentSlideListXpath + "[" + slideNo + "]//p", 1).getText()
							.length() > 0;
			// & driver.isElementAvailable( locator,
			// contentSlideListXpath+"["+slideNo+"]//a", 1);
			// & driver.isElementAvailable( locator,
			// contentSlideListXpath+"["+slideNo+"]//img", 1);
			if (status)
				driver.OutputStream("PASSED");
			else
				driver.OutputStream("Slide no " + slideNo + " test FAILED", false);

		} catch (Exception e) {
			driver.OutputStream("Exception Triggered. During testing slide no " + slideNo);
		}
		return status;
	}

	private static boolean clickOnSlideController(Driver driver, int slideNo) {
		// driver.click( "Xpath",
		// contentSlideControllerListXpath+"["+slideNo+"]");
		WebElement tab = driver.findElement("Xpath", contentSlideControllerListXpath + "[" + slideNo + "]", 5);
		if (tab == null) {
			driver.OutputStream("Slide controller no " + slideNo + " is not available. Element Identifier: "
					+ contentSlideControllerListXpath + "[" + slideNo + "]", false);
			return false;
		}
		driver.click(tab);
		driver.waitToLoad();
		driver.wait(100);
		tab = driver.findElement("Xpath", contentSlideControllerListXpath + "[" + slideNo + "]", 20);
		String className = tab.getAttribute("class");
		if (className.contains("selected")) {
			driver.OutputStream(tab.getText() + " is selected.");
			return true;
		} else {
			driver.waitToLoad();
			tab = driver.findElement("Xpath", contentSlideControllerListXpath + "[" + slideNo + "]", 20);
			className = tab.getAttribute("class");
			if (className.contains("current")) {
				driver.OutputStream(tab.getText() + " is selected.");
				return true;
			} else {
				driver.OutputStream(tab.getText() + " is selected.");
				return false;
			}
		}

	}
}
