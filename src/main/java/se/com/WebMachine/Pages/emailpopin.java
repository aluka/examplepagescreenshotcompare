package se.com.WebMachine.Pages;

import org.openqa.selenium.By;

import se.com.WebMachine.Tools.Driver;

public class emailpopin {
	private static final String formXpath = "//*[@id='cboxLoadedContent']/div[1]/div/div/form";
	private static final By EmailInputBox = By.xpath(formXpath + "/input[@type='email']");
	private static final By submitButton = By.xpath(formXpath + "/input[@class='submit-button']");
	private static final By MassageBox = By.id("cboxLoadedContent");
	private static final By continueButton = By.xpath("//button[@id='cboxClose']");
	private static final String reviewXpath = "//*[@id='no']";
	private static String[] validEmail = { "divyatest@yopmail.com" };
	private static String[] invalidEmail = { "divyatestinvalidyopmail.com", "divyatestinvalidyopmail.com" };

	public static boolean validateEmailPopin(Driver driver, String browserName) {
		String url = driver.getCurrentUrl();
		boolean status = validateEmailPopin(url, validEmail[0], true, browserName);
		status = status & validateEmailPopin(url, invalidEmail[0], false, browserName);
		status = status & validateEmailPopin(url, invalidEmail[1], false, browserName);
		return status;
	}

	public static boolean validateEmailPopin(Driver driver) {
		String url = driver.getCurrentUrl();
		String browserName = "Chrome";
		boolean status = validateEmailPopin(url, validEmail[0], true, browserName);
		status = status & validateEmailPopin(url, invalidEmail[0], false, browserName);
		status = status & validateEmailPopin(url, invalidEmail[1], false, browserName);
		return status;
	}

	public static boolean validateEmailPopin(String url, String email, boolean valid, String browserName) {
		boolean status = false, successfull = false;
		String bodyText = "";
		Driver driver = new Driver();
		driver.get(url);
		driver.wait(2000);
		try {
			driver.click("Xpath", reviewXpath);
			driver.findElement(EmailInputBox, 60).sendKeys(email);
			driver.wait(200);
			driver.click(submitButton);
			driver.wait(500);
			bodyText = driver.findElement(MassageBox, 5).getText();
			System.out.println(bodyText);
			driver.wait(100);
			driver.click(continueButton);
			driver.wait(400);
		} catch (Exception e) {
			driver.OutputStream(" Error triggered during running email submission. " + e.getMessage(), false);
		}
		successfull = bodyText.contains("Thank you");
		if (successfull == valid) {
			driver.OutputStream("Email popin Test Passed");
			status = true;
		} else {
			System.out.println("Email popin Test Failed");
			status = false;
		}
		driver.close();
		return status;
	}

	public static boolean validateSecondSubmission(String url, String validEmail, String invalidEmail,
			String browserName) {
		boolean status = false, successfull = false;
		String bodyText = "";
		Driver driver = new Driver();
		driver.get(url);
		driver.wait(2000);
		try {
			driver.click("Xpath", reviewXpath);
			driver.findElement(EmailInputBox, 60).sendKeys(invalidEmail);
			driver.wait(200);
			driver.click(submitButton);
			bodyText = driver.findElement(MassageBox, 5).getText();
			driver.OutputStream("Finish submitting invalid email id.");
			System.out.println("Showing massage is: " + bodyText);
			successfull = !bodyText.contains("Thank you");
			driver.wait(500);
			driver.findElement(EmailInputBox, 60).clear();
			driver.findElement(EmailInputBox, 60).sendKeys(validEmail);
			driver.wait(200);
			driver.click(submitButton);
			driver.wait(500);
			bodyText = driver.findElement(MassageBox, 5).getText();
			driver.OutputStream("Finish submitting valid email id.");
			System.out.println("Showing massage is: " + bodyText);
			successfull = successfull & bodyText.contains("Thank you");
			driver.wait(100);
			driver.click(continueButton);
			driver.wait(400);
		} catch (Exception e) {
			driver.OutputStream(" Error triggered during running email submission. " + e.getMessage(), false);
		}
		if (successfull) {
			driver.OutputStream("Email popin Test Passed");
			status = true;
		} else {
			System.out.println("Email popin Test Failed");
			status = false;
		}
		driver.close();
		return status;
	}
}
