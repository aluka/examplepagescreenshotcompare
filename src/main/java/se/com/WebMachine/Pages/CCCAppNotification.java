package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class CCCAppNotification {

	private static final String CCCAppNotiXpath = "//*[@id='ccc-app-notification']/div";
	private static final String AppInfoXpath = CCCAppNotiXpath + "/div[@class='app-info']";
	private static final String AppIconXpath = CCCAppNotiXpath + "/div[@class='application-icons']/a";

	public static boolean validateCccAppNoti(Driver driver, String appStore) {
		boolean status = true;
		if (isNotiVisible(driver)) {
			printInfo(driver);
			status = validateIcon(driver, appStore);
		}
		return status;
	}

	private static boolean isNotiVisible(Driver driver) {
		boolean status = false;
		if (driver.isElementAvailable("Xpath", CCCAppNotiXpath, 5)) {
			Dimension size = driver.findElement("Xpath", CCCAppNotiXpath, 5).getSize();
			status = size.height > 0 && size.width > 0;
			if (status)
				driver.OutputStream("CCC app notification is vissible in the page.");
		} else {
			driver.OutputStream("CCC app notification is not vissible.", false);
		}
		return status;
	}

	private static void printInfo(Driver driver) {
		String info = null;
		try {
			info = driver.findElement("Xpath", AppInfoXpath, 10).getText();
		} catch (Exception e) {
		}
		if (info != null)
			driver.OutputStream("CCC app notification text is: " + info);
		else
			driver.OutputStream("There is no CCC app notification text.", false);
	}

	private static boolean validateIcon(Driver driver, String StoreName) {
		boolean status = true;
		List<WebElement> Icons = driver.findElements("Xpath", AppIconXpath);
		for (WebElement icon : Icons) {
			Dimension size = icon.getSize();
			if (size.height > 0 && size.width > 0) {
				driver.OutputStream(icon.getText() + "is Vissible");
			}
		}
		return status;
	}

}
