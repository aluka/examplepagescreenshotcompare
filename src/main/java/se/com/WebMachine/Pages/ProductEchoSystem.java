package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Pages.HomePage;
import se.com.WebMachine.Tools.Driver;


/**Contain Scrip to test product echo system page interface. Contains bug.*/
@SuppressWarnings("unused")
public class ProductEchoSystem {
	private static String headerSectionXpath = "//ul[@class='tier-one']/li[1]";
	private static String headerSelectAllXpath = headerSectionXpath + "//h3";
	private static String cetagoryNameInPageXpath = "//div[@class='tagline']";
	private static String productLinkXpath = headerSectionXpath + "//li[not(@style) and not(@class)]";
	private static String productFeaturedBoxXpath = "//div[@class='featured-block']"; // subclass
																						// is------
																						// /ul/li[not(@class)]
	private static String level2Xpath = headerSectionXpath + "//a[@class='cat-title']";
	private static By productSearch = By.className("product-search");
	private static String productTipsElementXpath = "//ul[@class='tips-section support-contact sub-ul']/li";
	// 1. Reach us, 2. Distributor Locator, 3. Need Information? ----- h3, span,
	// a
	private static String allProductWhiteBoxXpath = "//*[@class='just-links']";
	private static String url;
	private static String title;

	public static boolean PEStest(Driver driver, int priority) {
		boolean status = true;
		url = driver.getCurrentUrl();
		title = driver.getTitle();
		try {
			if (driver.isElementAvailable("id", "header-content-v1")) {
				status = checkAllProduct(driver);
				if (priority > 1)
					status = status & checkCetagory(driver);
				if (priority > 2)
					status = status & checkAllLink(driver);
			} else {
				status = false;
				driver.OutputStream("This page does not contain the new header. url: " + driver.getCurrentUrl()
						+ " . This code scope does not cover old header product echo system test.", false);
			}
		} catch (ElementNotVisibleException e) {
			driver.OutputStream("Error: " + e, false);
		} catch (NoSuchElementException e) {
			driver.OutputStream("Error: " + e, false);
		} catch (Exception e) {
			status = false;
			driver.OutputStream("Error occured during testing. url: " + driver.getCurrentUrl(), false);
			driver.OutputStream("Error:  " + e, false);
		}
		return status;
	}

	public static boolean checkAllProduct(Driver driver) {
		boolean status = true;
		String locator = "Xpath";
		try {
			status = driver.isElementAvailable(locator, headerSectionXpath);
			if (status) {
				driver.click(locator, headerSectionXpath);
				int exceptedWhiteBoxNo = driver.findElements(locator, level2Xpath).size() - 1;
				driver.wait(200);
				driver.click(locator, headerSelectAllXpath);
				driver.wait(200);
				driver.switchTab();
				/*----------------------- Start testing the page-----------------------*/
				status = status & HomePage.priority1Test(driver);
				status = status & driver.isElementAvailable(productSearch);
				if (status) {
					driver.OutputStream("Search input is available inside the page.");
				} else {
					driver.OutputStream("Warning!!!!! Search input is not visible in the page.", false);
				}
				if (driver.isElementAvailable(locator, allProductWhiteBoxXpath)) {
					int accualWhiteBoxNo = driver.findElements(By.xpath(allProductWhiteBoxXpath)).size();
					status = status & (accualWhiteBoxNo >= exceptedWhiteBoxNo);
					driver.OutputStream(
							"There is " + driver.findElements(locator, allProductWhiteBoxXpath + "//a").size()
									+ " product links in this page.");
					driver.OutputStream("Expected number of white boxes is " + exceptedWhiteBoxNo + ", original is "
							+ accualWhiteBoxNo);
				} else {
					driver.OutputStream("White box is not visible in the page. url: " + driver.getCurrentUrl(), false);
					status = false;
				}
				status = status & tipsElementCheck(driver);
				/*-----------------------end testing the page------------------------*/
				driver.closeMultipleTabs();
				driver.get(url);
				driver.waitToLoad(title);
			} else {
				driver.OutputStream("Warning!!! Product is not accesable in green header.", false);
			}
		} catch (Exception e) {
			driver.OutputStream("Error validating PES menu. Error: " + e, false);
			status = false;
		}
		return status;
	}

	private static boolean checkCetagory(Driver driver) {
		boolean status = true;
		String locator = "Xpath";
		WebElement product = driver.findElement(locator, headerSectionXpath, 15);
		product.click();
		List<WebElement> cetagoryList = driver.findElements(locator, level2Xpath);
		driver.OutputStream("Check cetagory. No of elements is: " + cetagoryList.size());
		for (int cetagoryNo = 0; cetagoryNo < cetagoryList.size() - 1; cetagoryNo++) {
			// driver.OutputStream("Level 3 link name is:
			// "+cetagoryList.get(cetagoryNo).getText());
			if (cetagoryList.get(cetagoryNo).getText().length() > 0) {
				driver.OutputStream(
						"--------------------------------------------------------------------------------------------------");
				driver.OutputStream("Name of the element is: " + cetagoryList.get(cetagoryNo).getText());
				cetagoryList.get(cetagoryNo).click();
				driver.wait(100);
				driver.switchTab();
				/*----------------------Test functionality---------------------*/
				driver.OutputStream("catagory name in the page is: "
						+ driver.findElement(locator, cetagoryNameInPageXpath, 15).getText());
				status = status & HomePage.priority1Test(driver);
				status = status & featuredBoxElementCheck(driver);
				// checkPageContents(driver);
				/*--------------------------testing criteria end----------------*/
				driver.wait(100);
				driver.closeMultipleTabs();
				driver.get(url);
				driver.waitToLoad(title);
				driver.wait(100);
				driver.OutputStream(
						"--------------------------------------------------------------------------------------------------");
				product = driver.findElement(locator, headerSectionXpath, 15);
				product.click();
				cetagoryList = driver.findElements(locator, level2Xpath);
			}
		}
		return status;
	}

	private static boolean checkAllLink(Driver driver) {
		boolean status = true;
		String locator = "xpath";
		driver.OutputStream("Start Testing all links.........");
		status = driver.isElementAvailable(locator, headerSectionXpath);
		// driver.click( locator, headerSectionXpath);
		if (status) {
			try {
				// click product
				driver.click(locator, headerSectionXpath);
				driver.wait(200);
				List<WebElement> productLink = driver.findElements(locator, productLinkXpath);
				for (int productNo = 1; productNo < (productLink.size() - 1) && productLink.size() > 1; productNo++) {
					// String link =
					// productLink.get(productNo).getAttribute("href");
					// if(link.contains("http://")){
					if (true) {
						driver.wait(100);
						productLink.get(productNo).click();
						driver.wait(100);
						driver.switchTab();
						// -----------------------page content Test
						// start---------------------//
						boolean state = HomePage.priority1Test(driver);
						state = state & featuredBoxElementCheck(driver);
						tipsElementCheck(driver);
						// ---------------------- page content test
						// end---------------------//
						driver.closeMultipleTabs();
						driver.get(url);
						driver.wait(100);
						driver.waitToLoad(title);
						driver.wait(100);
						driver.click(locator, headerSectionXpath);
						driver.wait(100);
						productLink = driver.findElements(locator, productLinkXpath);
					}
				}
			} catch (ElementNotVisibleException e) {
				driver.OutputStream("Error: " + e, false);
			}
		} else {
			driver.OutputStream("Warning!!!! Product is not accessable", status);
		}
		return status;
	}

	private static boolean checkPageContents(Driver driver) {
		boolean status = true;
		status = status & featuredBoxElementCheck(driver);
		status = status & tipsElementCheck(driver);
		return status;
	}

	public static boolean featuredBoxElementCheck(Driver driver) {
		boolean status = true;
		String locator = "Xpath";
		if (driver.isElementAvailable(locator, productFeaturedBoxXpath)) {
			try {
				List<WebElement> featureBoxs = driver.findElements(locator, productFeaturedBoxXpath);
				for (int boxNo = 0; boxNo < featureBoxs.size(); boxNo++) {
					driver.OutputStream("There is " + featureBoxs.size() + " white feature boxes in this page.");
					String featureBoxElementRawXpath = productFeaturedBoxXpath + "[" + (boxNo + 1) + "]/ul";
					List<WebElement> featuredBoxElementRaw = driver.findElements(locator, featureBoxElementRawXpath);
					if (featuredBoxElementRaw.size() > 0) {
						driver.OutputStream(
								"There is " + featuredBoxElementRaw.size() + " raws of elements in Feature box");
						driver.OutputStream(
								"-----------------------------------------------------------------------------------------------");
						for (int rawNo = 0; rawNo < featuredBoxElementRaw.size(); rawNo++) {
							String featureBoxEleXpath = featureBoxElementRawXpath + "[" + (rawNo + 1)
									+ "]/li[not(@class)]";
							List<WebElement> featuredBoxElementCall = driver.findElements(locator, featureBoxEleXpath);
							driver.OutputStream("There is " + featuredBoxElementCall.size() + " items in raw "
									+ (rawNo + 1) + " of Feature Box.");
							for (int eleNo = 0; eleNo < featuredBoxElementCall.size(); eleNo++) {
								String name = null;
								if (driver.isElementAvailable(locator,
										featureBoxEleXpath + "[" + (eleNo + 1) + "]//em"))
									name = driver
											.findElement(locator, featureBoxEleXpath + "[" + (eleNo + 1) + "]//em", 15)
											.getText();
								status = status && name.length() > 1;
								driver.OutputStream("Name of the item no " + (eleNo + 1) + " of raw " + (rawNo + 1)
										+ " of box " + (boxNo + 1) + " is --------------- " + name);
								status = status & driver.isElementAvailable(locator,
										featureBoxEleXpath + "[" + (eleNo + 1) + "]//img");
								if (status) {
									driver.OutputStream("Name and Image are visible for this item");
								} else {
									driver.OutputStream("Either Name or Image is not visible for this item");
									// driver.takeScreenShot(
									// ""+driver.getTitle().trim());
								}
							}
						}
						driver.OutputStream(
								"-----------------------------------------------------------------------------------------------");
					} else {
						driver.OutputStream(
								"No element visible in Featured box of this page. url: " + driver.getCurrentUrl(),
								false);
						driver.takeScreenShot("NoFeatureBox" + driver.getTitle().trim());
						status = false;
					}

				}
			} catch (Exception e) {
				driver.OutputStream("WARNING!!!!  Error validating featurebox element in this page. url : "
						+ driver.getCurrentUrl(), false);
				driver.OutputStream("Error: " + e);
				driver.takeScreenShot("ErrorFeatureBox" + driver.getTitle().trim());
				status = false;
			}
		} else {
			driver.OutputStream("There is no feature box in this page. " + driver.getCurrentUrl(), false);
			driver.takeScreenShot("NoFeatureBox" + driver.getTitle().trim());
			status = false;
		}
		return status;
	}

	public static boolean tipsElementCheck(Driver driver) {
		boolean status = true;
		String locator = "Xpath";
		try {
			List<WebElement> tipsElement = driver.findElements(locator, productTipsElementXpath);
			if (tipsElement.size() > 0) {
				driver.OutputStream("There is " + tipsElement.size() + " Tips visible in this page.");
				for (int elementNo = 0; elementNo < tipsElement.size(); elementNo++) {
					try {
						status = status & driver
								.findElement(locator, productTipsElementXpath + "[" + (elementNo + 1) + "]//h3", 15)
								.getText().length() > 1;
						// driver.OutputStream("Title is : "+driver.findElement(
						// locator,
						// productTipsElementXpath+"["+(elementNo+1)+"]//h3",
						// 15).getText());
						status = status & driver
								.findElement(locator, productTipsElementXpath + "[" + (elementNo + 1) + "]//span", 15)
								.getText().length() > 1;
						// driver.OutputStream("Details is:
						// "+driver.findElement( locator,
						// productTipsElementXpath+"["+(elementNo+1)+"]//span",
						// 15).getText());
						status = status & driver
								.findElement(locator, productTipsElementXpath + "[" + (elementNo + 1) + "]//a", 15)
								.getAttribute("href").length() > 1;
						// driver.OutputStream("Link is: "+driver.findElement(
						// locator,
						// productTipsElementXpath+"["+(elementNo+1)+"]//a",
						// 15).getAttribute("href"));
						if (status) {
							driver.OutputStream("Header, Infomation and link all of them are visible for Tip no: "
									+ (elementNo + 1));
						} else {
							driver.OutputStream("Either Header, information or link of Tip no " + (elementNo + 1)
									+ " is not visible.", status);
							driver.takeScreenShot("TipsNotValid" + driver.getTitle().trim());
						}
					} catch (Exception e) {
						continue;
					}
				}
			} else {
				driver.OutputStream("No Tips element is visible in the page.", false);
				driver.takeScreenShot("TipsNotVisible" + driver.getTitle().trim());
				status = false;
			}
		} catch (Exception e) {
			driver.OutputStream("WARNING!!!!  There is no Tips visible in this page: " + driver.getCurrentUrl(), false);
			driver.takeScreenShot("TipsNotVisible" + driver.getTitle().trim());
			status = false;
		}
		return status;
	}

}
