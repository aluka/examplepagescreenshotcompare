package se.com.WebMachine.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.com.WebMachine.Tools.Driver;

@SuppressWarnings("unused")
public class VideoLinkAlignment {
	private static final String baseURL = "http://www-pre.amer1.sdl.schneider-electric.com/us/en/tests/se-example-pages/work/solutions-casestudies.jsp";
	private static final String mainBodyXpath = "//*[@class='main']";
	private static final String CaseStudyBlockXpath = mainBodyXpath + "//div[@class='case-studies']//li[not(@class)]";

	public static boolean validateLinkAlignment(Driver driver) {
		boolean status = true;
		WebDriverWait wait = new WebDriverWait(driver.getInstance(), 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(CaseStudyBlockXpath)));
		int noOfImage = driver.findElements("Xpath", CaseStudyBlockXpath).size();
		if (noOfImage > 1) {
			for (int imageNo = 0; imageNo < noOfImage - 2; imageNo = imageNo + 2) {
				int pos1st = driver.findElements("Xpath", CaseStudyBlockXpath + "//img").get(imageNo).getLocation()
						.getY();
				int pos2nd = driver.findElements("Xpath", CaseStudyBlockXpath + "//img").get(imageNo + 1).getLocation()
						.getY();
				if (!(pos1st > 0 && pos2nd > 0)) {
					driver.OutputStream("Cound not locate the images. Test Failed.", false);
					status = false;
				} else if (pos1st == pos2nd) {
					driver.OutputStream("Both elements are in the same horizental position. Test Passed");
				} else {
					driver.OutputStream("Not both the element position is same. Test Failed.");
					status = false;
				}
			}
			return status;
		} else {
			driver.OutputStream("There is not enough 'case Study' block in this page to test the alinment.");
			return true;
		}
	}
}
