package se.com.WebMachine.Pages;

import se.com.WebMachine.Tools.Driver;

public class Sitemap {
	private static final String GreenHeaderMenuXpath = "//*[@class='main-menu']/li[@class='nav']/ul[@class='tier-one']/li";
	private static final String SiteMapContentXpath = "//*[@class='sitemap']/ul/li";
	private static int menuNo = 0;

	public static boolean validateSiteMapContent(Driver driver) {
		return validateNoOfSection(driver) && validateNoOfLinks(driver);
	}

	private static boolean validateNoOfLinks(Driver driver) {
		boolean status = true;
		int NoOfSection = driver.findElements("Xpath", GreenHeaderMenuXpath).size();
		for (menuNo = 1; menuNo <= NoOfSection; menuNo++) {
			int linkInMenu = noOfL3LinksInMegaMenu(driver, menuNo);
			int linkInPage = noOfL3LinksInPage(driver, menuNo);
			driver.OutputStream("Links in the menu is " + linkInMenu + " and links in page is " + linkInPage + ".");
			if (linkInMenu != linkInPage) {
				if (Math.abs(linkInMenu - linkInPage) < 3) {
					driver.OutputStream(
							"Links in the menu no " + menuNo
									+ " and in the page is not same, But still with in Critical range. Test PASSED.",
							false);
					status = true;
				} else {
					status = false;
					driver.OutputStream("Links in the menu no " + menuNo + " and in the page is not same, Test FAILED",
							false);
				}
			}
		}
		return status;
	}

	private static int noOfL3LinksInMegaMenu(Driver driver, int MenuNo) {
		int NoOfLinks = 0;
		driver.click("Xpath", GreenHeaderMenuXpath + "[" + MenuNo + "]");
		driver.wait(500);
		if (driver.isElementAvailable("Xpath", GreenHeaderMenuXpath + "[" + MenuNo + "]//a", 5)) {
			NoOfLinks = driver
					.findElements("Xpath",
							GreenHeaderMenuXpath + "[" + MenuNo
									+ "]/ul//li[not(contains(@style,'display:none')) and not(contains(@class,'col-promo'))]/a")
					.size() + driver
							.findElements("Xpath",
									GreenHeaderMenuXpath + "[" + MenuNo + "]/ul/div[@class='tier-two-wrap']/h3/a")
							.size()
					- driver.findElements("Xpath",
							GreenHeaderMenuXpath + "[" + MenuNo + "]/ul/div[@class='extra-links']//a").size();
			/* [not (@class) and contains(@href,'www')] */
		}
		// driver.MoveTo( "Xpath", SElogoXpath);
		// driver.wait(100);
		return NoOfLinks;
	}

	private static int noOfL3LinksInPage(Driver driver, int MenuNo) {
		int NoOfLinks = 0;
		if (driver.isElementAvailable("Xpath", SiteMapContentXpath + "[" + MenuNo + "]//a", 5)) {
			NoOfLinks = driver.findElements("Xpath", SiteMapContentXpath + "[" + MenuNo + "]//a").size();
			/* [contains(@href,'www')] */
		}
		return NoOfLinks;
	}

	private static boolean validateNoOfSection(Driver driver) {
		int NoOfSectionInMenu = 0, NoOfSectionInPage = 0;
		if (driver.isElementAvailable("Xpath", GreenHeaderMenuXpath, 2)) {
			NoOfSectionInMenu = driver.findElements("Xpath", GreenHeaderMenuXpath).size();
		}
		if (driver.isElementAvailable("Xpath", SiteMapContentXpath, 2)) {
			NoOfSectionInPage = driver.findElements("Xpath", SiteMapContentXpath).size();
		}
		if (NoOfSectionInMenu == NoOfSectionInPage && NoOfSectionInMenu != 0 && NoOfSectionInPage != 0) {
			driver.OutputStream("There is " + NoOfSectionInMenu + " section in the main menu and " + NoOfSectionInPage
					+ " sections in the page. As Expected. Test Passed.");
			return true;
		} else {
			driver.OutputStream("There is " + NoOfSectionInMenu + " section in the main menu and " + NoOfSectionInPage
					+ " sections in the page. Not as Expected. Test Failed.", false);
			return false;
		}
	}
}
