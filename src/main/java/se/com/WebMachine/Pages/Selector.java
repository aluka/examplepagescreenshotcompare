package se.com.WebMachine.Pages;

import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class Selector {
	private static String SelectorKeyWordXpath = "//div[@class='country-selector']";
	private static String ContinentXpath = SelectorKeyWordXpath + "/ul[@class='language-details']/li";

	public static boolean validateSelectorKeyword(Driver driver) {
		if (isKeywordActive(driver)) {
			int numberOfContinents = getNumberOfContinents(driver);
			boolean status = numberOfContinents > 0;
			driver.OutputStream("There is " + numberOfContinents + " continents in this keyword selector.");
			String selectedList = ReadRightSideData(driver);
			status = status & selectedList.length() > 0;
			driver.OutputStream("Selected list now is: " + selectedList);
			for (int continentNo = 2; continentNo <= numberOfContinents; continentNo++) {
				selectContinentNo(driver, continentNo);
				String newSelectedList = ReadRightSideData(driver);
				driver.OutputStream("New selected list is: " + newSelectedList + ".");
				status = status & (!selectedList.contains(newSelectedList));
			}
			return status;
		} else {
			driver.OutputStream("There is no selector keyword list in this page. Skip testing.", false);
			return false;
		}
	}

	private static boolean isKeywordActive(Driver driver) {
		driver.waitToLoad();
		return driver.isElementAvailable("Xpath", SelectorKeyWordXpath, 2);
	}

	private static int getNumberOfContinents(Driver driver) {
		return driver.findElements("Xpath", ContinentXpath).size();
	}

	private static boolean selectContinentNo(Driver driver, int no) {
		String path = ContinentXpath + "[" + no + "]";
		if (driver.isElementAvailable("Xpath", path, 2)) {
			WebElement continent = driver.findElement("Xpath", path, 2);
			driver.click("Xpath", path + "/a");
			driver.wait(100);
			driver.waitToLoad();
			// WebDriverWait wait = new WebDriverWait(20);
			// wait.until(ExpectedConditions.elementToBeSelected(By.xpath(path)));
			continent = driver.findElement("Xpath", path, 5);
			if (continent.getAttribute("class").contains("selected")) {
				driver.OutputStream("Continent no " + no + " is selected.");
				return true;
			} else {
				continent = driver.findElement("Xpath", path, 5);
				driver.wait(200);
				if (continent.getAttribute("class").contains("selected")) {
					driver.OutputStream("Continent no " + no + " is selected.");
					return true;
				} else {
					driver.OutputStream("Continent no " + no + " was not able to select. Skip Testing.", false);
					return false;
				}
				// driver.OutputStream("Continent no "+no+" was not able to
				// select. Skip Testing.",false);
				// return false;
			}
		} else {
			driver.OutputStream("Cound not locate continent no " + no + ". Skip testing.", false);
			return false;
		}
	}

	private static String ReadRightSideData(Driver driver) {
		String path = ContinentXpath + "[@class='selected']/ul";
		if (driver.isElementAvailable("Xpath", path, 3)) {
			return driver.findElement("Xpath", path, 5).getText();
		} else {
			driver.OutputStream("There is no continent selected. cant read data from right side list.", false);
			return null;
		}
	}

}
