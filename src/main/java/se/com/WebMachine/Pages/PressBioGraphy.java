package se.com.WebMachine.Pages;

import se.com.WebMachine.Tools.Driver;

public class PressBioGraphy {

	private static final String CommitteeTextXpath = "//div[@class='main']/div/h2[contains(text(),'Executive committee')]";
	private static final String committeeTipsXpath = "//div[@class='main']/div/ul[@class='tips-section flexible']";
	private static String liderShipListXpath = committeeTipsXpath + "/li";

	public static boolean validateCorporateBio(Driver driver) {
		boolean status = false;
		if (isBoxThere(driver)) {
			driver.OutputStream("This page contains information about Corporate leaders of 'SE'.");
			status = true;
			int noOfLeaders = numberOfLeaders(driver);
			driver.OutputStream(noOfLeaders + " leaders information listed here.");
			for (int leaderNo = 1; leaderNo <= noOfLeaders; leaderNo++) {
				if (ItemContainsImage(driver, leaderNo) && ItemContainsTitle(driver, leaderNo)
						&& ItemContainsDesignation(driver, leaderNo)) {
					driver.OutputStream("Leader list item " + leaderNo + " contains all the information.");
				} else {
					driver.OutputStream(
							"Leader list item " + leaderNo + " does not have all the informations. Test failed", false);
					status = false;
				}
			}
			if (status)
				driver.OutputStream("Corporate-Press-Bio Test PASSED.");
			else
				driver.OutputStream("Corporate-Press-Bio Test FAILED.", false);
		} else {
			driver.OutputStream("There is no executive bio in this page. Skip testing.");
			return true;
		}
		return status;
	}

	private static boolean isBoxThere(Driver driver) {
		return driver.isElementAvailable("Xpath", CommitteeTextXpath, 5);
	}

	private static int numberOfLeaders(Driver driver) {
		return driver.findElements("Xpath", liderShipListXpath).size();
	}

	private static boolean ItemContainsImage(Driver driver, int itemNo) {
		return driver.isElementAvailable("Xpath", liderShipListXpath + "[" + itemNo + "]//*[@class='picture']//img", 5);
	}

	private static boolean ItemContainsTitle(Driver driver, int itemNo) {
		return driver.findElement("Xpath", liderShipListXpath + "[" + itemNo + "]//h3", 5).getText().length() > 0;
	}

	private static boolean ItemContainsDesignation(Driver driver, int itemNo) {
		return driver.findElement("Xpath", liderShipListXpath + "[" + itemNo + "]//span[not(@class)]", 5).getText()
				.length() > 0;
	}

}
