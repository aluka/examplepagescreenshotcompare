package se.com.WebMachine.Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class StickyBar {

	public static final String StickyBarBoxXpath = "//*[@id='support-bar']";
	public static final String ContactXpath = StickyBarBoxXpath + "/label/span";
	public static final String contactOptions = StickyBarBoxXpath + "//*[@id='support-bar-icons']/ul/li";

	public static boolean validateStickyBar(Driver driver, int priority) {
		return validateStickyBarBounce(driver)
				// & validateStickyBarLinks( priority)
				& validateStickyBarScroll(driver);
	}

	public static boolean validateStickyBarBounce(Driver driver) {
		boolean status = true;
		driver.navigate().refresh();
		driver.wait(100);
		WebElement StickyBar = driver.findElement("Xpath", ContactXpath, 15);
		// driver.wait(1000);
		if (StickyBar == null) {
			driver.OutputStream("There is no contact sales Sticky bar in this page. Skip this test.");
			return false;
		}
		List<Integer> position = new ArrayList<Integer>();
		driver.OutputStream("Start Reading postion of sticky bar");
		for (int posNo = 0; posNo < 100; posNo++) {
			position.add(posNo, StickyBar.getLocation().getX());
			// System.out.print(position.get(posNo)+" ");
			driver.wait(50);
		}
		driver.OutputStream("End reading position of Sticky Bar.");
		int max = getMax(position);
		int min = getMin(position);
		driver.OutputStream(
				"Max Position is: " + max + ". Min Position is: " + min + ". Difference is: " + (max - min) + ".");
		status = (max - min) > 5;
		if (status)
			driver.OutputStream("Sticky Bar Bounce functionality test PASSED.");
		else
			driver.OutputStream("Sticky bar Bounch functionality test FAILED.");
		return status;
	}

	public static boolean validateStickyBarScroll(Driver driver) {
		boolean status = true;
		driver.OutputStream("Start Testing flying functionality of Sticky Bar.");
		driver.scrollTop();
		int posBefore = driver.findElement("xpath", ContactXpath, 20).getLocation().getY();
		driver.scrollBottom();
		driver.wait(100);
		int posAfter = driver.findElement("xpath", ContactXpath, 20).getLocation().getY();
		status = posBefore != posAfter;
		driver.OutputStream(
				"Position of Contact Sales before scroll is " + posBefore + " and after is " + posAfter + ".");
		if (status)
			driver.OutputStream("Sticky Bar scroll test PASSED");
		else
			driver.OutputStream("Sticky Bar scroll test FAILED", false);
		driver.navigate().refresh();
		return status;
	}

	public static boolean validateStickyBarLinks(Driver driver, int priority) {
		boolean status = true;
		boolean statusLocal;
		final String url = driver.getCurrentUrl();
		final String title = driver.getTitle();
		if (driver.isElementAvailable("Xpath", ContactXpath, 5)) {
			driver.click("xpath", ContactXpath);
			driver.wait(100);
			int noOfelements = driver.findElements("Xpath", contactOptions).size();
			Dimension size = driver.findElement("Xpath", StickyBarBoxXpath, 20).getSize();
			driver.OutputStream("The Size of sticky bar is " + size.height + "x" + size.width + ".");
			status = status && size.height > 0 && size.width > 0;
			status = status & noOfelements > 1;
			if (status)
				driver.OutputStream(
						"There is " + noOfelements + " options in this sticky ber. As expected. Continue testing.");
			else
				driver.OutputStream(
						"There is " + noOfelements
								+ " options in this sticky ber. Not with in the expected range. Continue testing.",
						false);
			for (int elementNo = 1; elementNo <= noOfelements && priority > 2; elementNo++) {
				driver.OutputStream("Going to click on number " + elementNo + " option in the sticky ber.");
				driver.click("Xpath", contactOptions + "[" + elementNo + "]");
				driver.wait(100);
				driver.switchTab();
				driver.waitToLoad();
				// Start testing the destination page
				driver.OutputStream(
						"-------------------------------Start Testing Destination Page---------------------------------------");
				statusLocal = HomePage.SmokeTest(driver);
				if (statusLocal)
					statusLocal = HomePage.priority1Test(driver);
				// Going back to test page
				if (statusLocal)
					driver.OutputStream("Sticky bar links no " + elementNo + " test PASSED");
				else
					driver.OutputStream("Sticky bar links no " + elementNo + " test FAILED");
				driver.OutputStream(
						"----------------------Finish testing Destination Page. Going back to test page.---------------------");
				driver.closeMultipleTabs();
				driver.get(url);
				driver.wait(100);
				driver.waitToLoad(title);
				driver.click("xpath", ContactXpath);
				driver.wait(100);
				status = status & statusLocal;
			}
		} else {
			driver.OutputStream("There is no sticky ber in this page.", false);
		}
		if (status)
			driver.OutputStream("Sticky bar links test PASSED");
		else
			driver.OutputStream("Sticky bar links test FAILED");
		driver.navigate().to(url);
		driver.waitToLoad(title);
		return status;
	}

	private static int getMax(List<Integer> array) {
		int max = 0;
		for (int no = 0; no < array.size(); no++) {
			if (max < array.get(no))
				max = array.get(no);
		}
		return max;
	}

	private static int getMin(List<Integer> array) {
		int min = 5000;
		for (int no = 0; no < array.size(); no++) {
			if (min > array.get(no))
				min = array.get(no);
		}
		return min;
	}
}
