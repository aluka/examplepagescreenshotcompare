package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class Partner {

	private static final String locator = "Xpath";
	private static final String mainContentSecXpath = "//*[@class='main']";
	private static final String picsXpath = mainContentSecXpath + "/div[@class='picks']";
	private static final String tilesXpath = picsXpath + "//ul[@class='tiles']";
	private static final String tipsSectionLiXpath = "//*[@class='tips-section']/li";
	public static final String homeProductBoxXpath = "//div[@id='home-products']//ul";
	public static final String homeProductListXpath = homeProductBoxXpath + "/li";
	public static final String homeProductNextButtonXpath = "//div[@id='home-products']//div[@class='right-button active']";

	public static boolean testPartner(Driver driver) {
		String baseURL = driver.getCurrentUrl();
		boolean status = false;
		if (DispatchContent.gotoPartner(driver)) {
			driver.OutputStream("In 'Partner' page. Continue with test.");
			status = Partner.validatePartnerPageContent(driver);
		} else {
			driver.OutputStream("Cant navigate to 'Partner' page.");
			return true;
		}
		driver.navigate().to(baseURL);
		driver.wait(100);
		driver.waitToLoad();
		return status;
	}

	public static boolean validatePartnerPageContent(Driver driver) {
		boolean status = true;
		HomePage.closeSubscription(driver, 3);
		status = driver.readServerError();
		status = status & DispatchContent.validateCerasel(driver);
		status = status & AtHomeContent.validateTiles(driver);
		// status = status & HomePage.priority1Test(driver);
		return status;
	}

	private static boolean validateTiles(Driver driver) {
		boolean status = true;
		if (driver.isElementAvailable(locator, tilesXpath, 2)) {
			try {
				int noOfTiles = driver.findElements(locator, tilesXpath).size();
				driver.OutputStream("There is " + noOfTiles + " Tiles in this page. Continue testing.");
				for (int tilesNo = 1; tilesNo <= noOfTiles; tilesNo++) {
					status = status & validateTileNo(driver, tilesNo);
					if (status)
						driver.OutputStream("Tile no " + tilesNo + " test complete. content test PASSED");
					else
						driver.OutputStream("Tile no " + tilesNo + " test complete. content test FAILED", false);
				}
			} catch (Exception e) {
				driver.OutputStream("Exception triggered", false);
				status = false;
			}
		} else {
			driver.OutputStream("This page dont have any Tiles. Contiue with next Test.");
		}
		return status;
	}

	private static boolean validateTileNo(Driver driver, int tileNo) {
		boolean status = true;
		boolean statusLocal;
		String tileListXpath = picsXpath + "[" + tileNo + "]//ul[@class='tiles']/li";
		List<WebElement> tileListElements = driver.findElements(locator, tileListXpath);
		int noOfItems = tileListElements.size();
		driver.OutputStream("There is " + noOfItems + " item under tile no " + tileNo + ". Continue Testing.");
		status = noOfItems > 0;
		for (int itemNo = 1; itemNo <= noOfItems; itemNo++) {
			String className = tileListElements.get(itemNo - 1).getAttribute("class");
			if (className.equalsIgnoreCase("tall-left larger")) {
				statusLocal = testTallLeftTilesContent(driver, itemNo, tileListXpath);
				if (statusLocal)
					driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
							+ " is a pic less tile. Content_Test Test PASSED");
				else {
					try {
						if (driver.isElementAvailable(locator, tileListXpath + "[" + itemNo
								+ "]/div[@class='picks-welcome' or contains(@class, 'video')]", 2)) {
							driver.OutputStream("Item no " + itemNo + " contains script. Not testable. PASSED");
							statusLocal = true;
						} else
							driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
									+ " is a Tile with pics. Content_Test Test FAILED", false);
					} catch (Exception e) {
						driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
								+ " is a pic less tile. Content_Test Test FAILED", false);
					}
				}
				status = status && statusLocal;
			} else if (className.equalsIgnoreCase("larger")) {
				statusLocal = testLargerTilesContent(driver, itemNo, tileListXpath);
				if (statusLocal)
					driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
							+ " is a Tile with pics. Content_Test Test PASSED");
				else {
					try {
						if (driver.isElementAvailable(locator, tileListXpath + "[" + itemNo
								+ "]/div[@class='picks-welcome' or contains(@class, 'video')]", 2)) {
							driver.OutputStream("Item no " + itemNo + " contains script. Not testable. PASSED");
							statusLocal = true;
						} else
							driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
									+ " is a Tile with pics. Content_Test Test FAILED", false);
					} catch (Exception e) {
						driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
								+ " is a pic less tile. Content_Test Test FAILED", false);
					}
				}
				status = status && statusLocal;
			} else if (className.length() == 0) {

				statusLocal = testLargerTilesContent(driver, itemNo, tileListXpath);
				/*
				 * if((!statusLocal) && (driver.findElement( locator,
				 * tileListXpath+"["+itemNo+"]//script", 5)!=null)) {
				 * statusLocal = true; (driver.findElement( locator,
				 * tileListXpath+"["+itemNo+"]/div[contains(@class, 'video')]",
				 * 5)!=null) }
				 */
				if (statusLocal)
					driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
							+ " is a Tile with pics. Content_Test Test PASSED");
				else {
					try {
						if (driver.isElementAvailable(locator, tileListXpath + "[" + itemNo
								+ "]/div[@class='picks-welcome' or contains(@class, 'video')]t", 2)) {
							driver.OutputStream("Item no " + itemNo + " contains script. Not testable. PASSED");
							statusLocal = true;
						} else
							driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
									+ " is a Tile with pics. Content_Test Test FAILED", false);
					} catch (Exception e) {
						driver.OutputStream("Tile no " + tileNo + " ItemNo " + itemNo
								+ " is a pic less tile. Content_Test Test FAILED", false);
					}
				}
				status = status && statusLocal;
			}
		}
		return status;
	}

	private static boolean testLargerTilesContent(Driver driver, int itemNo, String xpath) {
		boolean status = false;
		try {
			status = driver.isElementAvailable(locator, xpath + "[" + itemNo + "]//img", 2)
					&& driver.findElement(locator, xpath + "[" + itemNo + "]//em", 2).getText().length() > 0
					&& driver.isElementAvailable(locator, xpath + "[" + itemNo + "]//a", 2);
		} catch (Exception e) {
		}
		return status;
	}

	private static boolean testTallLeftTilesContent(Driver driver, int itemNo, String xpath) {
		boolean status = false;
		try {
			status = driver.findElement(locator, xpath + "[" + itemNo + "]//em", 2).getText().length() > 0
					&& driver.isElementAvailable(locator, xpath + "[" + itemNo + "]//li", 2)
					&& driver.isElementAvailable(locator, xpath + "[" + itemNo + "]//a", 2);
			status = status & driver.findElements(locator, xpath + "[" + itemNo + "]//li").size() == driver
					.findElements(locator, xpath + "[" + itemNo + "]//a").size();
		} catch (Exception e) {

		}
		return status;
	}

}
