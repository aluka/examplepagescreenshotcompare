package se.com.WebMachine.Pages;

import se.com.WebMachine.Tools.Driver;

public class SiteState {

	public static final String GsiteValue = "GTM-WG7926|+@UA-8189219-8";

	public static boolean testStatPerameters(Driver driver, String gSitevalue) {
		boolean status = true;
		String[] StatIdentifiers = readStatIdentifier(driver);
		/* varrify XTSite value */
		if (validateXTSiteIdentifier(driver, StatIdentifiers[0])) {
			driver.OutputStream("XTsite perameter is been varrified");
		} else {
			driver.OutputStream("XTsite perameter value is invalid");
			status = false;
		}
		/* varrify XTSD value */
		if (validateXTSDValue(driver, StatIdentifiers[1])) {
			driver.OutputStream("XTSD perameter is been varrified");
		} else {
			driver.OutputStream("XTSD perameter value is invalid");
			status = false;
		}
		/* varrify GSite value */
		if (validateGsiteValue(driver, StatIdentifiers[2], gSitevalue)) {
			driver.OutputStream("Gsite perameter is been varrified");
		} else {
			driver.OutputStream("Gsite perameter value is invalid");
			status = false;
		}
		return status;
	}

	public static boolean testStatPerameters(Driver driver) {
		boolean status = true;
		String[] StatIdentifiers = readStatIdentifier(driver);
		/* varrify XTSite value */
		if (validateXTSiteIdentifier(driver, StatIdentifiers[0])) {
			driver.OutputStream("XTsite perameter is been varrified");
		} else {
			driver.OutputStream("XTsite perameter value is invalid");
			status = false;
		}
		/* varrify XTSD value */
		if (validateXTSDValue(driver, StatIdentifiers[1])) {
			driver.OutputStream("XTSD perameter is been varrified");
		} else {
			driver.OutputStream("XTSD perameter value is invalid");
			status = false;
		}
		/* varrify GSite value */
		if (validateGsiteValue(driver, StatIdentifiers[2])) {
			driver.OutputStream("Gsite perameter is been varrified");
		} else {
			driver.OutputStream("Gsite perameter value is invalid");
			status = false;
		}
		return status;
	}

	private static boolean validateXTSDValue(Driver driver, String value) {
		boolean status = true;
		if (!value.contains("http://")) {
			status = false;
			driver.OutputStream("XTSDValue of this site is not valid.");
		}
		return status;
	}

	private static boolean validateGsiteValue(Driver driver, String value, String expected) {
		boolean status = true;
		if (!value.contains(expected)) {
			status = false;
			driver.OutputStream("Gsite value does not match for this page.");
		}
		return status;
	}

	private static boolean validateGsiteValue(Driver driver, String value) {
		boolean status = true;
		if ((!value.contains("GTM-")) || (!value.contains("@UA-"))) {
			status = false;
			driver.OutputStream("Gsite value does not match for this page.");
		}
		return status;
	}

	private static String[] readStatIdentifier(Driver driver) {
		String[] data = new String[3];
		String pageSource = driver.getPageSource();
		data[0] = getPerameterValue(driver, pageSource, "xtsite");
		data[1] = getPerameterValue(driver, pageSource, "xtsd");
		data[2] = getPerameterValue(driver, pageSource, "gasiteID");
		// System.out.println("data1: "+data[0]+", data2:"+data[1]+", data3:
		// "+data[2]);
		return data;
	}

	private static String getPerameterValue(Driver driver, String data, String perameter) {
		String value = "";
		String temp = data;
		if (temp.contains("var " + perameter)) {
			perameter = "var " + perameter;
			driver.OutputStream("This site contains new versions of google statistical analysis perameter value for "
					+ perameter + ".");
			driver.OutputStream("This site contains old version of google statistical analysis perameter value for "
					+ perameter + ".");
			try {
				temp = temp.substring(temp.indexOf(perameter) + perameter.length());
				temp = temp.substring(temp.indexOf("'") + 1);
				value = temp.substring(0, temp.indexOf("'"));
			} catch (Exception e) {
				driver.OutputStream("Exception Triggered during site stat data process. Error: " + e.getMessage());
			}
		} else if (temp.contains(perameter)) {
			driver.OutputStream("This site contains old version of google statistical analysis perameter value for "
					+ perameter + ".");
			try {
				temp = temp.substring(temp.indexOf(perameter) + perameter.length());
				temp = temp.substring(temp.indexOf('"') + 1);
				value = temp.substring(0, temp.indexOf('"'));
			} catch (Exception e) {
				driver.OutputStream("Exception Triggered during site stat data process. Error: " + e.getMessage());
			}
		} else {
			driver.OutputStream("This site doen not contains any google statistical analysis perameter value for "
					+ perameter + ".");
		}
		return value;
	}

	private static boolean validateXTSiteIdentifier(Driver driver, String identifier) {
		boolean status = true;
		if (identifier != null && identifier.length() > 0) {
			status = identifier.length() == 6;
			if (status)
				driver.OutputStream("Site state iedntifier contains 6 digits. identifier is: " + identifier);
			// contain char?
			int noOfChar = identifier.length();
			for (int i = 0; i < noOfChar; i++) {
				if (!Character.isDigit(identifier.charAt(i))) {
					status = false;
					driver.OutputStream("Site state does not contain number at " + (i + 1));
					return status;
				}
			}
			driver.OutputStream("Site state contsains all numbers.");
		} else {
			driver.OutputStream("The identifer value passed through methode is not valid!!!!!!", false);
			return false;
		}
		return status;
	}

}
