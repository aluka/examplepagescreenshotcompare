package se.com.WebMachine.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.com.WebMachine.Tools.Driver;

@SuppressWarnings("unused")
public class clickWholeBanner {

	private static final String sliderContainerXpath = "//section[contains(@id,'slider') or contains(@class, 'slider')]";
	private static String SliderXpath = sliderContainerXpath + "/ul[@class='slides']/li";
	private static String sliderControllerXpath = sliderContainerXpath + "//*[@class='pagination']/li";

	public static boolean validateLinkBanner(Driver driver) {
		WebDriverWait wait = new WebDriverWait(driver.getInstance(), 30);
		Dimension screenSize = new Dimension(1000, 800);
		driver.getInstance().manage().window().setSize(screenSize);
		driver.getInstance().navigate().refresh();
		driver.wait(100);
		driver.waitToLoad();
		boolean status = false;
		int numberOfSlides = driver.findElements("Xpath", SliderXpath).size();
		if (!(numberOfSlides > 0)) {
			driver.OutputStream("There is no slides in this page. Skip testing.");
			return true;
		}
		status = numberOfSlides > 0;
		for (int slideNo = 1; slideNo <= numberOfSlides; slideNo++) {
			String slideXpath = SliderXpath + "[" + slideNo + "]";
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(slideXpath)));
			if (!driver.isElementAvailable(By.xpath(slideXpath + "//a"), 1)) {
				driver.OutputStream("There is no link in this slide. Skip slide no " + slideNo + ".");
				continue;
			}
			WebElement linkElement = driver.findElement("Xpath", slideXpath + "//a", 5);
			if (!isItBlueBoxLink(driver, linkElement)) {
				driver.OutputStream("This is not blue box link. Skip slide no " + slideNo + ".");
				continue;
			}
			if (numberOfSlides > 1)
				driver.click("Xpath", sliderControllerXpath + "[" + slideNo + "]");
			driver.wait(100);
			boolean statusLocal = validateLinkTopLeft(driver, linkElement);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(slideXpath)));
			linkElement = driver.findElement("Xpath", slideXpath + "//a", 5);
			if (numberOfSlides > 1)
				driver.click("Xpath", sliderControllerXpath + "[" + slideNo + "]");
			driver.wait(100);
			statusLocal = statusLocal & validateLinkBottomRight(driver, linkElement);
			if (statusLocal)
				driver.OutputStream("Test Passed for Blue Box link in slide no " + slideNo + ". Continue Testting");
			else
				driver.OutputStream("Test Failed for Blue Box link in slide no " + slideNo + ".");
			status = status & statusLocal;
		}
		if (status)
			driver.OutputStream("Blue box link test Passed.");
		else
			driver.OutputStream(" Blue of link test Failed.");
		driver.getInstance().manage().window().maximize();
		return status;
	}

	private static boolean validateLinkTopLeft(Driver driver, WebElement linkEle) {
		boolean status = false;
		String url = driver.getCurrentUrl();
		// String destURL = linkEle.getAttribute("href");
		driver.click(linkEle);
		driver.wait(100);
		driver.switchTab();
		driver.waitToLoad();
		if (!url.contains(driver.getCurrentUrl()) || !driver.getCurrentUrl().contains(url)) {
			status = true;
			driver.OutputStream("driver was able to click on the Top left corner of blue link box. Test Passed");
		} else
			driver.OutputStream("driver was not able to click on the Top Left corner of blue link box. Test Failed.");
		driver.wait(100);
		driver.closeMultipleTabs();
		driver.wait(100);
		driver.get(url);
		return status;
	}

	private static boolean validateLinkBottomRight(Driver driver, WebElement linkEle) {
		boolean status = false;
		String url = driver.getCurrentUrl();
		// String destURL = linkEle.getAttribute("href");
		driver.click(linkEle, linkEle.getSize().width - 2, linkEle.getSize().height - 2);
		driver.wait(100);
		driver.switchTab();
		driver.waitToLoad();
		if (!url.contains(driver.getCurrentUrl()) || !driver.getCurrentUrl().contains(url)) {
			status = true;
			driver.OutputStream("driver was able to click on the bottom left corner of blue link box. Test Passed");
		} else
			driver.OutputStream(
					"driver was not able to click on the bottom left corner of blue link box. Test Failed.");
		driver.wait(100);
		driver.closeMultipleTabs();
		driver.wait(100);
		driver.get(url);
		return status;
	}

	private static void skipSlide(Driver driver) {
		driver.wait(100);
	}

	private static boolean isItBlueBoxLink(Driver driver, WebElement element) {
		if (element == null)
			return false;
		else if (element.getText().contains("Watch the video")) {
			driver.OutputStream("This is a video link. Cant validate destination URL. Skip this Link");
			return false;
		} else if (element.getAttribute("class").contentEquals("cta"))
			return true;
		else
			return false;
	}
}
