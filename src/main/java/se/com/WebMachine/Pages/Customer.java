package se.com.WebMachine.Pages;

import se.com.WebMachine.dataSourceReader.Xls_Reader;

public enum Customer {
	/* Class contain a list of customers with valid and invalid data list. 
	*/	
	CusValid1(1), CusValid2(), CusInvalid1(2), CusInvalid2(3), CusInvalid3(4);
	public String FirstName, LastName, Telephone, Email, Company, PostCode, Country, State, City, Question1, Question2,
			Comment;
	public boolean NewsLetter, Valid;

	Customer() {
		/*Read static data define in the class file.*/
		readData();
	}

	Customer(int cusNumber) {
		/*Read data from xls file customer data sheet*/
		readData(cusNumber);
	}

	Customer(String first, String last, String telephone, String email, String country, boolean news) {
		/*define values for customer data set value */
		FirstName = first;
		LastName = last;
		Telephone = telephone;
		Email = email;
		Country = country;
		NewsLetter = news;
	}

	private void readData() {
		/*Return list of static data define in the class.*/
		FirstName = "Hossain";
		LastName = "Elahi";
		Telephone = "4013639009";
		Email = "hossain.elahi@non.schneider-electric.com";
		Company = "Schneider";
		PostCode = "02892";
		Country = "Zambia";
		Question1 = "Q1 answer 1";
		Question2 = "Q2 answer 2";
		Comment = "Test Input";
		NewsLetter = false;
		Valid = true;
	}

	private void readData(int cusNumber) {
		String sheetName = "CustomerDataSheet";
		String path = Xls_Reader.filename;
		Xls_Reader file = new Xls_Reader(path);
		FirstName = file.getCellData(sheetName, cusNumber, 2);
		LastName = file.getCellData(sheetName, cusNumber, 3);
		Telephone = file.getCellData(sheetName, cusNumber, 4);
		Email = file.getCellData(sheetName, cusNumber, 5);
		Company = file.getCellData(sheetName, cusNumber, 6);
		PostCode = file.getCellData(sheetName, cusNumber, 7);
		Country = file.getCellData(sheetName, cusNumber, 8);
		State = file.getCellData(sheetName, cusNumber, 9);
		City = file.getCellData(sheetName, cusNumber, 10);
		if (file.getCellData(sheetName, cusNumber, 11).toLowerCase().contains("yes"))
			NewsLetter = true;
		else
			NewsLetter = false;
		Question1 = file.getCellData(sheetName, cusNumber, 12);
		Question2 = file.getCellData(sheetName, cusNumber, 13);
		Comment = file.getCellData(sheetName, cusNumber, 14);
		if (file.getCellData(sheetName, cusNumber, 15).toLowerCase().contains("yes"))
			Valid = true;
		else
			Valid = false;

	}

}
