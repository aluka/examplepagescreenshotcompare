package se.com.WebMachine.Pages;

import java.util.ArrayList;

import se.com.WebMachine.Tools.Driver;

public class OpenLinkInSameWindow {
	public static final String linkXpath = "//*[@class='main']//*[@class='many-list']/ul/li";

	public static boolean validateLinkOpenSameWindow(Driver driver) {
		String baseUrl = driver.getCurrentUrl();
		boolean status = true;
		int noOfLinks = driver.findElements("Xpath", linkXpath).size();
		for (int linkNo = 1; linkNo <= noOfLinks; linkNo++) {
			if (clickLinkNo(driver, linkNo)) {
				if (noOfOpenTab(driver) > 1) {
					if (expectedToNewTab(linkNo)) {
						driver.OutputStream("Link no " + linkNo + " open in a new tab. As expected. Test PASSED.");
						status = true;
					} else {
						driver.OutputStream("Link no " + linkNo + " open in a new tab. Test FAILED.");
						status = false;
					}
					driver.closeMultipleTabs();
					gotoPage(driver, baseUrl);
				} else {
					driver.OutputStream("Link no" + linkNo + " open in same tab. Test PASSED.");
					gotoPage(driver, baseUrl);
				}
				driver.wait(100);
			} else {
				driver.OutputStream("cant go to the link no " + linkNo + ".");
				status = false;
			}
		}
		return status;
	}

	private static boolean expectedToNewTab(int linkNo) {
		if (linkNo % 2 == 0)
			return true;
		else
			return false;
	}

	private static int noOfOpenTab(Driver driver) {
		ArrayList<String> window = new ArrayList<String>(driver.getWindowHandles());
		return window.size();
	}

	private static boolean clickLinkNo(Driver driver, int linkNo) {
		if (linkNo == 0) {
			driver.OutputStream("Invalid link no. Continue testing.");
			return false;
		}
		String Xpath = linkXpath + "[" + linkNo + "]/a";
		if (driver.isElementAvailable("Xpath", Xpath, 1)) {
			driver.click("Xpath", Xpath);
			driver.wait(100);
			return true;
		} else
			return false;
	}

	private static void gotoPage(Driver driver, String url) {
		if (url.contains(driver.getCurrentUrl()) & url.length() == driver.getCurrentUrl().length()) {
			return;
		}
		driver.get(url);
	}
}
