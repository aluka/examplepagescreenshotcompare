package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.com.WebMachine.Pages.HomePage;
import se.com.WebMachine.Tools.Driver;

@SuppressWarnings("unused")
public class Search {

	public static String url;
	public static String title;
	private static final String searchContainerXpath = "//*[contains(@class,'search-container')]";
	private static final String searchBerXpath = searchContainerXpath + "//form[@class='search-bar']";
	private static final String searchInputXpath = searchBerXpath + "/input[@type='text']";
	private static final String searchIconXpath = searchBerXpath + "/input[@type='submit']";
	private static final String searchResultXpath = "//li[@class='single-result']";
	private static final String searchGuidedXpath = searchContainerXpath + "//*[@class='guided-search-results']";
	private static final String searchSuggetionXpath = searchContainerXpath + "//div[@class='guided-search-results']";
	private static final String searchResultList = searchSuggetionXpath + "//div[@class='guided-row']//a";
	private static final String searchMainBerXpath = "//div[@class='main']//form[contains(@class,'search-bar')]";
	private static final String searchInputPageXpath = searchMainBerXpath + "//input[@type='text']";
	private static final String searchMainSubmitXpath = searchMainBerXpath + "//input[@type='submit']";
	private static final String searchResultNoXpath = "//*[@class='nb-result']";
	private static final String megamenuXpath = "//*[@class='main-menu']//*[@class='tier-one']";

	public static boolean codeMove(Driver driver) {
		String oldURL = driver.getCurrentUrl();
		String oldTitle = driver.getTitle();
		String txt1 = "WebMachine Search (2/15/2017, 4:52:13 PM)", txt2 = "develop [4.7_RC1-SNAPSHOT] commit:";
		if (HomePage.getPageDomain(driver).contains("www")) {
			driver.OutputStream("This is live server. We cant test code move in live server. Continue testing.");
			return true;
		}
		if (!openJSFile(driver)) {
			driver.OutputStream("cant go to guided serach. Test Failed.");
			return false;
		}
		boolean status = searchText(driver, txt1) & searchText(driver, txt2);
		if (status)
			driver.OutputStream("Code moved to platform '" + HomePage.getPageDomain(driver) + "' Test PASSED.");
		else
			driver.OutputStream("Code did not move to platform '" + HomePage.getPageDomain(driver) + "' Test FAILED.");
		driver.get(oldURL, oldTitle);
		return status;
	}

	private static boolean openJSFile(Driver driver) {
		String oldURL = driver.getCurrentUrl();
		String url = getJSURL(driver);
		if (url.length() > 0) {
			driver.OutputStream("Going to " + url + " to validate guided search JS file moved.");
			driver.get(url);
			driver.waitToLoad();
			return true;
		} else {
			driver.OutputStream("We dont have js file link for this url: " + oldURL + ". Continue testing.");
			return false;
		}
	}

	private static boolean searchText(Driver driver, String txt) {
		try {
			return driver.findElement(By.tagName("body")).getText().contains(txt);
		} catch (Exception e) {
			return false;
		}
	}

	private static String getJSURL(Driver driver) {
		String env = HomePage.getPageDomain(driver);
		String URL;
		if (env.contains("int"))
			URL = "http://www-int1.schneider-electric.com/us/en/assets-re1/js/guided-search/guided-rtl.stripped.js";
		else if (env.contains("sqe"))
			URL = "http://www-sqe1.schneider-electric.com/us/en/assets-re1/js/guided-search/guided-rtl.stripped.js";
		else if (env.contains("pre"))
			URL = "http://www-pre.amer1.sdl.schneider-electric.com/us/en/assets-re1/js/guided-search/guided-rtl.stripped.js";
		else if (env.contains("prd"))
			URL = "http://www.schneider-electric.com/us/en/assets-re1/js/guided-search/guided-rtl.stripped.js";
		else
			URL = "";
		return URL;
	}

	public static boolean searchTest(Driver driver, String searchData, int priority) {
		url = driver.getCurrentUrl();
		title = driver.getTitle();
		boolean status = validateUI(driver);
		if (status) {
			// status = status & ExpandFuncValid(driver);
			// status = status & test1(driver);
			// driver.takeScreenShot( "AfterSearchTest1");
			if (priority > 2) {
				status = test2(driver, searchData);
				driver.takeScreenShot("AfterSearchTest2");
			}
		} else {
			driver.OutputStream("Search input box UI is not validated. Test Failed", status);
		}
		if (!(driver.getCurrentUrl().equalsIgnoreCase(url))) {
			driver.get(url);
			driver.waitToLoad(title);
		}
		driver.takeScreenShot("AfterFinishSearchTest");
		return status;
	}

	private static boolean ExpandFuncValid(Driver driver) {
		int widthBefore = 0;
		int widthAfter = 0;
		try {
			widthBefore = driver.findElement("Xpath", searchBerXpath, 5).getSize().getWidth();
			driver.findElement("Xpath", searchBerXpath, 2).click();
			waitTillExtent(driver);
			widthAfter = driver.findElement("Xpath", searchBerXpath, 5).getSize().getWidth();
		} catch (Exception e) {
			driver.OutputStream("Expection triggered during testing search box 'expand' functionality test. Exception: "
					+ e.getMessage());
			return false;
		}
		if (widthBefore > 0 && widthAfter > 0) {
			boolean status = false;
			driver.OutputStream(
					"'Search-box' width before selecting was: " + widthBefore + " and after is: " + widthAfter + ".");
			if (widthAfter > widthBefore) {
				driver.OutputStream("SeachBox expansion test PASSED.");
				status = true;
			} else {
				driver.OutputStream("SeachBox expansion test FAILED.");
				status = false;
			}
			driver.refresh();
			driver.wait(100);
			driver.waitToLoad();
			return status;
		} else {
			driver.OutputStream("Could not read 'SearchBox' size.", false);
			return false;
		}
	}

	private static boolean validateUI(Driver driver) {
		boolean status = driver.isElementVissible("Xpath", searchInputXpath, 2)
				& driver.isElementVissible("Xpath", searchIconXpath, 2) & validateLocation(driver)
				& isIconInside(driver);
		if (status)
			driver.OutputStream("Seach UI validated. Test PASSED");
		else
			driver.OutputStream("Search UI is not validated. Test Failed.");
		return status;
	}

	private static boolean isIconInside(Driver driver) {
		try {
			WebElement searchBox = driver.findElement("Xpath", searchBerXpath, 2);
			WebElement searchIcon = driver.findElement("Xpath", searchIconXpath, 2);
			Dimension iconSize = searchIcon.getSize();
			Point iconLoc = searchIcon.getLocation();
			Dimension searchBoxSize = searchBox.getSize();
			Point searchBoxLoc = searchBox.getLocation();
			boolean status = iconLoc.x > searchBoxLoc.x & iconLoc.y >= searchBoxLoc.y
					& (iconLoc.x + iconSize.width) <= (searchBoxLoc.x + searchBoxSize.width)
					& (iconLoc.y + iconSize.height) <= (searchBoxLoc.y + searchBoxSize.height);
			if (status)
				driver.OutputStream("Search icon is inside search box.");
			else {
				driver.OutputStream("Search icon location=" + iconLoc + " and dimension is=" + iconSize + ".");
				driver.OutputStream("Search box location=" + searchBoxLoc + " and dimension is=" + searchBoxSize + ".");
				driver.OutputStream("Search icon is not inside search box. test FAILED", false);
			}
			return status;
		} catch (Exception e) {
			return false;
		}

	}

	private static boolean validateLocation(Driver driver) {
		int expectedLocX = expectedPositionX(driver), expectedLocY = expectedPositionY(driver),
				expectedWidth = expectedWidth(driver), accualLocX = 0, accualLocY = 0, accualWidth = 0;
		if (driver.isElementAvailable("Xpath", searchBerXpath, 2)) {
			WebElement searchBox = driver.findElement("Xpath", searchBerXpath, 3);
			accualLocX = searchBox.getLocation().x;
			accualLocY = searchBox.getLocation().y;
			accualWidth = searchBox.getSize().width;
			if (Math.abs(expectedWidth - accualWidth) < 2 && Math.abs(expectedLocX - accualLocX) < 2
					&& accualLocY < expectedLocY) {
				driver.OutputStream("Search input box location validaed. Test PASSED.");
				return true;
			} else {
				driver.OutputStream("Search box expected location X=" + expectedLocX + ", Y<" + expectedLocY
						+ " and expected width=" + expectedWidth + ". But accual location X=" + accualLocX + ", Y="
						+ accualLocY + " and Width=" + accualWidth + ".");
				driver.OutputStream("Search input box location is not validated. Test Failed.");
				return false;
			}
		} else {
			driver.OutputStream("Search box is not vissible in the page. Skip testing");
			return true;
		}
	}

	private static int expectedWidth(Driver driver) {
		int width = 0;
		try {
			WebElement greenHeader = driver.findElement("Xpath", megamenuXpath, 5);
			if (greenHeader != null) {
				width = greenHeader.getSize().width;
			}
		} catch (Exception e) {
		}
		return width;
	}

	private static int expectedPositionX(Driver driver) {
		int X = 0;
		try {
			WebElement greenHeader = driver.findElement("Xpath", megamenuXpath, 5);
			if (greenHeader != null) {
				X = greenHeader.getLocation().x;
			}
		} catch (Exception e) {
		}
		return X;
	}

	private static int expectedPositionY(Driver driver) {
		int Y = 0;
		try {
			WebElement greenHeader = driver.findElement("Xpath", megamenuXpath, 5);
			if (greenHeader != null) {
				Y = greenHeader.getLocation().y;
			}
		} catch (Exception e) {
		}
		return Y;
	}

	private static void waitTillExtent(Driver driver) {
		driver.wait(800);
	}
	/*
	 * private static boolean test1(Driver driver){ boolean status = false; try{
	 * // driver.wait(300); //
	 * driver.findElement(By.xpath(searchIconXpath)).click(); driver.click(
	 * "xpath", searchIconXpath); driver.wait(200); driver.waitToLoad( 20,
	 * "Search"); // HomePage.priority1Test(driver); status =
	 * driver.isElementAvailable( "Xpath", searchMainBerXpath); status = status
	 * & driver.isElementAvailable( "Xpath", searchInputPageXpath); status =
	 * status & driver.isElementAvailable( "Xpath", searchMainSubmitXpath);
	 * if(status){ driver.OutputStream("Search Test 1 is PASSED."); }else{
	 * driver.OutputStream("Search Test 1 is FAIELD!!!", status); }
	 * }catch(Exception e){
	 * driver.OutputStream("Search Icon is not accessable in this page. URL: "+
	 * url+ " Error is: "+e.getMessage(), false); } driver.get(url);
	 * driver.wait(100); driver.waitToLoad( title); driver.wait(300); return
	 * status; }
	 */

	private static boolean test2(Driver driver, String testData) {
		WebDriverWait wait = new WebDriverWait(driver.getInstance(), 20);
		Boolean status = false;
		String link;
		try {
			status = driver.isElementAvailable("xpath", searchInputXpath);
			driver.findElement("Xpath", searchInputXpath, 15).sendKeys(testData);
			driver.wait(500);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(searchResultList)));
			List<WebElement> searchSuggetion = null;
			if (driver.isElementAvailable("xpath", searchSuggetionXpath, 15))
				searchSuggetion = driver.findElements("xpath", searchResultList);
			if (searchSuggetion == null) {
				driver.OutputStream("There is no searh suggation vissible in this page.", false);
				driver.takeScreenShot("SearchSuggationMissing");
			}
			status = searchSuggetion.size() > 0;
			/*
			 * for(int resultNo= 0; resultNo<searchSuggetion.size();
			 * resultNo++){ link =
			 * searchSuggetion.get(resultNo).getAttribute("href"); //
			 * System.out.println("Search Suggetion no "+(resultNo+1)
			 * +" link is: "+link); // if(link!=null)status = status &
			 * link.contains("langType="); // else status = false;
			 * if(link.contains("langType=")) driver.
			 * OutputStream("Search Result link as expected. PASSED for no "+(
			 * resultNo+1)+" result"); else driver.
			 * OutputStream("Search Result link does not contain 'langType' expected. FAILED for no "
			 * +(resultNo+1)+" result"); }
			 */
			if (status) {
				if (driver.isElementVissible("Xpath", searchGuidedXpath, 3)) {
					driver.OutputStream("This page contains new guided search. Continue testing.");
					driver.click("Xpath", searchIconXpath);
					// driver.click( searchSuggetion.get(8));
				} else {
					driver.OutputStream("This page contains old guided search. Continue testing.");
					driver.click("Xpath", searchIconXpath);
					// driver.click( searchSuggetion.get(2));
				}
				// status = true;
				// searchSuggetion.get(1).click();
				// driver.click( "Xpath", searchIconXpath);
				// driver.findElement(By.tagName("body")).sendKeys(Keys.RETURN);
				driver.wait(100);
				driver.waitToLoad();
				// driver.wait(500);
				status = status & validateSearchPage(driver);
				if (status) {
					driver.OutputStream(
							"Most of the defiend functionalities are availble in the search result page. Continue with the test.");
				} else {
					driver.OutputStream("All the defined Search functionalities are not available in the page."
							+ driver.getCurrentUrl(), false);
				}
			} else {
				driver.OutputStream("No search suggetions is suggested for search data: " + testData, false);
			}
		} catch (Exception e) {
			driver.OutputStream("Seacrh Test 2 Exception: " + e, false);
			status = false;
		}
		driver.get(url);
		driver.wait(100);
		driver.waitToLoad(title);
		driver.wait(300);
		return status;
	}

	private static boolean validateSearchPage(Driver driver) {
		boolean status = true;
		if (!HomePage.schneiderLogoVisible(driver)) {
			driver.OutputStream("There is error in the page. Skip testing", false);
			return false;
		}
		// status = HomePage.priority1Test(driver);
		try {
			status = driver.isElementAvailable("Xpath", searchMainBerXpath);
			status = status & driver.isElementAvailable("Xpath", searchInputPageXpath);
			status = status & driver.isElementAvailable("Xpath", searchMainSubmitXpath);
			status = status & isContainNumber(driver.findElement("Xpath", searchResultNoXpath, 15).getText());
			List<WebElement> searchResult = driver.findElements("xpath", searchResultXpath);
			for (int searchResNo = 0; searchResNo < searchResult.size(); searchResNo++) {
				status = status & checkResultElement(driver, searchResNo);
			}
		} catch (Exception e) {
			driver.OutputStream("Exception triggared during testing search destination page. Skip testing.");
		}
		return status;
	}

	private static boolean checkResultElement(Driver driver, int number) {
		boolean status = true;
		int no = number + 1;
		String titleXpath = searchResultXpath + "[" + no + "]//a[contains(@class,'title')]";
		String dateXpath = searchResultXpath + "[" + no + "]//span[@class='date']";
		String detailsXpath = searchResultXpath + "[" + no + "]//p";
		String docLinkXpath = searchResultXpath + "[" + no + "]//a[@class='document']";
		int timeoutSeconds = 5;
		String locator = "Xpath";
		try {
			driver.OutputStream("Title of the search Output no " + no + " is: "
					+ driver.findElement(locator, titleXpath, timeoutSeconds).getText());
			driver.OutputStream(
					"Date last Updated is: " + driver.findElement(locator, dateXpath, timeoutSeconds).getText());
			driver.OutputStream("Details of the product is: "
					+ driver.findElement(locator, detailsXpath, timeoutSeconds).getText());
			driver.OutputStream("Document link of the product is: "
					+ driver.findElement(locator, docLinkXpath, timeoutSeconds).getAttribute("href"));
			status = driver.findElement(locator, titleXpath, timeoutSeconds).getText().length() > 0
					& driver.findElement(locator, dateXpath, timeoutSeconds).getText().length() > 0
					& driver.findElement(locator, detailsXpath, timeoutSeconds).getText().length() > 0
					& driver.findElement(locator, docLinkXpath, timeoutSeconds).getAttribute("href").length() > 0;
		} catch (Exception e) {
			status = false;
			driver.OutputStream(
					"Details of search result no: " + no + " is not available. url: " + driver.getCurrentUrl(), status);
			System.out.println("Exception report is: " + e);
		}
		return status;
	}

	public static boolean isContainNumber(String txt) {
		boolean status = false;
		try {
			int noOfChar = txt.length();
			for (int i = 0; i < noOfChar; i++) {
				if (Character.isDigit(txt.charAt(i))) {
					status = true;
					return status;
				}
			}
		} catch (Exception e) {
			System.out.println("Exception Triggered " + e.getCause());
		}
		return status;
	}
}
