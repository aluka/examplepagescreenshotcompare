package se.com.WebMachine.Pages;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import se.com.WebMachine.Tools.Driver;

/**Class contains script to test old header with two stage selection.*/
@SuppressWarnings("unused")
public class OldHeader {
	private static String headerTopMenuXpath = "//*[@class='header-content'] //*[@class='main-menu']/li/ul/li[not(@id) and (not(@class) or (@class='') or (@class='selected') )]";
	
	/**validate the whole header menu*/
	public static boolean validateTopMenu(Driver driver, String baseURL, int priority){
		String[] testParameters = new String[6];
		boolean topMenuValidated = true;	
		String topMenuNames = new String();
		String xpathTopMenu = new String();
		testParameters[0] = "Req"+ 1;
		testParameters[1] = "Header";
		testParameters[2] = "Menu";
		testParameters[3] = baseURL;
		testParameters[4] = "Xpath";
		testParameters[5] = headerTopMenuXpath;
		driver.navigate().refresh();

		if (testParameters[3] != null && testParameters[3].length() > 0){	
			//				domains = driver.click("xPath", testParameters[5], testParameters[3], testParameters);
			//				if (checkDomains()){
			//System.out.println("Domains are checked >>>>>> ");
			List<WebElement> topMenuElements = driver.findElements(testParameters[4], testParameters[5]);

			int numberOfTopMenuElements = topMenuElements.size();

			if(numberOfTopMenuElements>0){			// top menu item names which are present near schneider logo
				driver.OutputStream( "  ....Total Top Menu Options are : "+ numberOfTopMenuElements);

				topMenu:	
					//for(int topMenuIndex=1;topMenuIndex<=1;topMenuIndex++)		// iterate over each top menu item
						for(int topMenuIndex=1;topMenuIndex<=numberOfTopMenuElements && priority >1;topMenuIndex++)
					{
						try{
							xpathTopMenu = testParameters[5]+"["+topMenuIndex+"]"+"/a";  // xpath to top menu to be clicked

							if(driver.isElementAvailable( "xPath",xpathTopMenu) && priority >2){
								topMenuNames = driver.findElement( testParameters[4], xpathTopMenu ,30).getText();  // getting names of top menu
								driver.OutputStream( "  ....Top Menu Option "+ topMenuIndex +" is : "+ topMenuNames);
								driver.click( "xPath", xpathTopMenu);
								//Thread.sleep(2000);
								driver.wait(100);
								//	topMenuValidated = validateMegaMenuItems(xpathTopMenu,topMenuIndex,topMenuNames, testParameters);
								//topMenuValidated = 
								topMenuValidated = validateLeftMegaMenu(driver, xpathTopMenu,topMenuIndex,topMenuNames, testParameters);

							}else{
								driver.OutputStream( "  ...." + "Top Menu links not implemented, so test will be skipped");
								topMenuValidated = false;
							}	
						}catch (StaleElementReferenceException sere) {
							topMenuValidated = false;
							driver.OutputStream( "  ....StaleElementReferenceException on Link '" + topMenuNames+"'. Move on to next test...");
							driver.OutputStream( "  ....StaleElementReferenceException message is " + sere.getMessage());
							continue topMenu;
						}
						catch (NoSuchElementException nsee) {
							topMenuValidated = false;
							driver.OutputStream( "  ....NoSuchElementException on Link '" + topMenuNames + "'. Move on to next test...");
							driver.OutputStream( "  ....NoSuchElementException message is " + nsee.getMessage());
							
							continue topMenu;
						}

					} // for loop for Top menu names


			}
			else{
				driver.OutputStream( "  ...." + "Top Menu links not implemented, so test will be skipped");
				topMenuValidated = false;
				//return topMenuValidated;
			}				
		}
		else{
			driver.OutputStream( "  ...." + "No Base Url found, so test will be skipped");
			topMenuValidated = false;
			//return topMenuValidated;
		}

		return topMenuValidated;
	}
	
	private static boolean validateLeftMegaMenu(Driver driver, String xpathTopMenu, int topMenuIndex, String topMenuNames, String... testParameters)
	{
		//Selenium.this.setTimeout();
		boolean leftMenuValidated = true;
		//String classAttributeLeftMenu = new String();
		String xPathLeftMegaMenu = new String();
		String leftMenuCategoryNames = new String();
		String xPathLeftAdditionalLinks = new String();
		String leftMenuAdditionalLinkNames = new String();
		String beforeClickUrl = new String();
		String hrefAttributeAdditionalLink = new String();
		String leftMenuClassAttribute = new String();


		boolean isLeftMenuPresent = driver.isElementAvailable( "xPath", testParameters[5]+"["+topMenuIndex+"]//ul[@class='level2']/li");
		
		if(isLeftMenuPresent){
			//	classAttributeLeftMenu = driver.findElementAttribute("xPath",testParameters[5]+"["+topMenuIndex+"]" + "/div", 15, "class");
			//	if("active".equalsIgnoreCase(classAttributeLeftMenu)){
			List<WebElement> leftMenuCategoryList=		driver.findElements( "xPath", testParameters[5]+"["+topMenuIndex+"]//ul[@class='level2']/li");
			//+Constants.WebMachine_DispatchPage.header_MegaMenu_Left_Category);

			int noOfLeftMenuCategories = leftMenuCategoryList.size();

			if(noOfLeftMenuCategories >0){
				driver.OutputStream("  ....Total Left Menu Categories for Top Menu : '"+topMenuNames+"' are : "+ noOfLeftMenuCategories);

				leftMenuContinue :					// printing names of left menu category names
					for(int leftMenuIndex=0;leftMenuIndex<noOfLeftMenuCategories;leftMenuIndex++)
					{
						try{
							xPathLeftMegaMenu = testParameters[5]+"["+topMenuIndex+"]//ul[@class='level2']/li"  ;

							if(driver.isElementAvailable( "xPath", xPathLeftMegaMenu + "[" + (leftMenuIndex+1) + "]"+ "/a")){
								leftMenuCategoryNames = driver.findElement( testParameters[4], xPathLeftMegaMenu + "[" + (leftMenuIndex+1) + "]"+ "/a" ,30).getText();  // left Mega Menu Category Names
								leftMenuClassAttribute = driver.findElement( testParameters[4], xPathLeftMegaMenu + "[" + (leftMenuIndex+1) + "]"+ "/a", 30).getAttribute("class");
								//System.out.println(">>"+leftMenuClassAttribute+"<<");
								driver.OutputStream("  ............................................................................................");
								driver.OutputStream("  ............................................................................................");
								driver.OutputStream( "  ....Left Mega Menu Option " + (leftMenuIndex+1) +" for Top Menu Option '" + topMenuNames +"' is : "
										+ leftMenuCategoryNames);
								if("openLink".equalsIgnoreCase(leftMenuClassAttribute)){
									
									leftMenuValidated = validateRightMenuWithOpenLink(driver, xpathTopMenu,xPathLeftMegaMenu+ "[" + (leftMenuIndex +1)+ "]", topMenuNames, leftMenuCategoryNames, testParameters);									
								}else{
									
									leftMenuValidated = validateRightMenu(driver, xpathTopMenu,xPathLeftMegaMenu+ "[" + (leftMenuIndex+1)+ "]", topMenuNames, leftMenuCategoryNames, testParameters);
								}

							}else{
								driver.OutputStream( "  ...." + "Left Menu Categories are empty for Top Menu option-"+topMenuNames);
								leftMenuValidated = false;
							}
						}catch (StaleElementReferenceException sere) {
							leftMenuValidated = false;
							driver.OutputStream( "  ....StaleElementReferenceException on Link '" + leftMenuCategoryNames+"'. Move on to next Left link...");
							driver.OutputStream( "  ....StaleElementReferenceException message is " + sere.getMessage());
							continue leftMenuContinue;
						}
						catch (NoSuchElementException nsee) {
							leftMenuValidated = false;
							driver.OutputStream("  ....NoSuchElementException on Link '" + leftMenuCategoryNames + "'. Move on to next Left link...");
							driver.OutputStream( "  ....NoSuchElementException message is " + nsee.getMessage());
							continue leftMenuContinue;
						}catch (ElementNotVisibleException e) {	
							leftMenuValidated = false;
							driver.OutputStream( "  ....ElementNotVisibleException on Link '" + leftMenuCategoryNames + "'. Move on to next Left link...");
							driver.OutputStream( "  ....ElementNotVisibleException message is " + e.getMessage());
							continue leftMenuContinue;
						} 

					}
			}else{   		// if condition no of left menu categories >0
				driver.OutputStream( "  ...." + "Left Menu Categories are not implemented for Top Menu option-"+topMenuNames);
				leftMenuValidated = false;
			}

			/*
			 * Left Menu Additional links
			 */
			boolean isLeftMenuAdditionalLinkPresent = driver.isElementAvailable( "xPath", testParameters[5]+"["+topMenuIndex+"]" + "//ul[@class='additional-links']");
			if(isLeftMenuAdditionalLinkPresent){
				List<WebElement> leftMenuAdditionalLinks =		driver.findElements( "xPath", testParameters[5]+"["+topMenuIndex+"] "+ "/div"+ " //ul[@class='additional-links']/li");
				//+Constants.WebMachine_DispatchPage.header_MegaMenu_Left_Category);

				int noOfLeftMenuadditionalLinks = leftMenuAdditionalLinks.size();

				if(noOfLeftMenuadditionalLinks >0){
					driver.OutputStream("  ....Total Left Menu Additional Links for Top Menu : '"+topMenuNames+"' are : "
							+ noOfLeftMenuadditionalLinks);

					AddLinkContinue :					// printing names of left menu category names
						for(int AddLinkIndex=1;AddLinkIndex<=noOfLeftMenuadditionalLinks;AddLinkIndex++)
						{
							try{
								xPathLeftAdditionalLinks = testParameters[5]+"["+topMenuIndex+"] "+ "/div"+ "//ul[@class='additional-links']/li" + "[" + AddLinkIndex + "]" + "/a";

								if(driver.isElementAvailable("xPath", xPathLeftAdditionalLinks)){
									leftMenuAdditionalLinkNames = driver.findElement( testParameters[4], xPathLeftAdditionalLinks ,30).getText();  // left Mega Menu Additional link Names
									driver.OutputStream("  ............................................................................................");
									driver.OutputStream( "  ....Additional Link for Left Mega Menu Option "+ AddLinkIndex +" for Top Menu Option '"+ topMenuNames +"' is : "
											+ leftMenuAdditionalLinkNames);

									//validation of additional links starts here

									beforeClickUrl = driver.getCurrentUrl();  // before click value of url
									hrefAttributeAdditionalLink = driver.findElement( "xPath", xPathLeftAdditionalLinks, 30).getAttribute("href");  // taking href value
									
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);  // waiting for loading the window completely
									
									if(driver.isElementAvailable( "xPath", xPathLeftAdditionalLinks)){
										
										driver.click( "xPath", xPathLeftAdditionalLinks);
										driver.wait(100);
										if(driver.checkDomain( testParameters[3]) && HomePage.priority1Test(driver)){
											driver.OutputStream("link is varrified. url: "+driver.getCurrentUrl());
										}
										driver.get(testParameters[3]);
										driver.wait(100);
										//Set a =driver.getInstance().getWindowHandles();
										ArrayList<String> tabNew = new ArrayList<String>(
												driver.getWindowHandles());
	
										// if there are more windows then go inside if
										if(tabNew.size()>1)
										{
											driver.switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window
	
											String tabSwitchUrl = driver.getCurrentUrl();
	
											//	domains = driver.click("xPath", xPathLeftMenuCatLinks, testParameters[3], testParameters);
											if((hrefAttributeAdditionalLink.length()!=0) && (tabSwitchUrl.length() !=0))
											{
												if(hrefAttributeAdditionalLink.toLowerCase().contains(tabSwitchUrl.toLowerCase()) || tabSwitchUrl.toLowerCase().contains(hrefAttributeAdditionalLink.toLowerCase()))
												{
													driver.OutputStream( "  ...." + "Link '"+ leftMenuAdditionalLinkNames +"' is redirecting to correct URL, Hence Verified");
													if (driver.checkDomain( testParameters[3]))
														driver.OutputStream( "  ....Hence the domain is OK  for "+ leftMenuAdditionalLinkNames);
													else{
														driver.OutputStream( "  ...." + "Domain not verified. Known Issue. So test will be skipped");
														leftMenuValidated = false;
													}
												}
												else{
													driver.OutputStream( "  ...." + "Link '"+ leftMenuAdditionalLinkNames +"' is not redirecting to correct URL, Hence not Verified");
													driver.OutputStream( "  ...." + "Domain not verified. Known Issue. So test will be skipped");
													leftMenuValidated = false;
												}
											}
											else{
												driver.OutputStream("  ....Redirecting link is found blank for "+ leftMenuAdditionalLinkNames);
											}
	
											driver.switchTab();
											driver.wait(100);
											//driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
	
										}
									}else{
										driver.OutputStream( "  ....Redirecting link is found Wrong for "+ leftMenuAdditionalLinkNames);
										leftMenuValidated = false;
									}
									/// validation ends here

								}else{
									driver.OutputStream( "  ...." + "Additional link for Left Menu Categories are empty for Top Menu option-"+topMenuNames);
									leftMenuValidated = false;
								}
							}catch (StaleElementReferenceException sere) {
								leftMenuValidated = false;
								driver.OutputStream( "  ....StaleElementReferenceException on Link '" + leftMenuCategoryNames+"'. Move on to next Left Menu link...");
								driver.OutputStream( "  ....StaleElementReferenceException message is " + sere.getMessage());
								continue AddLinkContinue;
							}
							catch (NoSuchElementException nsee) {
								leftMenuValidated = false;
								driver.OutputStream( "  ....NoSuchElementException on Link '" + leftMenuCategoryNames + "'. Move on to next Left Menu link...");
								driver.OutputStream( "  ....NoSuchElementException message is " + nsee.getMessage());
								continue AddLinkContinue;
							}catch (ElementNotVisibleException e) {	
								leftMenuValidated = false;
								driver.OutputStream("  ....ElementNotVisibleException on Link '" + leftMenuCategoryNames + "'. Move on to next Left Menu link...");
								driver.OutputStream( "  ....ElementNotVisibleException message is " + e.getMessage());
								continue AddLinkContinue;
							} catch (IndexOutOfBoundsException e) {
								e.printStackTrace();
							} 

						}
				}else{   		// if condition no of left menu categories >0
					driver.OutputStream( "  ...." + "Left Menu Additional links are blank for Top Menu option-"+topMenuNames);
					leftMenuValidated = false;
				}
			}else{   		// if condition element not present
				driver.OutputStream( "  ...." + "Left Menu Additional links are not implemented for Top Menu option-"+topMenuNames);
				leftMenuValidated = false;
			}

			//		}

		}else{
			driver.OutputStream( "  ...." + "Left Menu Categories not implemented/There is some error in Left Menu Categories, for Top Menu option-"+topMenuNames);
			leftMenuValidated = false;
		}
		// Mega Menu opens and in that left menu is opened, xpath for Left side of Menu category

		return leftMenuValidated;
	}

	private static boolean validateRightMenu(Driver driver, String xpathTopMenu, String xPathLeftMegaMenu,String topMenuNames, String leftMenuCategoryNames, String...testParameters){
		boolean rightMenuValidated = true;
		String xPathRightMegaMenu = new String();
		String rightMenuHeadlineName = new String();
		String hrefAttributeRightMenu = new String();
		String rightMenuCategoryName = new String();
		int index =0;
		driver.click( "Xpath", xPathLeftMegaMenu + "/a");
/*		if(driver.isElementAvailable( "Xpath", xPathLeftMegaMenu + "/a"))
			driver.findElement( "Xpath", xPathLeftMegaMenu + "/a", 15).click();*/
		driver.wait(100);
		xPathRightMegaMenu = xPathLeftMegaMenu + "//div[not(@class)]";

		if(driver.isElementAvailable( "xPath", xPathRightMegaMenu+"/a[@class='openLink']") ||
				driver.isElementAvailable( "xPath", xPathRightMegaMenu+"//ul[contains(@style,'')]")){
			
			rightMenuHeadlineName = driver.findElement( testParameters[4], xPathRightMegaMenu+"/a[@class='openLink']" ,30).getText(); // right menu headline names
			
			if(leftMenuCategoryNames.equalsIgnoreCase(rightMenuHeadlineName)){
			
				List<WebElement> rightMenuCategoryList = driver.findElements( "xPath", xPathRightMegaMenu + "//ul[contains(@style,'')]"+"/li");
				
				int noOfRightMenuElements = rightMenuCategoryList.size();
				
				driver.OutputStream( "  ....Total Right Menu Categories for Left Menu Category option : '"+leftMenuCategoryNames+"' are : "
						+ noOfRightMenuElements);
				
				

				rightMenuContinue :					// printing names of left menu category names
					for(int rightMenuIndex=1;rightMenuIndex<=noOfRightMenuElements;rightMenuIndex++)
					{
						hrefAttributeRightMenu = driver.findElement( "Xpath", xPathRightMegaMenu + "//ul[contains(@style,'')]"+"/li"+"[" + rightMenuIndex + "]" + "/a", 15).getAttribute("href");
						rightMenuCategoryName = driver.findElement( "Xpath", xPathRightMegaMenu + "//ul[contains(@style,'')]"+"/li"+"[" + rightMenuIndex + "]" + "/a", 15).getAttribute("text");
						
//						System.out.println("xpath for right menu >> "+xPathRightMegaMenu + "//ul[contains(@style,'')]"+"/li"+ 
//									"[" + rightMenuIndex + "]" + "/a");
					//rightMenuSameOptionContinue :
						try {
							driver.OutputStream("  ............................................................................................");
							driver.OutputStream("  ....Clicking on Right Menu Category option "+rightMenuIndex + " ,that is : '" + rightMenuCategoryName 
									+"' of Left Menu option "+leftMenuCategoryNames);
							
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);  // waiting for loading the window completely
							
							if(driver.isElementAvailable( "xPath", xPathRightMegaMenu + "//ul[contains(@style,'')]"+"/li"+ 
										"[" + rightMenuIndex + "]" + "/a")){
								
								//System.out.println(System.getProperty("user.dir")	+ "\\reports\\images\\" +logDate()+"_"+rightMenuCategoryName+".png");
								Thread.sleep(2000);
								
								driver.click( "xPath", xPathRightMegaMenu + "//ul[contains(@style,'')]"+"/li"+"[" + rightMenuIndex + "]" + "/a");
								driver.wait(100);
								if(driver.checkDomain( testParameters[3]) && HomePage.priority1Test(driver)){
									driver.OutputStream("link is varrified. url: "+driver.getCurrentUrl());
								}
								driver.get(testParameters[3]);
								driver.wait(100);
								//Thread.sleep(2000);
								driver.wait(100);
								
								ArrayList<String> tabNew = new ArrayList<String>(
										driver.getWindowHandles());
	
								// if there are more windows then go inside if
								if(tabNew.size()>1)
								{
	
									driver.switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window
	
									String tabSwitchUrl = driver.getCurrentUrl();
	
									if((hrefAttributeRightMenu.length()!=0) && (tabSwitchUrl.length() !=0))
									{
										if((hrefAttributeRightMenu.toLowerCase().contains(tabSwitchUrl.toLowerCase())) || (tabSwitchUrl.toLowerCase().contains(hrefAttributeRightMenu.toLowerCase())) )
										{
											driver.OutputStream( "  ...." + "Link '"+ rightMenuCategoryName +"' is redirecting to correct URL, Hence Verified");
											if (driver.checkDomain( testParameters[3]))
												driver.OutputStream("  ....Hence the domain is OK  for "+ rightMenuCategoryName);
											else{
												driver.OutputStream( "  ...." + "Domain not verified. Known Issue.");
												//footerLinksValidated = false;
											}
										}
										else{
											driver.OutputStream( "  ...." + "Link '"+ rightMenuCategoryName +"' is not redirecting to correct URL, Hence not Verified");
											driver.OutputStream( "  ...." + "Domain not verified. Known Issue.");
											//footerLinksValidated = false;
										}
									}
									else{
										driver.OutputStream( "  ....Redirecting link is found blank for "+ rightMenuCategoryName);
										rightMenuValidated = false;
									}
	
									driver.switchtoMainWindow( testParameters[3]);   // getting back to main window
									driver.wait(100);
									//driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely
									if(driver.isElementAvailable( "xPath", xpathTopMenu))
										driver.findElement( "Xpath", xpathTopMenu, 15).click();
									driver.wait(100);
									if(driver.isElementAvailable( "xPath", xPathLeftMegaMenu+"/a"))
										driver.findElement( "Xpath", xPathLeftMegaMenu+ "/a", 15).click();
									driver.wait(100);
	
								}else{
	
									String afterClickUrl = driver.getCurrentUrl();
									
									driver.OutputStream( "  ....hrefAttribute : "+ hrefAttributeRightMenu + "   ||  afterclick url : "+afterClickUrl);
									//	domains = driver.click("xPath", xPathLeftMenuCatLinks, testParameters[3], testParameters);
									if((hrefAttributeRightMenu.length()!=0) && (afterClickUrl.length() !=0))
									{
										if((hrefAttributeRightMenu.toLowerCase().contains(afterClickUrl.toLowerCase())) || (afterClickUrl.toLowerCase().contains(hrefAttributeRightMenu.toLowerCase())) )
										{
											driver.OutputStream("  ...." + "Link '"+ rightMenuCategoryName +"' is redirecting to correct URL, Hence Verified");
											if (driver.checkDomain( testParameters[3]))
												driver.OutputStream( "  ....Hence the domain is OK  for "+ rightMenuCategoryName);
											else{
												driver.OutputStream( "  ...." + "Domain not verified. Known Issue.");
												//footerLinksValidated = false;
											}
										}
										else{
											driver.OutputStream( "  ...." + "Link '"+ rightMenuCategoryName +"' is not redirecting to correct URL, Hence not Verified");
											driver.OutputStream( "  ...." + "Domain not verified. Known Issue.");
											//footerLinksValidated = false;
										}
									}
									else{
										driver.OutputStream( "  ....Redirecting link is found blank for "+ rightMenuCategoryName);
										rightMenuValidated = false;
									}
	
									driver.get(testParameters[3]);    // navigating to Dispatch page
									driver.wait(100);
									//driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely
									if(driver.getCurrentUrl().equalsIgnoreCase(testParameters[3]))
									{
										if(driver.isElementAvailable("xPath", xpathTopMenu))
											driver.findElement( "Xpath", xpathTopMenu, 15).click();
											driver.wait(100);
										if(driver.isElementAvailable( "xPath", xPathLeftMegaMenu+"/a"))
											driver.findElement( "Xpath", xPathLeftMegaMenu+ "/a", 15).click();
											driver.wait(100);
									}
									else
									{
										driver.OutputStream("  ...." + "Not the correct link, so diverting to correct link");
										driver.get(testParameters[3]);
										driver.wait(100);
										if(driver.isElementPresent( "Xpath",xpathTopMenu))
										{
											if(driver.isElementAvailable( "xPath", xpathTopMenu))
												driver.click("xPath", xpathTopMenu);
											driver.wait(100);
											if(driver.isElementAvailable( "xPath", xPathLeftMegaMenu+"/a"))
												driver.click( "xPath", xPathLeftMegaMenu+ "/a");
											driver.wait(100);
										}
									}
									
								}  // if tab size is > 1
							}else{
								driver.OutputStream("  ....Redirecting link is found Wrong for "+ rightMenuCategoryName);
								rightMenuValidated = false;
							}
						}
						catch (StaleElementReferenceException sere) {
							rightMenuValidated = false;
							driver.OutputStream( "  ....StaleElementReferenceExceptionfor '"+rightMenuCategoryName+"' on Link '" + leftMenuCategoryNames+"'. Move on to next link...");
							driver.OutputStream( "  ....StaleElementReferenceException message is " + sere.getMessage());
							driver.wait(100);
							driver.click( "xPath", xpathTopMenu);
							driver.wait(100);
							driver.click( "xPath", xPathLeftMegaMenu+ "/a"); 
							continue rightMenuContinue;
							
						}
						catch (NoSuchElementException nsee) {
							rightMenuValidated = false;
							driver.OutputStream( "  ....NoSuchElementException for '"+rightMenuCategoryName+"' on Link '" + leftMenuCategoryNames + "'. Move on to next link...");
							driver.OutputStream( "  ....NoSuchElementException message is " + nsee.getMessage());
							do{
								driver.wait(100);
								driver.click( "xPath", xpathTopMenu);
								driver.wait(100);
								driver.click( "xPath", xPathLeftMegaMenu+ "/a"); 
//								driver.wait(100);
								//System.out.println(rightMenuIndex + "  is the rightMenu Index >> " +rightMenuIndex--+" is the rightmenu index after reduction");
								rightMenuIndex--;
								index++;
								continue rightMenuContinue;
								}while(index<=1);
						}catch (ElementNotVisibleException e) {	
							
/*							File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					              try {
									FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")	+ "\\reports\\images\\" +driver.logDate()
											+"_"+rightMenuCategoryName
											+".png"));
								} catch (IOException e1) {
									e1.printStackTrace();
								}*/
							driver.OutputStream("  ....ElementNotVisibleException for '"+rightMenuCategoryName+"' on Link '" + leftMenuCategoryNames + "'. Move on to next link...");
							driver.OutputStream( "  ....ElementNotVisibleException message is " + e.getMessage());
							driver.wait(100);
							driver.click( "xPath", xpathTopMenu);
							driver.wait(100);
							driver.click( "xPath", xPathLeftMegaMenu+ "/a"); 
							continue rightMenuContinue;
						}catch (IndexOutOfBoundsException e) {
							e.printStackTrace();
							continue rightMenuContinue;
						}
						catch (InterruptedException e) {
							e.printStackTrace();
						}

					}  // for loop ends here 
			}else{
				driver.OutputStream( "  ...." + "Left Mega Menu name and Right Mega Menu header is different, so this test will be skipped");
				rightMenuValidated = false;
			}
		}else{
			driver.OutputStream( "  ...." + "Right Menu Categories are empty for Left Menu Category option : "+leftMenuCategoryNames
					+" for Top Menu option-"+topMenuNames);
			rightMenuValidated = false;
		}

		return rightMenuValidated;
	}

	private static boolean validateRightMenuWithOpenLink(Driver driver, String xpathTopMenu, String xPathLeftMegaMenu,String topMenuNames, String leftMenuCategoryNames, String...testParameters){
		boolean rightMenuWithOpenLinkValidated = true;
		String hrefAttribute = new String();
		String baseURL = testParameters[3];
		//		String xPathRightMegaMenu = new String();
		//		String rightMenuHeadlineName = new String();

		hrefAttribute = driver.findElement( testParameters[4], xPathLeftMegaMenu + "/a", 30).getAttribute("href");

		try {
			
//			Thread.sleep(2000);
			
			driver.wait(300);
			driver.checkDomain( testParameters[3]);
			//Thread.sleep(2000);
			driver.wait(100);
			driver.get(testParameters[3]);
			driver.wait(100);
			ArrayList<String> tabNew = new ArrayList<String>(driver.getWindowHandles());

			// if there are more windows then go inside if
			if(tabNew.size()>1)
			{

				driver.switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window

				String tabSwitchUrl = driver.getCurrentUrl();

				if((hrefAttribute.length()!=0) && (tabSwitchUrl.length() !=0))
				{
					if((hrefAttribute.toLowerCase().contains(tabSwitchUrl.toLowerCase())) || (tabSwitchUrl.toLowerCase().contains(hrefAttribute.toLowerCase())) )
					{
						driver.OutputStream( "  ...." + "Link '"+ leftMenuCategoryNames +"' is redirecting to correct URL, Hence Verified");
						if (driver.checkDomain( baseURL))
							driver.OutputStream( "  ....Hence the domain is OK  for "+ leftMenuCategoryNames);
						else{
							driver.OutputStream( "  ...." + "Domain not verified. Known Issue.");
							//footerLinksValidated = false;
						}
					}
					else{
						driver.OutputStream( "  ...." + "Link '"+ leftMenuCategoryNames +"' is not redirecting to correct URL, Hence not Verified");
						driver.OutputStream( "  ...." + "Domain not verified. Known Issue.");
						//footerLinksValidated = false;
					}
				}
				else{
					driver.OutputStream( "  ....Redirecting link is found blank for "+ leftMenuCategoryNames);
					rightMenuWithOpenLinkValidated = false;
				}

				driver.switchtoMainWindow( testParameters[3]);   // getting back to main window
				driver.wait(100);
				//driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely
				driver.click( "xPath", xpathTopMenu);
				driver.wait(100);
				driver.click( "xPath", xPathLeftMegaMenu);
				driver.wait(100);

			}else{

				String afterClickUrl = driver.getCurrentUrl();

				//	domains = driver.click("xPath", xPathLeftMenuCatLinks, testParameters[3], testParameters);
				if((hrefAttribute.length()!=0) && (afterClickUrl.length() !=0))
				{
					if((hrefAttribute.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttribute)) )
					{
						driver.OutputStream( "  ...." + "Link '"+ leftMenuCategoryNames +"' is redirecting to correct URL, Hence Verified");
						if (driver.checkDomain( baseURL))
							driver.OutputStream( "  ....Hence the domain is OK  for "+ leftMenuCategoryNames);
						else{
							driver.OutputStream( "  ...." + "Domain not verified. Known Issue.");
							//footerLinksValidated = false;
						}
					}
					else{
						driver.OutputStream("  ...." + "Link '"+ leftMenuCategoryNames +"' is not redirecting to correct URL, Hence not Verified");
						driver.OutputStream( "  ...." + "Domain not verified. Known Issue.");
						//footerLinksValidated = false;
					}
				}
				else{
					driver.OutputStream( "  ....Redirecting link is found blank for "+ leftMenuCategoryNames);
					rightMenuWithOpenLinkValidated = false;
				}
				
		
				driver.get(testParameters[3]);    // navigating to Dispatch page
				driver.wait(100);
				driver.click("xPath", xpathTopMenu);
				driver.wait(100);
				driver.click( "xPath", xPathLeftMegaMenu);
				driver.wait(100);
			}
		}
		catch (StaleElementReferenceException sere) {
			rightMenuWithOpenLinkValidated = false;
			driver.OutputStream("  ....StaleElementReferenceException on Link '" + leftMenuCategoryNames+"'. Move on to next test...");
			driver.OutputStream( "  ....StaleElementReferenceException message is " + sere.getMessage());
			driver.click("xPath", xpathTopMenu);
			//driver.wait(100);
			driver.click( "xPath", xPathLeftMegaMenu);
			//driver.wait(100);

		}
		catch (NoSuchElementException nsee) {
			rightMenuWithOpenLinkValidated = false;
			driver.OutputStream("  ....NoSuchElementException on Link '" + leftMenuCategoryNames + "'. Move on to next test...");
			driver.OutputStream( "  ....NoSuchElementException message is " + nsee.getMessage());
			driver.click("xPath", xpathTopMenu);
			//driver.wait(100);
			driver.click( "xPath", xPathLeftMegaMenu);
			//driver.wait(100);

		}catch (ElementNotVisibleException e) {	
			//continue rightMenuContinue;
			 File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	           //The below method will save the screen shot in d drive with name "screenshot.png"
/*	              try {
					FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")	+ "\\reports\\images\\" +driver.logDate()
							+"_"+leftMenuCategoryNames
							+".png"));
				} catch (IOException e1) {
					e1.printStackTrace();
				}*/
			rightMenuWithOpenLinkValidated = false;
			driver.OutputStream( "  ....ElementNotVisibleException on Link '" + leftMenuCategoryNames + "'. Move on to next test...");
			driver.OutputStream( "  ....ElementNotVisibleException message is " + e.getMessage());
			driver.click( "xPath", xpathTopMenu);
			driver.click( "xPath", xPathLeftMegaMenu);

		}catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}


		return rightMenuWithOpenLinkValidated;
	}
	
	
}
