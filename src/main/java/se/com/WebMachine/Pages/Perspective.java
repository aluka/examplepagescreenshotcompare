package se.com.WebMachine.Pages;

import se.com.WebMachine.Tools.Driver;

public class Perspective {
	private static final String pageContentXpath = "//*[@class='main']";
	private static final String perspectiveXpath = pageContentXpath + "//section[@class='perspectives']";

	public static boolean validatePerspective(Driver driver) {
		if (driver.isElementAvailable("Xpath", perspectiveXpath, 5)) {
			return validatePerspectivesColumn(driver) & validateLoadMore(driver);
		} else {
			driver.OutputStream("There is no perspective in this page. Skip Test.");
			return false;
		}
	}

	private static boolean validatePerspectivesColumn(Driver driver) {
		int size = driver.findElements("Xpath", perspectiveXpath + "/div[@class='column-article']").size();
		if (size == 2) {
			driver.OutputStream("There is 2 column in perspectives. As expected. Test PASSED.");
			return true;
		} else {
			driver.OutputStream("There is " + size + " column in perspectives. Expected is 2. Test FAILED", false);
			return false;
		}
	}

	private static boolean validateLoadMore(Driver driver) {
		int numberOfArticlesBefore = getNumOfPerspectiveArticles(driver);
		if (!clickLoadMore(driver)) {
			driver.OutputStream("Cant click on 'LOadMore' button. Continue testing.");
			return true;
		}
		int numberOfArticlesAfter = getNumOfPerspectiveArticles(driver);
		if (numberOfArticlesAfter == numberOfArticlesBefore + 4) {
			driver.OutputStream("Perspective articles load more test PASSED");
			return true;
		} else {
			driver.OutputStream("Perspective articles load more test FAILED", false);
			return false;
		}
	}

	private static int getNumOfPerspectiveArticles(Driver driver) {
		return driver.findElements("Xpath", perspectiveXpath + "//article").size();
	}

	private static boolean clickLoadMore(Driver driver) {
		if (!driver.isElementAvailable("Xpath", perspectiveXpath + "/*[@class='load']", 2)) {
			driver.OutputStream("There is no 'loadMore' button in this perspective.");
			return false;
		}
		driver.click("Xpath", perspectiveXpath + "/*[@class='load']");
		driver.wait(100);
		driver.waitToLoad();
		return true;
	}
}
