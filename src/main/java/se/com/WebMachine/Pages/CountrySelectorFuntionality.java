package se.com.WebMachine.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import se.com.WebMachine.Pages.HomePage;
import se.com.WebMachine.Tools.Driver;

public class CountrySelectorFuntionality {
	
	private static By countrySelector = By.id("header-country-selector");
	private static By selectCountry = By.className("choose-country");
	private static By northAmerica = By.xpath("//*[@id='country-list-menu-v1']/div/ul/li[4]");
	private static By Asia = By.xpath("//*[@id='country-list-menu-v1']/div/ul/li[2]");
	private static By Europe = By.xpath("//*[@id='country-list-menu-v1']/div/ul/li[3]");
	private static By USA = By.xpath("//*[@id='country-list-menu-v1']/div/ul/li[4]/ul/li[18]/a");
	private static By Malaysia = By.xpath("//*[@id='country-list-menu-v1']/div/ul/li[2]/ul/li[18]/a");
	private static By France = By.xpath("//*[@id='country-list-menu-v1']/div/ul/li[3]/ul/li[14]/a");
	private static By Global = By.xpath("//*[@id='country-list-menu-v1']/div/ul/li[1]");
	private static By global = By.xpath("//*[@id='country-list-menu-v1']/div/ul/li[1]/ul/li[1]/a");
	
	public static boolean landingGlobal(Driver driver, String url){
		boolean status = false;
		driver.get(url);
		driver.wait(100);
		driver.waitToLoad("Schneider");
		status = HomePage.countryNameValid(driver, "Change");
		return status;
	}

	public static boolean cookieBlock(Driver driver, String url){
		boolean status = false;
		driver.get(url);
		gotoCountry(driver);
		String countryIdentifier = "FR/FR";
		status = HomePage.countryNameValid(driver, countryIdentifier);
		if(status)
			driver.OutputStream("driver landed to "+countryIdentifier+" page using country selector. Cookie should be set.\n Clearing cookies.");
		else{
			driver.OutputStream("driver did not landed to "+countryIdentifier+". May be country selector links are not working.", false);	
			return false;
		}
		clearCookies(driver);
		driver.OutputStream("Going to Global page using URL. driver should land to Global Dispatch page as cookies are removed.");
		gotoGlobalURL(driver);
		status = HomePage.countryNameValid(driver, "Change");
		if(status)
			driver.OutputStream("driver did not Re-routed to "+countryIdentifier+". Test PASSED");
		else if(HomePage.countryNameValid(driver, countryIdentifier))
			driver.OutputStream("driver re-routed to USA. Test FAILED", false);
		else
			driver.OutputStream("driver re-router to un-expected location. Test FAILED", false);
		return status;
	}
	
	public static boolean cookieAccept(Driver driver, String url){
		boolean status = false;
		driver.get(url);
		gotoCountry(driver);
		String countryIdentifier = "FR/FR";
		status = HomePage.countryNameValid(driver, countryIdentifier);
		if(status)
			driver.OutputStream("driver landed to "+countryIdentifier+" page using country selector. Cookie should be set.");
		else{
			driver.OutputStream("driver did not landed to "+countryIdentifier+". May be country selector links are not working.", false);	
			return false;
		}
		driver.OutputStream("Going to Global page URL. driver should not land to Global Dispatch page as cookies should setup for USA");
		gotoGlobalURL(driver);
		status = HomePage.countryNameValid(driver, countryIdentifier);
		if(status)
			driver.OutputStream("driver did Re-routed to "+countryIdentifier+". Test PASSED");
		else if(HomePage.countryNameValid(driver, "Chnage"))
			driver.OutputStream("driver did not re-routed to "+countryIdentifier+". Test FAILED", false);
		else
			driver.OutputStream("driver re-router to un-expected location. Test FAILED", false);
		return status;
	}
	
	public static boolean reRouteGlobal(Driver driver, String url){
		boolean status = false;
		if(cookieAccept(driver, url)){
			gotoGlobal(driver);
			status = HomePage.countryNameValid(driver, "Change");
			if(status){
				driver.OutputStream("Country selector reset the cookie to Global. Test PASSED.");
			}else{
				driver.OutputStream("Country selector could not reset the cookie. Test FAILED.", false);
			}
		}else{
			driver.OutputStream("Country selector could not set country cookie. Skip the test.", false);
		}
		return status;
	}
	
	public static boolean changeCountry(Driver driver, String url){
		boolean status = false;
		if(cookieAccept(driver, url)){
			driver.OutputStream("Now in USA. Going to change select different country using Country Selctor.");
			gotoCountry(driver, "Europe", "United Kingdom");
			status = HomePage.countryNameValid(driver, "UK/EN");
			if(status){
				driver.OutputStream("Country selector take us to 'UK', Test PASSED.");
			}
		}else{
			driver.OutputStream("Could not setup cookie for 'UK' using Country Selctor. Skip Testing", false);
			return false;
		}
		return status;
	}
	
	public static boolean validateURL(Driver driver, String StartURL, String url, String CountryName){
		boolean status = false;
		if(cookieAccept(driver, StartURL)){
			clearCookies(driver);
			driver.get(url);
			driver.wait(100);
			driver.waitToLoad( 20, "Schneider");
			driver.wait(100);
			driver.waitToLoad();
			status = HomePage.countryNameValid(driver, CountryName);
			if(status)	driver.OutputStream("Test PASSED");
			else driver.OutputStream("Test Failed", false);
		}else{
			driver.OutputStream("Going to skip this test.", false);
			return false;
		}
		return status;
	}
	
	public static boolean validateURL2(Driver driver, String StartURL, String url, String CountryName){
		boolean status = false;
		if(changeCountry(driver, StartURL)){
			driver.get(url);
			driver.wait(100);
			driver.waitToLoad( 20, "Schneider");
			driver.wait(100);
			driver.waitToLoad();
			status = HomePage.countryNameValid(driver, CountryName);
			if(status)	driver.OutputStream("Test PASSED");
			else driver.OutputStream("Test Failed", false);
		}else{
			driver.OutputStream("Going to skip this test.", false);
			return false;
		}
		return status;
	}
	
	private static void gotoUSA(Driver driver){
//		driver.wait(200);
//		driver.waitToLoad( 20, "Schneider");
//		driver.get( "http://www.schneider-electric.com/us/en/?preferredCountry=yes");
		driver.wait(100);
		driver.waitToLoad();
		driver.OutputStream(driver.getTitle()+ " is loaded. Going to use country selector to go to 'USA'.");
		driver.click( countrySelector);
		driver.wait(200);
		if(!driver.isElementAvailable( selectCountry)){
			driver.click( countrySelector);
			driver.wait(200);			
		}
		driver.click( selectCountry);
		driver.wait(200);
		driver.click( northAmerica);
		driver.wait(200);
		driver.click( USA);
		driver.wait(200);
		driver.waitToLoad( 20, "Schneider");
	}
	
	private static void gotoGlobal(Driver driver){
/*		driver.wait(200);
		driver.waitToLoad( 20, "Schneider");
		driver.get( "http://www.schneider-electric.com/ww/en/?preferredCountry=yes");*/
		driver.wait(100);
		driver.waitToLoad();
		driver.OutputStream(driver.getTitle()+ " is loaded. Going to use country selector to go to 'GLobal-EN'.");
		driver.click( countrySelector);
		driver.wait(200);
		if(!driver.isElementAvailable( selectCountry)){
			driver.click( countrySelector);
			driver.wait(200);			
		}
		driver.click( selectCountry);
		driver.wait(200);
		driver.click( Global);
		driver.wait(200);
		driver.click( global);
		driver.wait(200);
		driver.waitToLoad( 20, "Schneider");
	}
	
	private static void gotoCountry(Driver driver, String continent, String country){
		driver.get("http://www.schneider-electric.com/uk/en/?preferredCountry=yes");
/*		driver.wait(200);
		driver.waitToLoad( 20, "Schneider");
		driver.wait(100);
		driver.waitToLoad(driver);
		driver.OutputStream(driver.getTitle()+ " is loaded. Going to use country selector to go to 'USA'.");
		driver.click( countrySelector);
		driver.wait(200);
		if(!driver.isElementAvailable( selectCountry)){
			driver.click( countrySelector);
			driver.wait(200);			
		}
		driver.click( selectCountry);
		driver.wait(200);
		driver.click( Asia);
		driver.wait(200);
		driver.click( Malaysia);
		driver.wait(200);
		driver.waitToLoad( 20, "Schneider");*/
	}
	private static void gotoCountry(Driver driver){
		driver.wait(200);
		driver.waitToLoad(20, "Schneider");
		driver.wait(100);
		driver.waitToLoad();
		driver.OutputStream(driver.getTitle()+ " is loaded. Going to use country selector to go to 'USA'.");
		driver.click(countrySelector);
		driver.wait(200);
		if(!driver.isElementAvailable( selectCountry)){
			driver.click(countrySelector);
			driver.wait(200);			
		}
		driver.click(selectCountry);
		driver.wait(200);
		driver.click( Europe);
		driver.wait(200);
		driver.click(France);
		driver.wait(200);
		driver.waitToLoad(20, "Schneider");
	}
	
	
	private static void gotoGlobalURL(Driver driver){
		driver.get("http://www.schneider-electric.com");
		driver.wait(200);
		driver.waitToLoad(20, "Schneider");
	}
	
	public static void clearCookies(Driver driver){
		driver.get("http://www..schneider-electric.com/ww/en/");
		driver.manage().deleteCookieNamed("SECOUNTRYCODE");
		driver.manage().deleteAllCookies();
		driver.navigate().back();
	}
}
