package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import se.com.WebMachine.Tools.Driver;
import se.com.WebMachine.dataSourceReader.Xls_Reader;

public class HomePage {
	public static By mainHeader = By.className("main-menu");
	public static By SkipVideo = By.className("skip-video");
	public static String flagXpath = "//*[@class='notification-title-icon']/img";
	private static String SElogoXpath = "//*[@class='logo-header' or @class='logoContainer' or @id='odmn_logo']//img";
	private static By searchInput = By.className("search-bar");
	private static By countrySelector = By.id("header-country-selector");
	private static String countrySelectorXpath = "//*[@id='header-country-selector' or @class= 'odmn_country']";
	private static String headerSeperatorXpath = "//*[@id='header-link-separator']";
	private static String headerSectionXpath = "//ul[@class='tier-one']/li";
	private static By countryConfirm = By.className("confirm-location-btn");
	// private static String baseURL = "US/EN";
	private static By chatClose = By.className("close-button");
	private static String socialXpath = "//*[@class='footer-social']//a";
	private static String footerContainerXpath = "//*[@class='footer-container']";
	private static String emailSubcribeXpath = "//*[@class='footer-container']//input[@id='Email']";
	private static String lifeisOnfooterXpath = "//*[@class='footer-main']//img";
	private static String footerLinkXpath = "//*[@class ='footer-links']//a";
	// private static String footerLegalXpath="//*[@class='footer-main']";
	private static By footerLegal = By.partialLinkText("Privacy Policy");
	private static String nameOfCountry = "US/EN";
	private static String greenFooterXpath = "//ul[@class ='footer-about']";
	private static By selectCountry = By.className("choose-country");
	private static String ContinentListXpath = "//ul[@class='region-list']/li";
	private static String GobalMassageXpath = ContinentListXpath + "[1]";
	private static By noSubscription = By.xpath("//*[@class='buttons']/a[2]");
	private static final String GreenHeaderXpath = "//*[@class='main-menu']";
	private static final By whiteBar = By.id("trail");
	private static final String ceraselContainer = "//section[contains(@id,'slider')]";
	private static final String cookieNotificationBar = "//div[@id='cookie-notification']";
	private static final String cookieNotificationClose = cookieNotificationBar
			+ "//*[@class='notification-close']/span";
	public static int pageOfset = 0;
	public static String domain = "www";

	public static String getPageDomain(Driver driver) {
		String url = driver.getCurrentUrl();
		return getPageDomain(url);
	}

	public static String getPageDomain(String url) {
		if (url.contains("-sqe"))
			domain = "sqe";
		else if (url.contains("-int"))
			domain = "int";
		else if (url.contains("-pre"))
			domain = "pre";
		else if (url.contains("-prd"))
			domain = "prd";
		else
			domain = "www";
		if (url.contains(".staging."))
			domain = domain + "-staging";
		return domain;
	}

	public static boolean validateFlag(Driver driver, String perameter) {
		boolean status = false;
		driver.OutputStream(" Perameter is: " + perameter);
		driver.click("Xpath", countrySelectorXpath);
		driver.wait(500);
		if (driver.isElementVissible("Xpath", flagXpath, 5)) {
			driver.OutputStream("Flag is visible in the page. Validating the flag.");
			if (driver.findElement("Xpath", flagXpath, 2).getAttribute("src").contains(perameter + ".gif")) {
				driver.OutputStream("Flag validated. Continue testing.");
				status = true;
			} else
				driver.OutputStream("flag source is not validated. Skip testing.", false);
		} else {
			driver.OutputStream("There is no flag in the country selector of this page. Skip Validating", false);
		}
		closeCountrySelector(driver);
		return status;
	}

	public static boolean searchValid(Driver driver, int priority) {
		boolean status = false;
		status = driver.isElementAvailable(searchInput);
		if (status) {
			driver.OutputStream("Search input is visible in page: " + driver.getCurrentUrl());
		}
		return status;
	}

	public static boolean cookieDesimalValidation(Driver driver) {
		if (domain.contains("prd") || domain.contains("pre")) {
			if (driver.isElementVissible("Xpath", cookieNotificationBar, 5)) {
				boolean status = true;
				if (driver.findElement("Xpath", cookieNotificationBar, 2).getText().length() > 0)
					driver.OutputStream("There is some massage in the cookie desimal ber. Continue testing.");
				else {
					driver.OutputStream("There is no massage in the cookie desimal!!!!", false);
					status = false;
				}
				if (driver.isElementAvailable("Xpath", cookieNotificationClose))
					driver.click("Xpath", cookieNotificationClose);
				else {
					driver.OutputStream("Cant find notification close button. Skip testing.");
					status = false;
				}
				return status;
			} else {
				driver.OutputStream("Cookie Notification is not visible in the page.", false);
				return false;
			}
		}
		return true;
	}
	public static String getCountryName(Driver driver){
		String Name = "US";
		WebElement country = driver.findElement("Xpath", countrySelectorXpath, 15);
		if(country!=null)Name = country.getText();
		if(Name.contains(" "))
			Name = Name.substring(0, Name.indexOf(" "));
		return Name;
	}
	public static boolean countryNameValid(Driver driver, String name) {
		boolean status = true;
		// status = status & driver.readServerError(driver);
		if (!status) {
			driver.OutputStream("Server Error!!!!!.........", false);
		}
		status = status & driver.isElementAvailable("Xpath", countrySelectorXpath);
		nameOfCountry = name;
		// System.out.println("Expected country name is: "+ name);
		try {
			if (status) {
				WebElement country = driver.findElement("Xpath", countrySelectorXpath, 15);
				driver.OutputStream("Country name is visible. Country name is: " + country.getText());
				// driver.takeScreenShot( name);
				status = country.getText().contains(name) || name.contains(country.getText());
			}
		} catch (Exception e) {
			driver.OutputStream("Country name is not varrifier!!!. expected name is: " + name + " for this url: "
					+ driver.getCurrentUrl(), false);
			driver.takeScreenShot("CountryNameNotVarrified");
			status = false;
		}
		/*
		 * if(name.contains("/")) status = status & validateFlag(
		 * name.substring(0, 2).toLowerCase());
		 */
		closechat(driver);
		driver.wait(500);
		return status;
	}

	public static boolean validateCountrySelector(Driver driver, int priority) {
		boolean status = driver.isElementAvailable(countrySelector);
		String locator = "xpath";
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		String CountryName, CountryURL;
		driver.navigate().refresh();
		try {
			if (status) {
				driver.click(driver.findElement(countrySelector));
				driver.wait(100);
				driver.click(selectCountry);
				driver.wait(200);
//				 status = status && validateCountrySelectorMassage(driver);
				// driver.OutputStream("Country Selector massage is validated.
				// Continue with testing country selector links.");
				if (priority > 1) {
					List<WebElement> Continent = driver.findElements(locator, ContinentListXpath + "/span");
					for (int continentNo = 1; continentNo < Continent.size() && Continent.size() > 1; continentNo++) {
						// in production size = 6
						driver.click(Continent.get(continentNo));
						driver.wait(100);
						driver.OutputStream(
								Continent.get(continentNo).getText() + " is selected from left country list.");
						List<WebElement> Country = driver.findElements(locator,
								ContinentListXpath + "[" + (continentNo + 1) + "]//a");
						for (int countryNo = 0; countryNo < Country.size() && priority > 2; countryNo++) {
							CountryName = Country.get(countryNo).getText();
							CountryURL = Country.get(countryNo).getAttribute("href");
							if (CountryName.length() < 1) {
								driver.navigate().to(url);
								driver.wait(100);
								driver.waitToLoad(title);
								driver.wait(100);
								driver.click(countrySelector);
								driver.wait(200);
								driver.click(selectCountry);
								driver.wait(200);
								Continent = driver.findElements(locator, ContinentListXpath + "/span");
								driver.click(Continent.get(continentNo));
								driver.wait(300);
								Country = driver.findElements(locator,
										ContinentListXpath + "[" + (continentNo + 1) + "]//a");
								CountryName = Country.get(countryNo).getText();
								CountryURL = Country.get(countryNo).getAttribute("href");
							}
							driver.OutputStream("Script is going to select : " + CountryName);
							if (CountryName.contains("Moldavia") || CountryName.contains("East Mediterranean")
									|| CountryName.contains("Belgium (Dutch)")) {
								driver.OutputStream("Known Error. Skip Testing.");
								continue;
							}
							if (CountryURL.contains("http://")) {
								driver.click(Country.get(countryNo));
								driver.wait(300);
								driver.waitToLoad();
								/*
								 * -------------------- Start Testing
								 * destination
								 * page-------------------------------
								 */
								driver.OutputStream("---------------------------------------Testing " + CountryName
										+ " Country Page-----------------------------");
								boolean statusLocal = schneiderLogoVisible(driver);
								statusLocal = statusLocal & validateSelectorCountryName(driver, CountryName);
								// statusLocal = statusLocal &
								// driver.checkCountryDomain(driver);
								status = status & statusLocal;
								if (statusLocal) {
									driver.OutputStream(CountryName + " is validated.");
								} else {
									driver.OutputStream(
											CountryName + " is not validated. url: " + driver.getCurrentUrl(), false);
									driver.takeScreenShot(CountryName + "NotValidated");
								}
								driver.wait(100);
								driver.OutputStream("----------------------------------------Done with " + CountryName
										+ " Page Test------------------------");
								/*-------------------------------End testing destination page---------------------------------*/
								// if(!(driver.getCurrentUrl().equals(url))){
								driver.get(url);
								driver.waitToLoad(title);
								driver.wait(100);
								driver.click(countrySelector);
								driver.wait(200);
								driver.click(selectCountry);
								driver.wait(200);
								Continent = driver.findElements(locator, ContinentListXpath + "/span");
								driver.click(Continent.get(continentNo));
								;
								driver.wait(300);
								Country = driver.findElements(locator,
										ContinentListXpath + "[" + (continentNo + 1) + "]//a");
								// }
							} else {
								driver.OutputStream(CountryName + " Country link is not valid." + CountryURL, false);
								driver.takeScreenShot("CountryLinkNotValid");
							}
						}
					}
				}
			} else {
				driver.OutputStream("Warning!!!!   Country name is not visible in the page.", false);
				driver.takeScreenShot("CountryNameNotVisible");
				status = false;
			}
		} catch (TimeoutException e) {
			driver.OutputStream("Time Out Error: " + e, false);
			driver.takeScreenShot("TimeOutException");
			status = false;
		} catch (IndexOutOfBoundsException e) {
			driver.OutputStream("Index Out of bound error: " + e, false);
			driver.takeScreenShot("IndexOutOfBOundException");
			status = false;
		} catch (Exception e) {
			driver.OutputStream("Exception: " + e, false);
			driver.takeScreenShot("Exception");
			status = false;
		}
		closeCountrySelector(driver);
		return status;
	}
	private static boolean validateCountrySelectorMassage(Driver driver) {
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		String locator = "Xpath";
		boolean status = true;
		List<WebElement> massage = driver.findElements( locator, GobalMassageXpath + "//p");
		List<WebElement> titleName = driver.findElements( locator, GobalMassageXpath + "//a");
		status = status && massage.size() == 2;
		if (massage.size() == 2) {
			String massage1 = massage.get(0).getText();
			String massage2 = massage.get(1).getText();
			if (massage1.length() < 1 || massage2.length() < 1) {
				/*
				 * Try Second Time to read massage.
				 */
				driver.navigate().to(url);
				driver.wait(100);
				driver.waitToLoad( title);
				driver.wait(100);
				driver.click( countrySelector);
				driver.wait(200);
				driver.click( selectCountry);
				driver.wait(200);
				driver.wait(200);
				massage = driver.findElements( locator, GobalMassageXpath + "//p");
				titleName = driver.findElements( locator, GobalMassageXpath + "//a");
				massage1 = massage.get(0).getText();
				massage2 = massage.get(1).getText();
				status = status && massage1.length() > 0 && massage2.length() > 0;
				if (status)
					driver.OutputStream(
							"Country Selector massage is validated. Continue with testing country selector links.");
				else {
					driver.OutputStream("There is missing massage in the country selector.", false);
					driver.takeScreenShot( "CountrySelectorMassageMissing");
				}
			}
			driver.OutputStream(titleName.get(0).getText() + " massage in the country selector is: " + massage1);
			driver.OutputStream(titleName.get(1).getText() + " massage in the country selector is: " + massage2);
		} else {
			driver.OutputStream("Global massage in both language is not vissible.", false);
			driver.takeScreenShot( "GobalMassageNotVisible");
			status = false;
		}
		return false;
	}
	public static boolean validateHeaderLinkSeperator(Driver driver) {
		if (driver.isElementAvailable("Xpath", headerSeperatorXpath, 2)) {
			boolean status = driver.isElementAvailable("Xpath", headerSeperatorXpath + "/a", 2);
			if (status) {
				driver.OutputStream("There is a link to change language of the selected country. Continue Testing.");
			} else {
				driver.OutputStream("There is no link in the language seperator. Test Failed.", false);
			}
			return status;
		} else {
			driver.OutputStream("There is no Language Seperator in the eyebrow of this page.", false);
			return true;
		}
	}

	public static boolean schneiderLogoVisible(Driver driver) {
		boolean status = true;
		if (!driver.getCurrentUrl().contains(".schneider-electric.")) {
			driver.OutputStream("This is not a Schneider Electric WebMachine page. Skip Testing");
			return true;
		}
		try {
			status = driver.isElementAvailable("Xpath", SElogoXpath);
			if (status) {
				Dimension size = driver.findElement("Xpath", SElogoXpath, 15).getSize();
				int height = size.height;
				int width = size.width;
				driver.OutputStream("SE logo is visible. logo size is: " + height + "x" + width, false);
				status = height > 0 & width > 0;
			} else {
				driver.OutputStream("Logo is not visible in this page. " + driver.getCurrentUrl(), false);
				driver.takeScreenShot("SELogoNotVisible");
			}
			String pageTitle = driver.getTitle();
			if (pageTitle.contains("Error") || pageTitle.contains("404")) {
				driver.OutputStream("  ....ERROR! Link " + driver.getCurrentUrl() + " is Broken", false);
				driver.takeScreenShot("ErrorLink");
				status = false;
			}
		} catch (Exception e) {
			driver.OutputStream("Error is: " + e, false);
			driver.takeScreenShot("Exception");
			status = false;
		}
		return status;
	}

	public static boolean socialMediaLinks(Driver driver, int priority) {

		boolean status = true;
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();

		List<WebElement> socialMediaElements = driver.findElements("xPath", socialXpath);

		driver.OutputStream("Total number or Social Media links present is the page is: " + socialMediaElements.size());

		status = socialMediaElements.size() > 0;
		for (int socialNo = 0; socialNo < socialMediaElements.size() && status && priority > 2; socialNo++) {
			String mediaName = socialMediaElements.get(socialNo).getAttribute("title");
			String mediaLink = socialMediaElements.get(socialNo).getAttribute("href");
			String companyName = "Schneider";
			driver.OutputStream("Element no: " + (socialNo + 1) + " Social Media link for " + mediaName + " is: "
					+ mediaLink + " Going to the link now.");
			socialMediaElements.get(socialNo);
			driver.wait(100);
			boolean status1 = driver.findElement(By.tagName("body")).getText().toLowerCase()
					.contains(mediaName.toLowerCase())
					|| driver.findElement(By.tagName("body")).getText().toLowerCase()
							.contains(companyName.toLowerCase());
			if (status1) {
				driver.OutputStream("Link is valid");
			} else {
				driver.OutputStream("Link is not valid", false);
				driver.takeScreenShot("SocialLinkNotValid" + mediaName);
			}
			// have to input test criteria....
			driver.get(url);
			driver.waitToLoad(title);
			driver.wait(500);
			socialMediaElements = driver.findElements("xPath", socialXpath);
		}
		return status;
	}

	private static boolean validateFooterLegalVissible(Driver driver) {
		if (driver.isElementAvailable(footerLegal, 3)) {
			driver.OutputStream("Footer legal notice is vissible in the page. Continue Testing");
			return true;
		} else {
			driver.OutputStream("Footer Legal notice is not vissible in the page. Test Failed.");
			return false;
		}
	}

	public static boolean validateFooter(Driver driver, int priority) {
		getPageDomain(driver);
		pageOfset = getPageOfset(driver);
		boolean status = true;
		boolean statusLocal;
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		String linkName, linkURL;
		// System.out.println("Priority value is: "+priority);
		// Dimension size = new Dimension(0, 0);
		try {
			Point location = driver.findElement(By.xpath(footerContainerXpath + "/footer")).getLocation();
			// System.out.println("footer location is: "+ location);
			status = status & location.x > 0 & location.y > 0;
			if (location.x > 0 && location.y > 0) {
				driver.OutputStream("Footer is properly positioned in the page");
				// System.out.println("PASSED");
			} else {
				// System.out.println("FAILED");
				driver.OutputStream("Warning!!! ........ footer position is not varified.", false);
				driver.takeScreenShot("FooterPosition" + title.trim());
			}
		} catch (Exception e) {
		}
		closechat(driver);
		closeCountrySelector(driver);
		status = status & driver.isElementAvailable("xPath", emailSubcribeXpath, 3);
		status = status & validateFooterLegalVissible(driver);
		if (driver.isElementAvailable("xPath", emailSubcribeXpath, 2))
			driver.OutputStream("Email subscribtion visible in the page.");
		if (driver.isElementAvailable("xPath", footerContainerXpath, 5)) {
			// status = status & driver.isElementAvailable( "xPath",
			// lifeisOnfooterXpath);
			if (driver.isElementAvailable("xPath", lifeisOnfooterXpath)) {
				Dimension size = driver.findElement("Xpath", lifeisOnfooterXpath, 15).getSize();
				driver.OutputStream("The size of lifeIsOn logo on footer size is: " + size.height + "x" + size.width);
				status = status & size.height > 0 & size.width > 0;
			}
			// status = status & size.height>0 & size.width>0;
			boolean footerLinkAvailable = driver.isElementAvailable("xPath", footerLinkXpath);
			status = status & footerLinkAvailable;
			if (priority > 1 && footerLinkAvailable) {
				try {
					List<WebElement> footerLink = driver.findElements("Xpath", footerLinkXpath);
					for (int linkNo = 0; linkNo < footerLink.size(); linkNo++) {
						linkName = footerLink.get(linkNo).getText();
						linkURL = footerLink.get(linkNo).getAttribute("href");
						driver.OutputStream("Footer link name is: " + linkName);
						driver.OutputStream("Link is: " + linkURL);
						// driver.OutputStream("Title of the link is: "+
						// footerLink.get(linkNo).getAttribute("title"));
						if (priority > 2) {
							driver.scrollToElement(footerLink.get(linkNo), pageOfset);
							footerLink.get(linkNo).click();
							driver.wait(100);
							driver.switchTab();
							driver.waitToLoad();
							if (driver.getCurrentUrl().equalsIgnoreCase(url)) {
								driver.OutputStream(
										"Footer Link " + linkName + " did not go to destination. \n Destination was: "
												+ linkURL + " \n Accual Destination is: " + driver.getCurrentUrl(),
										false);
								driver.takeScreenShot("FooterLink" + linkName + "NotAccessable");
								continue;
							}
							/*-----------------------Test Destination Page---------------------------*/
							driver.OutputStream(
									"..............................................................................");
							// countryNameValid( nameOfCountry, 1);
							statusLocal = validateFooterLink(driver);
							if (statusLocal) {
								if (priority1Test(driver))
									driver.OutputStream(linkName + " is tested and varrified");
							} else
								driver.OutputStream(linkName + " is tested but FAIELD.", false);
							driver.OutputStream(
									"..............................................................................");
							/*------------------------      Test done ----------------------------*/
							driver.closeMultipleTabs();
							driver.getInstance().navigate().to(url);
							driver.wait(100);
							driver.waitToLoad(title);
							driver.wait(200);
							footerLink = driver.findElements("Xpath", footerLinkXpath);
							status = status & statusLocal;
							// go to the links and validate the destination page
							// by calling priority1test()
						}
					}
				} catch (Exception e) {
					driver.OutputStream("Footer links are not accessable for this link: " + driver.getCurrentUrl(),
							false);
					driver.OutputStream("Triggered Exception is: " + e.getMessage());
					driver.takeScreenShot("NotAccessableLinkFooter");
					status = false;
				}
			} else if (!footerLinkAvailable) {
				driver.OutputStream("Footer links are not availabe in the page. " + driver.getCurrentUrl(), false);
				driver.takeScreenShot("FooterLink" + driver.getTitle().trim());
				status = false;
			}
		}
		return status;
	}

	public static boolean GreenHeaderTest(Driver driver, int priority) {
		boolean status = true;

		try {
			if (driver.isElementAvailable("Xpath", GreenHeaderXpath)) {
				Point location = driver.findElement("Xpath", GreenHeaderXpath, 15).getLocation();
				// System.out.println("Position is:" + location);
				status = status & location.x > 0 & location.y > 0;
				driver.OutputStream("Header is positioned properly in the page.");
			} else {
				driver.OutputStream("Main Header is not Visible in the page. Could not varified the location");
			}
			if (driver.isElementAvailable("id", "header-content-v1")) {
				driver.OutputStream("This page contains New Header. Continue with Test");
				if (priority > 2)
					status = status & HeaderNew.greenHeaderTest(driver, priority);
				else
					status = status & GreenHeaderTestNew(driver, priority);
			} else {
				driver.OutputStream("This page contains Old Header. Continue with Test");
				status = true;
				// status = status &
				// OldHeader.validateTopMenu(driver.getInstance(),
				// driver.getCurrentUrl(), priority);
			}
		} catch (NoSuchElementException e) {
			driver.OutputStream("Unknown error. " + e, false);
			status = false;
			// status = OldHeader.validateTopMenu( driver.getCurrentUrl(),
			// priority);
		}
		return status;
	}
	/** After opening each cetagory like product 
	 * ------- check this element is there or not 
	 * -- headerSectionXpath[ItemNo]//h3/a
	 * */
	public static boolean GreenHeaderTestNew(Driver driver, int priority) {
		boolean status = true;
		if (driver.isElementAvailable(countryConfirm)) {
			driver.findElement(countryConfirm).click();
		}
		String url, title, tire2Xpath, tire3Xpath, name, locator;
		url = driver.getCurrentUrl();
		title = driver.getTitle();
		locator = "Xpath";
		List<WebElement> headerSection = driver.findElements(locator, headerSectionXpath);
		if (headerSection.size() != 5) {
			status = false;
			driver.OutputStream("There is only " + headerSection.size() + " menu in menu list. Excepted 5");
		}
		for (int headerNo = 0; headerNo < headerSection.size() && priority > 1; headerNo++) {
			driver.click(headerSection.get(headerNo));
			name = driver.findElement(locator, headerSectionXpath + "[" + (headerNo + 1) + "]/a", 15).getText();
			driver.OutputStream("--------------------------------------------------------------");
			driver.OutputStream(name + " is selected from green header");
			tire2Xpath = headerSectionXpath + "[" + (headerNo + 1) + "]//a[@class='cat-title']";
			List<WebElement> tire2element = driver.findElements(locator, tire2Xpath);
			status = status & tire2element.size() > 0;

			driver.OutputStream("--------------------------------------------------------------");
			driver.OutputStream("No of level 2 links visible under " + name + " is:" + tire2element.size());
			if (!status) {
				driver.OutputStream(name + " sublinks is not vissible!!!!", false);
			}
			for (int tire2No = 0; tire2No < tire2element.size() && priority > 2; tire2No++) {
				driver.OutputStream("Level 2 element name is: " + tire2element.get(tire2No).getText());
			}
			driver.OutputStream("--------------------------------------------------------------");
			tire3Xpath = headerSectionXpath + "[" + (headerNo + 1) + "]//li[not(@style) and not(@class)]";
			driver.wait(100);
			/*---------------- click every single link and verrify the destiantion page------------------*/
			List<WebElement> tire3elements = driver.findElements(locator, tire3Xpath);
			for (int tire3No = 0; tire3No < tire3elements.size() && priority > 2; tire3No++) {
				driver.wait(100);
				driver.OutputStream("Level 3 link name is: " + tire3elements.get(tire3No).getText());
				// driver.OutputStream("Level 3 link url is:
				// "+tire3elements.get(tire3No).getAttribute("href"));
				// if(tire3elements.get(tire3No).getText().length()>0){
				try {
					tire3elements.get(tire3No).click();
					driver.wait(100);
					driver.switchTab();
					status = status & priority1Test(driver);
					driver.closeMultipleTabs();
					driver.get(url);
					driver.waitToLoad(title);
					headerSection = driver.findElements(locator, headerSectionXpath);
					driver.click(headerSection.get(headerNo));
					driver.wait(100);
					tire2element = driver.findElements(locator, tire2Xpath);
					tire3elements = driver.findElements(locator, tire3Xpath);
				} catch (Exception e) {
					driver.OutputStream("Header name " + name + " element name " + tire3elements.get(tire3No).getText()
							+ " is not accessable. element number is: " + (tire3No + 1), false);
					continue;
				}

				// }
				/*---------------- click every single link and verrify the destiantion page------------------*/
			}
		}
		return status;
	}

	public static boolean greenFooter(Driver driver, int priority) {
		boolean sitemap = true;
		boolean privacy = true;
		boolean legal = true;
		boolean status = true;
		if (driver.isElementAvailable(countryConfirm)) {
			driver.findElement(countryConfirm).click();
		}
		status = status & driver.isElementAvailable("Xpath", greenFooterXpath);
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		// String text1 = "Schneider Electric";
		// String text2 ="Site Map";
		// String text3 = "Privacy Policy";
		// String text4 = "Legal";
		if (status) {
			driver.OutputStream("Green Footer is visible at the bottom of this page.");
			// status = status & driver.isElementAvailable( "xpath",
			// greenFooterXpath+"/li");
			if (priority > 1) {
				driver.wait(100);
				// click site map and validate the destination
				List<WebElement> greenFooterLink = driver.findElements("xpath", greenFooterXpath + "//a");
				// int size = greenFooterLink.size();
				status = (greenFooterLink.size() > 2);
				if (status && priority > 2) {
					for (int linkNo = 0; linkNo < greenFooterLink.size(); linkNo++) {
						if (greenFooterLink.get(linkNo).getAttribute("href").contains("sitemap")) {
							driver.click(greenFooterLink.get(linkNo));
							driver.wait(500);
							driver.waitToLoad();
							sitemap = sitemap & schneiderLogoVisible(driver);
							driver.OutputStream("Finish testing 'Sitemap'");
							driver.get(url);
							driver.waitToLoad(title);
						} else if (greenFooterLink.get(linkNo).getAttribute("href").contains("privecy")) {
							driver.click(greenFooterLink.get(linkNo));
							driver.wait(500);
							driver.waitToLoad();
							privacy = privacy & schneiderLogoVisible(driver);
							driver.OutputStream("Finish testing 'Privecy'");
							driver.get(url);
							driver.waitToLoad(title);
						} else if (greenFooterLink.get(linkNo).getAttribute("href").contains("legal")) {
							driver.click(greenFooterLink.get(linkNo));
							driver.wait(500);
							driver.waitToLoad();
							privacy = privacy & schneiderLogoVisible(driver);
							driver.OutputStream("Finish testing 'Legal'");
							driver.get(url);
							driver.waitToLoad(title);
						}
						greenFooterLink = driver.findElements("xpath", greenFooterXpath + "/ul/li/ul/li");
					}
					status = status & sitemap & legal & privacy;
				} else if (!status) {
					driver.OutputStream("No of Elements in the green footer is not as expeted. Number of links are "
							+ greenFooterLink.size() + ". Expected is more then 3", false);
				}
			}
		} else {
			driver.OutputStream("Error!!!.....  Green footer is not visible in the page. Please check the url: " + url,
					false);
		}
		return status;
	}

	private static boolean validateSelectorCountryName(Driver driver, String CountryName) {
		boolean status = true;
		String sheetName = "CountryName";
		int[] location = new int[2];
		String path = Xls_Reader.filename;
		Xls_Reader file = new Xls_Reader(path);
		try {
			location = file.searchData(sheetName, CountryName);
		} catch (Exception e) {
			driver.OutputStream("Data file is not readable.", false);
		}
		if (location[1] > 0) {
			String CountryNameValue = file.getCellData(sheetName, location[0] + 1, location[1]);
			try {
				// driver.wait(100);
				status = driver.findElement("Xpath", countrySelectorXpath, 20).getText().toLowerCase()
						.contains(CountryNameValue.toLowerCase());
				if (status)
					driver.OutputStream("Country Name is validated.");
				else {
					driver.OutputStream("Country Name is not validated", false);
				}
			} catch (Exception e) {
				status = false;
				driver.OutputStream("Country name " + CountryNameValue + " is not visible for country " + CountryName
						+ ". url " + driver.getCurrentUrl(), false);
			}
		} else {
			driver.OutputStream("Warning!!!.... " + CountryName + " is not listed in the data set.");
		}
		return status;
	}

	public static void closeCountrySelector(Driver driver) {
		if (driver.isElementAvailable(countryConfirm)) {
			driver.findElement(countryConfirm).click();
		}
	}

	public static void closechat(Driver driver) {
		try {
			driver.findElement(chatClose).click();
		} catch (Exception e) {

		}
	}

	private static boolean validateFooterLink(Driver driver) {
		boolean status = true;
		status = driver.readServerError();
		String bodyText = driver.findElement(By.tagName("body")).getText().toLowerCase();
		status = status & ((!bodyText.contains("error")) || (!bodyText.contains("apache tomcat"))
				|| (!bodyText.contains("maintenance")));
		return status;
	}

	public static void closeSubscription(Driver driver) {
		closeSubscription(driver, 20);
	}
	public static void closeSubscription(Driver driver, int timeout) {
		try {
			if (driver.isElementAvailable(noSubscription, timeout))
				driver.findElement(noSubscription, timeout).click();
		} catch (Exception e) {
		}
	}

	public static void closeVideo(Driver driver) {
		if (driver.isElementAvailable(SkipVideo, 5))
			driver.click(SkipVideo);
	}

	public static boolean SmokeTest(Driver driver) {
		boolean status = true;
		// int priority = 1;
		closeSubscription(driver);
		// status = status & driver.checkDomain();
		status = status & schneiderLogoVisible(driver);
		if (!status) {
			driver.OutputStream(
					"Error!!! ..........SchneiderLogo is not visible on this page. May be links is not valid.... url: "
							+ driver.getCurrentUrl(),
					false);
		}
		if (status) {
			driver.OutputStream(driver.getCurrentUrl() + " is a webmachine page.");
		} else {
			driver.OutputStream(driver.getCurrentUrl() + " dose not contain all the webmachine page elements.", false);
			driver.takeScreenShot("PriorityOneTestFailed" + driver.getTitle().trim());
		}
		return status;
	}

	public static int getPageOfset(Driver driver) {
		int offset = 0;
		String locator = "xpath";
		driver.scrollTop();
		WebElement elem = null;
		if (driver.isElementAvailable(whiteBar, 2)) {
			elem = driver.findElement(whiteBar, 2);
			offset = elem.getLocation().getY() + elem.getSize().height;
		} else if (driver.isElementAvailable(locator, GreenHeaderXpath, 2)) {
			elem = driver.findElement(locator, GreenHeaderXpath, 2);
			offset = elem.getLocation().getY() + elem.getSize().height;
		} else if (driver.isElementAvailable(locator, ceraselContainer, 2)) {
			elem = driver.findElement(locator, ceraselContainer, 2);
			offset = elem.getLocation().getY();
		}
		// driver.OutputStream("Page ofset value is: "+offset);
		return offset;
	}
	public static boolean priority1Test(Driver driver){
		return priority1Test(driver, 1);
	}
	public static boolean priority1Test(Driver driver, int priority){
		return priority1Test(driver, nameOfCountry, priority);
	}
	public static boolean priority1Test(Driver driver,String nameOfCountry, int priority) {
		boolean status = true;
//		int priority = 1;
		closeSubscription(driver);
		// status = status & driver.checkDomain( baseURL);
		status = status & schneiderLogoVisible(driver);
		if (status) {
			status = status & countryNameValid(driver, nameOfCountry);
			status = status & cookieDesimalValidation(driver);
			// status = status & mainHeaderTest(driver);
			status = status & searchValid(driver, priority);
			status = status & socialMediaLinks(driver, priority);
			status = status & GreenHeaderTest(driver, priority);
			status = status & validateFooter(driver, priority);
		} else {
			driver.OutputStream(
					"Error!!! ..........SchneiderLogo is not visible on this page. May be links is not valid.... url: "
							+ driver.getCurrentUrl(),
					false);
		}
		if (status) {
			driver.OutputStream(driver.getCurrentUrl() + " is a webmachine page.");
		} else {
			driver.OutputStream(driver.getCurrentUrl() + " dose not contain all the webmachine page elements.", false);
			driver.takeScreenShot("PriorityOneTestFailed" + driver.getTitle().trim());
		}
		return status;
	}
}
