package se.com.WebMachine.Pages;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

@SuppressWarnings("unused")
public class DispatchContent {

	private static final String locator = "Xpath";
	private static final String mainContentXpath = "//*[@class='main']";
	private static final String topMainColEleXpath = mainContentXpath + "//*[contains(@class,'tips-section')]/li";
	private static final String rightRailXpath = mainContentXpath + "//*[@id='column-right']/ul";
	private static final String homeProductBoxXpath = mainContentXpath + "//div[@id='home-products']//ul";
	private static final String homeProductListXpath = homeProductBoxXpath + "/li";
	private static final String homeProductNextButtonXpath = "//div[@id='home-products']//div[@class='right-button active']";
	private static final String atHome = topMainColEleXpath + "[2]/div/a";
	private static final String atHomeAlt = "//*[@class ='footer-links']/li[1]/ul/li[2]/a";
	private static final String atPartner = topMainColEleXpath + "[3]/div/a";
	private static final String atPartnerAlt = "//*[@class ='footer-links']/li[1]/ul/li[3]/a";
	private static final String atSolution = topMainColEleXpath + "[1]/div/a";
	private static final String atSolutionAlt = "//*[@class ='footer-links']/li[1]/ul/li[1]/a";
	private static final String tilesBoxXpath = "//*[@class='multiple-picks']//ul[@class='tiles']";
	private static final String tilesBoxListXpath = tilesBoxXpath + "/li";
	private static final String ceraselNextXpath = "//*[@class='slider-new']/div[contains(@class, 'right-button')]";
	private static final String ceraselContainer = "//section[contains(@id,'slider') or contains(@class, 'slider')]";
	private static final String ceraselXpath = ceraselContainer + "//*[@class='slides']";
	private static final String ceraselListXpath = ceraselXpath + "/li";
	private static final String ceraselControllerXpath = ceraselContainer + "//*[@class='pagination']/li";
	private static final String contetnSlideXpath = mainContentXpath + "//*[@class='slides']";
	private static final String contentSlideListXpath = mainContentXpath + "//*[@class='slides']/li";
	private static final String contentSlideControllerListXpath = mainContentXpath + "//*[@class='pagination']/li";
	private static final String editorialXpath = mainContentXpath + "//*[@class='editorial-content']";
	private static int pageOfset = 0;

	public static boolean validateDispatchPageContents(Driver driver) {
		/*Validate components of dispatch page.
		 * read server error, validate Cerasel, Validate tiles, right rail, home product slider
		 * and editorial
		 * */
		boolean status = true;
		status = driver.readServerError() & validateCerasel(driver) & tilesValidate(driver) & rightRailValidate(driver)
				& homeProductValidate(driver) & validateSlide(driver) & validateEditorial(driver);
		return status;
	}

	private static boolean validateEditorial(Driver driver) {
		boolean status = true;
		if (driver.isElementAvailable(locator, editorialXpath, 2)) {
			driver.OutputStream("There is Editorial box in this page. Start testing editorial.");
			try {
				int noOfEditorial = driver.findElements(locator, editorialXpath).size();
				driver.OutputStream("There is " + noOfEditorial + " editorials in this page.");
				// int noOfEditorials = driver.findElements( locator,
				// editorialXpath).size();
				for (int editorialNo = 1; editorialNo <= noOfEditorial; editorialNo++)
					status = status & validateEditorialNo(driver, editorialNo);
			} catch (Exception e) {
				driver.OutputStream("Exception Triggered during testing editorial.");
			}
			if (status)
				driver.OutputStream("Editorial Test PASSED");
			else
				driver.OutputStream("Editorila Test FAILED", false);
		} else {
			driver.OutputStream("There is no Editorial box in this page. Continue testing.");
		}
		return status;
	}

	private static boolean validateEditorialNo(Driver driver, int no) {
		/*
		 * validate header component validate two collums
		 */
		boolean status = false;
		try {
			status = driver.findElement(locator, editorialXpath + "[" + no + "]//h1", 1).getText().length() > 0;
			status = status
					& driver.findElements(locator, editorialXpath + "[" + no + "]//*[@class='column']").size() == 2;
			if (status)
				driver.OutputStream("Editorial " + no + " test PASSED");
			else
				driver.OutputStream("Editorial " + no + " test FAILED");
		} catch (Exception e) {
			driver.OutputStream(
					"Exception Triggered diring testing editorial " + no + " testing. Error Massage: " + e.getMessage(),
					false);
		}
		return status;
	}

	private static boolean validateSlide(Driver driver) {
		boolean status = true;
		int noOfSlides, noOfControllers;
		if (driver.isElementAvailable(locator, contentSlideListXpath, 2)) {
			noOfSlides = driver.findElements(locator, contentSlideListXpath).size();
			noOfControllers = driver.findElements(locator, contentSlideControllerListXpath).size();
			driver.OutputStream("There is " + noOfSlides + " slides in this page. Continue with testing.");
			status = (noOfSlides > 0) && (noOfSlides == noOfControllers);
			for (int slideNo = 1; slideNo <= noOfSlides; slideNo++)
				status = status & validateContentSlideNo(driver, slideNo);
		}
		return status;
	}

	private static boolean validateContentSlideNo(Driver driver, int slideNo) {
		/*
		 * Check for 4 conditions title contains text peragraph contains text
		 * one link is vissible one image is vissible
		 */
		boolean status = false;
		driver.OutputStream("Start Testing slide no " + slideNo);
		try {
			if (driver.isElementAvailable(locator, contentSlideControllerListXpath + "[" + slideNo + "]", 2)) {
				driver.click(locator, contentSlideControllerListXpath + "[" + slideNo + "]");
				status = driver.findElement(locator, contentSlideListXpath + "[" + slideNo + "]//*[@class='title']", 4)
						.getText().length() > 0
						& driver.findElement(locator, contentSlideListXpath + "[" + slideNo + "]//p", 1).getText()
								.length() > 0
						& driver.isElementAvailable(locator, contentSlideListXpath + "[" + slideNo + "]//a", 1);
				// & driver.isElementAvailable( locator,
				// contentSlideListXpath+"["+slideNo+"]//img", 1);
				if (status)
					driver.OutputStream("PASSED");
				else
					driver.OutputStream("Slide no " + slideNo + " test FAILED", false);
			}
		} catch (Exception e) {
			driver.OutputStream("Exception Triggered.");
		}
		return status;
	}

	public static boolean validateCerasel(Driver driver) {
		/*
		 * Steps check how many cerasel is there. check no of cerasel and dots
		 * in the bottom of the is same check all cerasel contains image.
		 */
		driver.scrollTop();
		boolean status = true;
		status = driver.isElementAvailable(locator, ceraselXpath);
		if (status) {
			driver.OutputStream("Cerasol Visible in the page. Going to Start Testing Cerasel");
			int noOfCerasel = driver.findElements(locator, ceraselListXpath).size();
			int noOfController = driver.findElements(locator, ceraselControllerXpath).size();
			if (noOfCerasel > 1)
				status = noOfController == noOfCerasel & noOfCerasel > 0;
			if (status) {
				driver.OutputStream("Cerasel number matches. Continue with test.");
				status = validateCeraselElements(driver, noOfCerasel);
			} else {
				driver.OutputStream("Number of Cerasel Does not match with no of dots in the bottom of the cerasel.",
						false);
			}
		} else {
			driver.OutputStream("Cerasel is not visible in this page. " + driver.getCurrentUrl(), false);
		}
		if (status)
			driver.OutputStream("Cerasel Test PASSED.");
		else
			driver.OutputStream("Cerasel Test FAILED", false);
		return status;
	}

	private static boolean validateCeraselElements(Driver driver, int totalNo) {
		/*
		 * validate cerasel contains image.
		 */
		boolean status = true;
		if (totalNo == 1) {
			status = status & driver.isElementAvailable(locator, ceraselListXpath + "[" + totalNo + "]//img", 2);
			if (status) {
				driver.OutputStream("Cerasel No " + totalNo + " is validated.");
			} else {
				driver.OutputStream(
						"Cerasel No " + totalNo + " is not valideted. Something is missing in that cerasel.", false);
			}
			return status;
		}
		for (int ceraselNo = 1; ceraselNo <= totalNo; ceraselNo++) {
			// if(driver.isElementAvailable( locator,
			// ceraselControllerXpath+"["+ceraselNo+"]", 2))
			driver.click(locator, ceraselControllerXpath + "[" + ceraselNo + "]");
			status = status & driver.isElementAvailable(locator, ceraselListXpath + "[" + ceraselNo + "]//img", 2);
			if (status) {
				driver.OutputStream("Cerasel No " + ceraselNo + " is validated.");
			} else {
				driver.OutputStream(
						"Cerasel No " + ceraselNo + " is not valideted. Something is missing in that cerasel.", false);
			}
		}
		return status;
	}

	private static boolean tilesValidate(Driver driver) {
		boolean status = true;
		if (driver.isElementAvailable(locator, tilesBoxXpath, 2)) {
			driver.OutputStream("Tiles are availabe in this page. Continue with testing tiles components");
			try {
				int noOfTiles = driver.findElements(locator, tilesBoxListXpath).size();
				status = noOfTiles > 0;
				if (status) {
					driver.OutputStream("Page contains " + noOfTiles + " tiles.");
					for (int tileNo = 1; tileNo <= noOfTiles; tileNo++) {
						checkTileComponent(driver, tileNo);
					}
				} else
					driver.OutputStream("Page Dont have any tiles in the tips box. " + driver.getCurrentUrl(), false);
			} catch (Exception e) {
				driver.OutputStream("Expection Triggered.");
			}
			if (status)
				driver.OutputStream("Tiles Test PASSED");
			else
				driver.OutputStream("Tile Test FAILED", false);
		} else {
			driver.OutputStream("Tiles box is not visible on this page. Skip Test");
		}
		return status;
	}

	private static boolean checkTileComponent(Driver driver, int no) {
		boolean status = true;
		String tileXpath = tilesBoxListXpath + "[" + no + "]";
		status = driver.isElementAvailable(locator, tileXpath + "//a", 2) // link
				&& driver.isElementAvailable(locator, tileXpath + "//span[@class='subtitle']", 2) // text
				&& driver.isElementAvailable(locator, tileXpath + "//span/img", 2); // image
		if (status)
			driver.OutputStream("All the elements of Tile no " + no + " is available in the page.");
		else {
			driver.OutputStream(
					"Not all the elements are available for Tile no " + no + " on this page." + driver.getCurrentUrl(),
					false);
		}
		return true;
	}

	public static boolean homeProductValidate(Driver driver) {
		/*
		 * Bug!!!!! ---- cant test the last product.
		 **/
		boolean status = true;
		boolean statusLocal;
		pageOfset = HomePage.getPageOfset(driver);
		if (driver.isElementAvailable(locator, homeProductBoxXpath, 2)) {
			driver.OutputStream("Home Products are available in the page. Start Testing.....");
			int noOfProducts = driver.findElements(locator, homeProductListXpath).size();
			driver.OutputStream("There is " + noOfProducts + " home products in this page.");
			status = noOfProducts > 0;
			driver.scrollToElement(driver.findElement(locator, homeProductListXpath, 5), pageOfset);
			for (int productNo = 1; productNo < noOfProducts; productNo++) {
				if (!(driver.isElementAvailable(locator, homeProductListXpath + "[" + productNo + "]//a", 2)
						&& driver.isElementAvailable(locator, homeProductListXpath + "[" + productNo + "]//img", 2)))
					driver.click(locator, homeProductNextButtonXpath);
				statusLocal = driver.isElementAvailable(locator, homeProductListXpath + "[" + productNo + "]//a", 4); // link
				// & driver.isElementAvailable( locator,
				// homeProductListXpath+"["+productNo+"]//img", 4); //Image
				if (statusLocal)
					driver.OutputStream("Home Product no " + productNo + " contains link and image.");
				else
					driver.OutputStream("Home Product no " + productNo + " does not contains link or image.");
				status = status & statusLocal;
			}
		}
		if (status)
			driver.OutputStream("Home Product Test PASSED.");
		else
			driver.OutputStream("Home Product test FAILED", false);
		return status;
	}

	private static boolean rightRailValidate(Driver driver) {
		boolean status = true;
		/*
		 * Steps: 1. check right rail is available 2. check position of right
		 * rail 3. check multiple blocks are available 4. check links are valid
		 * using java-http
		 * 
		 * there might be multiple right rails.......
		 */
		driver.OutputStream("Start right rail test.");
		if (driver.isElementAvailable(locator, rightRailXpath)) {
			driver.OutputStream("Right Rail is available in the page. Continue testing");
			int noOfRightRail = driver.findElements(locator, rightRailXpath).size();
			for (int railNo = 1; railNo <= noOfRightRail; railNo++) {
				driver.OutputStream("---------------------------------------------------------\n" + "Starting " + railNo
						+ " no rail test.");
				status = status & subRailValidate(driver, railNo);
			}
			if (status)
				driver.OutputStream("RightRail PASSED.");
			else
				driver.OutputStream("Right Rail FAILED");
		} else {
			driver.OutputStream("There is no right rail in this page. Skip testing.");
		}
		return status;
	}

	private static boolean subRailValidate(Driver driver, int railNo) {
		boolean status = true;
		boolean statusLocal;
		String rightRailBoxListXpath = rightRailXpath + "[" + railNo + "]/li[not(@class='empty')]";
		int noOfSubRail = driver.findElements(locator, rightRailBoxListXpath).size();
		for (int subRailNo = 1; subRailNo <= noOfSubRail; subRailNo++) {
			String subPath = rightRailBoxListXpath + "[" + subRailNo + "]/div";
			if (driver.findElement(locator, subPath, 15).getAttribute("class").contains("links-only"))
				statusLocal = driver.isElementAvailable(locator, subPath + "//a");
			else
				statusLocal = driver.isElementAvailable(locator, subPath + "//a") // link
						&& driver.isElementAvailable(locator, subPath + "//h3") // title
						&& driver.isElementAvailable(locator, subPath + "//span"); // details
			if (statusLocal)
				driver.OutputStream("Sub Rail no " + subRailNo + " test complete. PASSED");
			else
				driver.OutputStream("Sub Rail no " + subRailNo + " test complete.FAILED");
			status = status & statusLocal;
		}
		return status;
	}

	private static boolean topMainColValidate(Driver driver) {
		boolean status = true;
		status = driver.isElementAvailable(locator, topMainColEleXpath);
		if (status) {
			int noOfElement = driver.findElements(locator, topMainColEleXpath).size();
			for (int no = 1; no <= noOfElement; no++)
				validateTopMenuElement(driver, no);
		}
		return status;
	}

	private static boolean validateTopMenuElement(Driver driver, int number) {
		boolean status = true;
		status = status & driver.isElementAvailable(locator, topMainColEleXpath + "[" + number + "]/div/div/img") // picture
				&& driver.isElementAvailable(locator, topMainColEleXpath + "[" + number + "]/div/h3") // header
				&& driver.isElementAvailable(locator, topMainColEleXpath + "[" + number + "]/div/span") // details
				&& driver.isElementAvailable(locator, topMainColEleXpath + "[" + number + "]/div/a"); // link
		if (status)
			driver.OutputStream(
					"All the elements of no " + number + " element of 1st tips section from the top is available.");
		else {
			driver.OutputStream(
					"All the elements of no " + number + " element of 1st tips section from the top is not available.",
					false);
			driver.takeScreenShot("TopTipsElementMissing");
			return status;
		}
		if (driver.isElementAvailable(locator, topMainColEleXpath + "[" + number + "]//img")) {
			Dimension picSize = driver.findElement(locator, topMainColEleXpath + "[" + number + "]//img", 15).getSize();
			status = status & picSize.height > 0 & picSize.width > 0;
		}
		if (status)
			driver.OutputStream("Top Menu passed.");
		return status;
	}

	public static boolean gotoHome(Driver driver) {
		boolean status = false;
		status = driver.isElementAvailable(locator, atHome, 20);
		if (status) {
			WebElement home = driver.findElement("xpath", atHome, 5);
			if (home.getAttribute("href").contains("home")) {
				driver.click(home);
				driver.waitToLoad(20, "Schneider");
				status = true;
			} else {
				home = driver.findElement("xpath", atHomeAlt, 5);
				if (home.getAttribute("href").contains("home")) {
					driver.click(home);
					driver.waitToLoad(20, "Schneider");
					status = true;
				} else {
					driver.OutputStream(
							"Link to 'AtHome' is not available is this page. URL: " + driver.getCurrentUrl(), false);
					driver.takeScreenShot("AtHomeNotAvailable");
					status = false;
				}
			}
		}
		return status;
	}

	public static boolean gotoPartner(Driver driver) {
		boolean status = true;
		status = driver.isElementAvailable(locator, atPartner, 20);
		if (status) {
			WebElement partner = driver.findElement("xpath", atPartner, 5);
			if (partner.getAttribute("href").contains("partners")) {
				driver.click(partner);
				driver.waitToLoad(20, "Schneider");
				status = true;
			} else {
				partner = driver.findElement("xpath", atPartnerAlt, 5);
				if (partner.getAttribute("href").contains("partners")) {
					driver.click(partner);
					driver.waitToLoad(20, "Schneider");
					status = true;
				} else {
					driver.OutputStream(
							"Link to 'Partners' is not available in this page. URL: " + driver.getCurrentUrl(), false);
					driver.takeScreenShot("ParnerNotAvailabe");
					status = false;
				}
			}
		}
		return status;
	}

	public static boolean gotoWork(Driver driver) {
		boolean status = true;
		status = driver.isElementAvailable(locator, atSolution, 15);
		if (status) {
			WebElement work = driver.findElement("Xpath", atSolution, 5);
			if (work.getAttribute("href").contains("work")) {
				driver.click(work);
				driver.waitToLoad(20, "Schneider");
				status = true;
			} else {
				work = driver.findElement("Xpath", atSolutionAlt, 5);
				if (work.getAttribute("href").contains("work")) {
					driver.click(work);
					driver.waitToLoad(20, "Schneider");
					status = true;
				} else {
					driver.OutputStream("Link to 'Work' is not available in this page. URL: " + driver.getCurrentUrl(),
							false);
					driver.takeScreenShot("AtWorkNotAvailable");
					status = false;
				}
			}
		}
		return status;
	}

}
