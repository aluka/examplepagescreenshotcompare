package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class SinglePageHeader {
	private static String headerSectionXpath = "//ul[@class='tier-one']/li";
	private static By countryConfirm = By.className("confirm-location-btn");
	// private static String baseURL = "US/EN";
	
	public static boolean SinglePageHeaderTest(Driver driver, int priority){
		boolean status = true;
		String locator = "xpath";
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		String tire21Xpath, tire22Xpath, tire3Xpath, headerName, linkName;
//		WebDriverWait wait = new WebDriverWait( 20);
//		WebElement elementClickable;
		if(driver.isElementAvailable(countryConfirm)){
			driver.findElement(countryConfirm).click();
		}
		List<WebElement> headerSection = driver.findElements( locator, headerSectionXpath);
		/*-------------------Expected to have 6 item in the header menu ---------------------*/
		status = status && headerSection.size()==5;
		if(status){
			driver.OutputStream("There is 5 items in the header menu. As expected.");
		}else{
			driver.OutputStream("There is more or less then 5 items in the header menu. Not expected."+driver.getCurrentUrl(), false);
			driver.takeScreenShot( "HeaderNoOfMenu");
		}
		for(int headerNo=0; headerNo < headerSection.size() && priority>1;headerNo++){
			headerName = driver.findElement( locator, headerSectionXpath+"["+(headerNo+1)+"]/a", 15).getText();
			driver.click( headerSection.get(headerNo));
			driver.wait(200);
			driver.switchTab();
			driver.waitToLoad();
			driver.OutputStream(headerName+ " is selected from green header");
			driver.OutputStream("--------------------------------------------------------------");
			if(!driver.getCurrentUrl().equalsIgnoreCase(url)){
				status = status & HomePage.SmokeTest(driver);
			}else{
				driver.OutputStream(headerName+" link did not go to the destination.", false);
			}
			driver.closeMultipleTabs();
			driver.navigate().to(url);
			driver.wait(100);
			driver.waitToLoad( title);
			driver.wait(100);
			driver.waitToLoad();
			headerSection = driver.findElements( locator, headerSectionXpath);		
		}
		driver.wait(100);
		return status;
	}

}
