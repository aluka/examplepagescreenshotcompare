package se.com.WebMachine.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.com.WebMachine.Pages.HomePage;
import se.com.WebMachine.Tools.Driver;

public class HeaderNew {

	private static final String headerSectionXpath = "//ul[contains(@class,'tier-one')]/li";
	private static final String locator = "xpath";
	private static int menuNo = 0;
	private static int subMenuNo = 0;
	private static int menuLinkNo = 0;
	private static String url = "";
	private static String title = "";
	private static String tire21Xpath = "", tire22Xpath = "", tire3Xpath = "", menuName = "", subMenuName = "",
			headerLinkName = " ", headerLinkURL = "";

	public static boolean greenHeaderTest(Driver driver, int priority) {
		driver.OutputStream("Start Testing Green Heaeder.");
		boolean status = true;
		url = driver.getCurrentUrl();
		title = driver.getTitle();
		driver.refresh();
		driver.waitToLoad("Schneider");
		HomePage.getPageDomain(driver);
		/*--------------------    Start validation ------------------------*/
		if (driver.isElementAvailable("xpath", headerSectionXpath, 3)) {
			if (validateLocation(driver))
				status = status & testMenuItem(driver, priority);
		} else {
			driver.OutputStream("There is no Greenheader in this page. Skip testing header.");
			return true;
		}
		/*--------------------    End validation ------------------------*/
		if (status)
			driver.OutputStream("Header test PASSED");
		else
			driver.OutputStream("Header test FAILED", false);
		driver.navigate().to(url);
		driver.waitToLoad(title);
		return status;
	}

	private static boolean testMenuItem(Driver driver, int priority) {
		boolean status = true;
		boolean statusLocal = false;
		int headerMenuSize = driver.findElements(locator, headerSectionXpath).size();
		status = headerMenuSize == 5;
		driver.OutputStream("There is " + headerMenuSize + " menu in the header.");
		for (menuNo = 1; menuNo <= headerMenuSize && priority > 1; menuNo++) {
			menuName = driver.findElement(locator, headerSectionXpath + "[" + menuNo + "]/a", 15).getText();
			driver.MoveTo(locator, headerSectionXpath + "[" + menuNo + "]");
			driver.OutputStream("Clicked on menu name " + menuName);
			driver.OutputStream("--------------------------------------------------------------------------------");
			// testSubMenu
			if (priority > 1) {
				statusLocal = testSubManu(driver, priority);
				status = status & statusLocal;
				driver.OutputStream("Header manu name " + menuName + " test end. Going back to Dispatch Page.");
			}
			if (statusLocal)
				driver.OutputStream("Header menu " + menuName + ", Submenu name " + subMenuName + " test PASSED");
			else
				driver.OutputStream("Header menu " + menuName + " test FAILED", false);
		}
		return status;
	}

	private static boolean testSubManu(Driver driver, int priority) {
		boolean status = true;
		boolean statusLocal = false;
		tire21Xpath = headerSectionXpath + "[" + menuNo + "]/ul/div/li";
		int tire21number = driver.findElements(locator, tire21Xpath).size();
		// This is two dimention submenu
		for (int tire21no = 1; tire21no < tire21number; tire21no++) {
			tire22Xpath = tire21Xpath + "[" + tire21no + "]/a";
			int tire2number = driver.findElements(locator, tire22Xpath).size();
			status = tire2number > 0;
			for (subMenuNo = 1; subMenuNo <= tire2number; subMenuNo++) {
				subMenuName = driver.findElement(locator, tire22Xpath + "[" + subMenuNo + "]", 15).getText();
				driver.OutputStream("Goint to SubMenu Name: " + subMenuName);
				statusLocal = subMenuName.length() > 0;
				// test SubMenu Links
				driver.OutputStream("Start Testing links under submenu " + subMenuName);
				tire3Xpath = tire21Xpath + "[" + tire21no + "]/ul[" + subMenuNo + "]/li";
				statusLocal = testMenuLink(driver, priority);
				status = status & statusLocal;
				// Validation done
				driver.click(locator, headerSectionXpath + "[" + menuNo + "]/a");
				driver.OutputStream("Clicked on menu name " + menuName);
				driver.OutputStream("--------------------------------------------------------------------------------");
			}
			if (statusLocal)
				driver.OutputStream("Header menu " + menuName + ", Submenu name " + subMenuName + " test PASSED");
			else
				driver.OutputStream("Header menu " + menuName + ", Submenu name " + subMenuName + " test FAILED",
						false);
		}
		return status;
	}

	private static boolean testMenuLink(Driver driver, int priority) {
		boolean status = true;
		boolean statusLocal = false;
		WebDriverWait wait = new WebDriverWait(driver, 20);
		int noOfMenuLinks = driver.findElements(locator, tire3Xpath).size();
		status = noOfMenuLinks > 0;
		for (menuLinkNo = 1; menuLinkNo < noOfMenuLinks; menuLinkNo++) {
			WebElement linkElement = driver.findElement(locator, tire3Xpath + "[" + menuLinkNo + "]/a", 15);
			// waiting for the links to be available and try to read link again.
			if (linkElement == null) {
				driver.navigate().refresh();
				driver.waitToLoad("Schneider");
				driver.wait(100);
				driver.waitToLoad();
				driver.wait(100);
				driver.MoveTo(locator, headerSectionXpath + "[" + menuNo + "]");
				driver.wait(200);
				linkElement = driver.findElement(locator, tire3Xpath + "[" + menuLinkNo + "]/a", 15);
				if (linkElement == null) {
					driver.wait(100);
					linkElement = driver.findElement(locator, tire3Xpath + "[" + menuLinkNo + "]/a", 15);
				}
				if (linkElement == null) {
					driver.OutputStream("Link element no " + menuLinkNo + " of Menu Name " + menuName
							+ ", Sub Menu Name " + subMenuName + " is not accessable", false);
					status = false;
					continue;
				}
			}
			headerLinkName = linkElement.getText();
			headerLinkURL = linkElement.getAttribute("href");
			statusLocal = headerLinkName.length() > 0 && headerLinkURL.contains("http://");
			if (priority > 2) {
				driver.OutputStream("Going to the header link name: " + headerLinkName);
				driver.click(linkElement);
				driver.wait(100);
				driver.switchTab();
				driver.waitToLoad(20, "Schneider");
				if (!driver.getCurrentUrl().equals(url)) {
					// Start testing Destination Page
					driver.OutputStream("In " + driver.getTitle() + " page. Start testing the page.");
					driver.OutputStream(
							"-----------------------------------------------------------------------------------------------------");
					statusLocal = statusLocal && HomePage.priority1Test(driver);
					driver.OutputStream("Test End going back to the page."
							+ "\n ---------------------------------------------------------------------------------------------------------------------------");
					driver.closeMultipleTabs();
				} else {
					driver.OutputStream(headerLinkName + " link did not go to Destination page.", false);
				}
				driver.navigate().to(url);
				driver.wait(100);
				driver.waitToLoad(title);
				driver.wait(100);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(headerSectionXpath)));
				driver.click(locator, headerSectionXpath + "[" + menuNo + "]");
			}
			if (statusLocal) {
				driver.OutputStream("Header link " + headerLinkName + " URL: " + headerLinkURL + " test PASSED");
			} else {
				status = status & false;
				driver.OutputStream("Header link test Failed. Menu Name " + menuName + ", Sub Menu Name " + subMenuName
						+ ", link no" + menuLinkNo + ".", false);
			}
			status = status & statusLocal;
		}
		return status;
	}

	public static boolean validateLocation(Driver driver) {
		boolean status = true;
		String GreenHeaderXpath = "//*[@class='main-menu']";
		Dimension size = driver.manage().window().getSize();
		if (size.width > 980) {
			try {
				if (driver.isElementAvailable("Xpath", GreenHeaderXpath)) {
					Point location = driver.findElement("Xpath", GreenHeaderXpath, 15).getLocation();
					status = status & location.x > 0 & location.y > 0;
					if (status)
						driver.OutputStream("Header is positioned properly in the page.");
					else
						driver.OutputStream("Header is not properly positioned in the page.");
				} else {
					driver.OutputStream("Main Header is not Visible in the page. Could not varified the location");
				}
			} catch (NoSuchElementException e) {
				driver.OutputStream("Unknown error. " + e, false);
				status = false;
			}
		} else {
			driver.OutputStream("This page is now in mobile view. Increase the browser size to test header menu.");
			return false;
		}
		return status;
	}
}
