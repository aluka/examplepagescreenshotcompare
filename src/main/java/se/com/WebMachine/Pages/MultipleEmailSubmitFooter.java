package se.com.WebMachine.Pages;

import org.openqa.selenium.By;

import se.com.WebMachine.Tools.Driver;

public class MultipleEmailSubmitFooter {
	private static String emailSubcribeXpath = "//*[@class='footer-container']//input[@id='Email']";
	private static String submitButtonXpath = "//*[@class='footer-container']//input[@value='OK']";
	private static String SubmissionConfirmationXpath = "//*[@id='cboxContent']//*[@class='newsletter-confirm']";

	public static boolean validateWordWrap(Driver driver) {
		try {
			if (driver.findElement(By.tagName("body")).getText().contains("Word Wrap")) {
				driver.OutputStream("Page Contains body text 'Word Wrap' in the page. Test FAILED.");
				return false;
			} else {
				driver.OutputStream("Page does not caontain any text contain 'Word Wrap' in the page. Test PASSED.");
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean validateDubbleSubmissionEmailFooter(Driver driver) {
		boolean status = true;
		boolean wrongEmailSubmit = !submitEmail(driver, "abcdtd");
		boolean rightEmailSubmit = submitEmail(driver, "hoqua@mail.com");
		status = wrongEmailSubmit & rightEmailSubmit;
		return status;
	}

	private static boolean submitEmail(Driver driver, String email) {
		boolean status = false;
		if (driver.isElementVissible("Xpath", emailSubcribeXpath, 1)) {
			driver.findElement("Xpath", emailSubcribeXpath, 2).sendKeys(email);
			driver.click("Xpath", submitButtonXpath);
			if (driver.isElementVissible("Xpath", SubmissionConfirmationXpath, 10)) {
				String text = driver.findElement("Xpath", SubmissionConfirmationXpath, 5).getText();
				if (text.length() > 0 & text.contains("Thank you")) {
					status = true;
					driver.OutputStream("Email submitted successfully.");
					driver.navigate().refresh();
				} else
					driver.OutputStream("Email submit was not successfull.");
			} else
				driver.OutputStream("Email submit was not successfull.");
		} else
			driver.OutputStream("Cound not find email box in the footer.");
		return status;
	}

}
