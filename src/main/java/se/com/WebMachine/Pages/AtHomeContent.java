package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class AtHomeContent {
	
	private static final String locator = "Xpath"; 
	private static final String mainContentSecXpath="//*[@class='main']";
	private static final String picsXpath = mainContentSecXpath+"/div[@class='picks']";
	private static final String tilesXpath = picsXpath+"//ul[@class='tiles']";
	private static final String tipsSectionLiXpath = "//*[@class='tips-section']/li";
	public static final String homeProductBoxXpath = "//div[@id='home-products']//ul";
	public static final String homeProductListXpath = homeProductBoxXpath+"/li";
	public static final String homeProductNextButtonXpath = "//div[@id='home-products']//div[@class='right-button active']";

	public static boolean testAtHome(Driver driver){
		HomePage.closeSubscription(driver, 5);
		String baseURL = driver.getCurrentUrl();
		boolean status = true;
		if(DispatchContent.gotoHome(driver)){
			driver.OutputStream("In 'At-Home' page. Continue with test.");
			if( AtHomeContent.validateAtHomePageContent(driver)){
				driver.OutputStream("At-Home test Passed");
				status = true;
			}else{
				driver.OutputStream("At-Home test Failed", false);
				status = false;
			}
		}else{
			driver.OutputStream("Cant navigate to At-Home page.");
			return true;
		}
		driver.navigate().to(baseURL);
		driver.wait(100);
		driver.waitToLoad();
		return status;
	}
	
	public static boolean validateAtHomePageContent(Driver driver){
		boolean status = true;
		HomePage.closeSubscription(driver, 2);
		status = driver.readServerError();
		status = status & checkTips(driver) ;
		status = status & DispatchContent.validateCerasel(driver);
		status = status & AtWork.validateTiles(driver) ;
		status = status & DispatchContent.homeProductValidate(driver);
//		status = status & HomePage.priority1Test();
		return status;
	}
	
/*	private static boolean homeProductValidate(Driver driver){
		boolean status = true;
		boolean statusLocal;
		if(driver.isElementAvailable( locator, homeProductBoxXpath)){
			try{	
				driver.OutputStream("Home Products are available in the page. Start Testing.....");
				int noOfProducts = driver.findElements( locator, homeProductListXpath).size();
				status = noOfProducts >0;
				for (int productNo = 1; productNo <= noOfProducts; productNo++){
					if(!(driver.isElementAvailable( locator, homeProductListXpath+"["+productNo+"]//a", 2)))
						driver.click( locator, homeProductNextButtonXpath);
					statusLocal = driver.isElementAvailable( locator, homeProductListXpath+"["+productNo+"]//a", 2); 	//link
//							&& driver.isElementAvailable( locator, homeProductListXpath+"["+productNo+"]//img", 2);		//Image
					if(statusLocal){
						driver.OutputStream("Home Product no "+productNo+" contains link and image.");
					}
					status = status & statusLocal;
				}
			}catch(Exception e){
				driver.OutputStream("Exception triggerd from homeProduct Validation.");
				status = false;
				}
		}
		if(status){
			driver.OutputStream("Home Product Test Passed. -----------------------------------------------------");
		}
		return status;
	}
*/	
	public static boolean validateTiles(Driver driver){
		boolean status = true; 
		if(driver.isElementAvailable( locator, tilesXpath, 2)){
			try{
				int noOfTiles = driver.findElements( locator, tilesXpath).size();
				driver.OutputStream("There is "+noOfTiles+" Tiles in this page. Continue testing.");
				for(int tilesNo = 1; tilesNo <= noOfTiles; tilesNo++){
					status = status & validateTileNo(driver, tilesNo);
					if(status)	driver.OutputStream("Tile no "+tilesNo+" test complete. content test PASSED");
					else	driver.OutputStream("Tile no "+tilesNo+" test complete. content test FAILED", false);
				}
			}catch(Exception e){
				driver.OutputStream("Exception triggered from ValidateTiles. "+e, false);
				status = false;
			}
		}else{
			driver.OutputStream("This page dont have any Tiles. Contiue with next Test.");
		}
		return status;
	}
	
	private static boolean validateTileNo(Driver driver, int tileNo){
		boolean status = true;
		boolean statusLocal;
		String tileListXpath = picsXpath+"["+tileNo+"]//ul[@class='tiles']/li";
		List<WebElement> tileListElements = driver.findElements( locator, tileListXpath);
		int noOfItems = tileListElements.size();
		driver.OutputStream("There is "+noOfItems+" item under tile no "+tileNo+". Continue Testing.");
		status = noOfItems>0;
		for(int itemNo=1; itemNo<=noOfItems; itemNo++){
				String className = tileListElements.get(itemNo-1).getAttribute("class");
				if(className.equalsIgnoreCase("tall-left larger")){
					if(driver.isElementAvailable( locator, tileListXpath+"["+itemNo+"]/div[@class='picks-welcome' or contains(@class, 'video')]", 2)){
						driver.OutputStream("Item no "+itemNo+" contains script. Not testable. PASSED" );
						statusLocal = true;
					}else{
						statusLocal = testTallLeftTilesContent(driver, itemNo, tileListXpath);
					if(statusLocal)	
						driver.OutputStream("Tile no "+tileNo+" ItemNo "+itemNo+" is a pic less tile. Content_Test Test PASSED" );
					else
							driver.OutputStream("Tile no "+tileNo+" ItemNo "+itemNo+" does not contain defined contents. Content_Test Test FAILED", false );						
					}
					status = status && statusLocal;
				}
				else if(className.equalsIgnoreCase("larger")){
					if(driver.isElementAvailable( locator, tileListXpath+"["+itemNo+"]/div[@class='picks-welcome' or contains(@class, 'video')]", 2)){
						driver.OutputStream("Item no "+itemNo+" contains script. Not testable. PASSED" );
						statusLocal = true;
					}else {
						statusLocal = testLargerTilesContent(driver, itemNo, tileListXpath);
						if(statusLocal)	
							driver.OutputStream("Tile no "+tileNo+" ItemNo "+itemNo+" is a Tile with pics. Content_Test Test PASSED" );
						else 
							driver.OutputStream("Tile no "+tileNo+" ItemNo "+itemNo+" is a Tile with pics. Content_Test Test FAILED", false );
						}
					status = status && statusLocal;
				}else if(className.length()==0) {
					if(driver.isElementAvailable( "Xpath", tileListXpath+"["+itemNo+"]/div[@class='picks-sign-up']", 1))
						statusLocal = testTextTile(driver, itemNo, tileListXpath);
					else if(driver.isElementAvailable( locator, tileListXpath+"["+itemNo+"]/div[@class='picks-welcome' or contains(@class, 'video')]t", 1)){
						driver.OutputStream("Item no "+itemNo+" contains script. Not testable. PASSED" );
						statusLocal = true;
					}
					else{
						statusLocal = testLargerTilesContent(driver, itemNo, tileListXpath);
					if(statusLocal)	
						driver.OutputStream("Tile no "+tileNo+" ItemNo "+itemNo+" is a Tile with pics. Content_Test Test PASSED" );
					else
						driver.OutputStream("Tile no "+tileNo+" ItemNo "+itemNo+" is a Tile with pics. Content_Test Test FAILED", false );						
					}
					status = status && statusLocal;
				}				
		}
		return status;
	}
	private static boolean testTextTile(Driver driver, int itemNo, String xpath){
		boolean status = false;
		try{
			status = driver.isElementAvailable( locator, xpath+"["+itemNo+"]//img", 2) 
					&& driver.findElement( locator, xpath+"["+itemNo+"]", 2).getText().length()>0;
		}catch(Exception e){
		}
		return status;	
	}
	private static boolean testLargerTilesContent(Driver driver, int itemNo, String xpath){
		boolean status = false;
		try{
			status = driver.isElementAvailable( locator, xpath+"["+itemNo+"]//img", 2) 
					&& driver.findElement( locator, xpath+"["+itemNo+"]//em", 2).getText().length()>0
					&& driver.isElementAvailable( locator, xpath+"["+itemNo+"]//a", 2);
		}catch(Exception e){
		}
		return status;
	}
	
	private static boolean testTallLeftTilesContent(Driver driver, int itemNo, String xpath){
		boolean status = false;
		try{
			status = driver.findElement( locator, xpath+"["+itemNo+"]//p", 2).getText().length()>0
					&& driver.isElementAvailable( locator, xpath+"["+itemNo+"]//a", 2)
//					&& driver.findElement( locator, xpath+"["+itemNo+"]//h1", 2).getText().length()>0 
					;
		}catch(Exception e){
			
		}
		return status;
	}
	
	private static boolean checkTips(Driver driver){
		boolean status = true;
		if(driver.isElementAvailable( locator, tipsSectionLiXpath, 5))
		{
			driver.OutputStream("Start testing tips in the page.");
			int noOfTips = driver.findElements( locator, tipsSectionLiXpath).size();
			driver.OutputStream("There is "+noOfTips+" tips in this page. Continue Testing");
			for (int tipNo= 1; tipNo <= noOfTips; tipNo++){
				status = validateTipsContent(driver, tipNo);
			}
		}else{
			driver.OutputStream("There is no Tips in this page. Continue with test.");
		}
		return status;
	}
	
	private static boolean validateTipsContent(Driver driver, int tipNo){
		String tipXpath = tipsSectionLiXpath+"["+tipNo+"]";
		boolean status = false;
		try{
			status = driver.isElementAvailable( locator, tipXpath+"/a", 2)
				&& driver.isElementAvailable( locator, tipXpath+"/a/span", 2)
				&& driver.findElement( locator, tipXpath+"/a//h3", 2).getText().length()>0
				&& driver.isElementAvailable( locator, tipXpath+"/a//img", 2);
		}catch(Exception e){
			
		}
		if(status) 
			driver.OutputStream("All the elements for tip section no "+ tipNo +" is visible.");
		else
			driver.OutputStream("Not all the elements for tip section no "+ tipNo+" is visible.", false);
		return status;
	}
}
