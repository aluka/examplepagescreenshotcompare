package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class GoToTop {

	private static String locator = "xpath";
	private static final String goToTopXpath = "//a[@title='Go back up']";
	private static int offset = 0;

	public static boolean goToTopValidation(Driver driver) {
		boolean status = true;
		if (driver.isElementAvailable(locator, goToTopXpath, 2)) {
			int noOfButtons = driver.findElements(locator, goToTopXpath).size();
			driver.OutputStream(
					"This page has " + noOfButtons + " 'Go Back Up' buttons. Start Testing the functionality.");
			offset = HomePage.getPageOfset(driver);
			for (int buttonNo = 1; buttonNo <= noOfButtons; buttonNo++) {
				status = status & testGoBackButtonNo(driver, buttonNo);
			}
			if (status)
				driver.OutputStream("Go To Top test PASSED.");
			else
				driver.OutputStream("Go to top test FAILED.");
		} else {
			driver.OutputStream("There is no 'Go Back Up' button on this page. Skip this test.");
		}
		return status;
	}

	private static boolean testGoBackButtonNo(Driver driver, int buttonNo) {
		boolean status = false;
		List<WebElement> buttons = driver.findElements(locator, goToTopXpath);
		if (buttons.size() >= buttonNo) {
			driver.scrollToElement(buttons.get(buttonNo - 1), offset);
			int pageLocation = driver.getPageLocation();
			driver.click(buttons.get(buttonNo - 1));
			driver.wait(200);
			int afterLocation = driver.getPageLocation();
			driver.OutputStream(
					"Location before clicking is " + pageLocation + " and location after click is " + afterLocation);
			status = validateScroll(0, afterLocation, 1);
			if (status)
				driver.OutputStream("Button no " + buttonNo + " test PASSED");
			else
				driver.OutputStream("Button no " + buttonNo + " test FAILED");
		}
		return status;
	}

	private static boolean validateScroll(int expected, int accual, int adjustment) {
		int difference = Math.abs(expected - accual);
		return difference <= adjustment;
	}

}
