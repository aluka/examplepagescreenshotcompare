package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import se.com.WebMachine.Tools.Driver;

public class Table {
	private static final String pageContentXpath = "//*[@class='main']";
	private static By table = By.tagName("table");
	private static String tableContainerXpath = pageContentXpath + "//*[@class='generic-text']";
	private static String search = tableContainerXpath + "//*[@class='dataTables_filter']";
	private static String SeachInput = search + "//input";
	private static String tableXpath = tableContainerXpath + "//table";
	private static String tableRowXpath = tableXpath + "/tbody/tr";
	private static String tableDataBoxXpath = tableXpath + "//td";
	private static String table1stRow = tableXpath + "/thead/tr/th";

	public static boolean validateTable(Driver driver) {
		boolean status = true;
		if (driver.isElementAvailable(table)) {
			int noOfTable = noOfTable(driver);
			if (noOfTable > 1) {
				driver.OutputStream("There is " + noOfTable + " tables in this page. Continue Testing.");
				for (int tableNo = 1; tableNo <= noOfTable; tableNo++) {
					selectTable(tableNo);
					status = status & validateFiltering(driver) & validateData(driver) & validateShorting(driver);
					driver.OutputStream("Finish testing table no " + tableNo + ".");
				}
			} else {
				driver.OutputStream("There is only one table in this page. Continue Testing.");
				status = validateFiltering(driver) & validateData(driver) & validateShorting(driver);
			}
			if (status)
				driver.OutputStream("Table Tested. PASSED");
			else
				driver.OutputStream("Table Tested. FAILED", false);
		} else {
			driver.OutputStream("There is no table in this page.", false);
		}
		return status;
	}

	private static int noOfTable(Driver driver) {
		return driver.findElements(table).size();
	}

	private static void selectTable(int no) {
		tableContainerXpath = pageContentXpath + "//*[@class='generic-text']" + "[" + no + "]";
		search = tableContainerXpath + "//*[@class='dataTables_filter']";
		SeachInput = search + "//input";
		tableXpath = tableContainerXpath + "//table";
		tableRowXpath = tableXpath + "/tbody/tr";
		tableDataBoxXpath = tableXpath + "//td";
		table1stRow = tableXpath + "/thead/tr/th";
	}

	private static boolean validateShorting(Driver driver) {
		boolean status = false;
		int pageOfset = HomePage.getPageOfset(driver);
		if (driver.isElementAvailable("xpath", search, 5)) {
			WebElement nameEle = driver.findElement("Xpath", table1stRow + "[1]", 15);
			driver.scrollToElement(nameEle, pageOfset);
			String dataBefore = driver.findElements("Xpath", tableDataBoxXpath).get(0).getText();
			driver.click(nameEle);
			driver.wait(200);
			String dataAfter = driver.findElements("Xpath", tableDataBoxXpath).get(0).getText();
			if (dataBefore.equals(dataAfter)) {
				driver.click(nameEle);
				driver.wait(500);
				dataAfter = driver.findElements("Xpath", tableDataBoxXpath).get(0).getText();
				if (dataBefore.equals(dataAfter)) {
					status = false;
					driver.OutputStream(
							"data before shorting was: " + dataBefore + ", and after shorting is: " + dataAfter + ".");
					driver.OutputStream("Table shorting functionality test FAILED", false);
				} else {
					status = true;
					driver.OutputStream("Table Shorting funcitionality is tested and varrified.");
				}
			} else {
				status = true;
				driver.OutputStream("Table Shorting funcitionality is tested and varrified.");
			}
		}
		return status;
	}

	private static boolean validateData(Driver driver) {
		boolean status = true;
		List<WebElement> dataSet = driver.findElements("Xpath", tableDataBoxXpath);
		for (WebElement data : dataSet) {
			status = status && data.getText().length() > 0;
		}
		if (status)
			driver.OutputStream("All the boxes in the table contains value.");
		else
			driver.OutputStream("Some of the boxes in the talbe does not contain value", false);
		return status;
	}

	private static boolean validateFiltering(Driver driver) {
		boolean status = false;
		if (driver.isElementAvailable("xpath", search, 5)) {
			int beforeCount = getDataCount(driver);
			// Considering there is some people in this list name start with
			// 'Ang'
			driver.findElement("xpath", SeachInput, 5).sendKeys("Ang");
			driver.wait(100);
			int afterCount = getDataCount(driver);
			status = beforeCount >= afterCount;
			if (status)
				driver.OutputStream("table filter functionality varrified.");
			else
				driver.OutputStream("Table filtering functionality test Failed.");
			driver.navigate().refresh();
			driver.wait(100);
			driver.waitToLoad("Schneider");
			driver.wait(100);
			driver.waitToLoad();
		} else {
			driver.OutputStream("There is no search box for filtering. Failed test", false);
		}
		return status;
	}

	private static int getDataCount(Driver driver) {
		int result = 0;
		result = driver.findElements("Xpath", tableRowXpath).size();
		return result;
	}

}
