package se.com.WebMachine.Pages;

import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

import se.com.WebMachine.Tools.Driver;

public class Press {

	private static final String pageContentXpath = "//*[@class='main']";
	private static final String tabBarXpath = pageContentXpath + "//div[@class='tab-bar']";
	private static final String tabbarList = tabBarXpath + "/ul/li";
	private static final String perspectiveXpath = pageContentXpath + "//section[@class='perspectives']";
	private static final String perspectiveFilterXpath = perspectiveXpath
			+ "//div[contains(@class,'selectize-control')]";
	private static final String perspectiveFilterSelectXpath = perspectiveFilterXpath
			+ "//div[contains(@class,'selectize-input')]";
	private static final String perspectiveFilterContentXpath = perspectiveFilterXpath
			+ "//div[@class='selectize-dropdown-content']/div";
	private static final String ContactPressXpath = pageContentXpath + "//div[@class='timeline']/div";
	private static final String ContactFormXpath = pageContentXpath + "//div[@class='customer-contact-form']";
	private static String slideContainerXpath = pageContentXpath + "//div[@class='simple-slider']";
	private static String contentSlideListXpath = slideContainerXpath + "//*[@class='slides']/li";
	private static String contentSlideControllerListXpath = slideContainerXpath + "//*[@class='pagination']/li";

	public static boolean validatePress(Driver driver) {
		boolean status = validatePressContent(driver) & validateLibraryContent(driver) & validateCalenderContent(driver)
				& validateContactContent(driver);
		if (status)
			driver.OutputStream("'Corporate Press' functionality and content tested and PASSED");
		else
			driver.OutputStream("'Corporate Press' functionality and content tested and FAILED.", false);
		return status;
	}

	public static boolean validatePressContent(Driver driver) {
		String url = driver.getCurrentUrl();
		boolean status = validateTab(driver) & validatePerspective(driver) & validateSlide(driver);
		driver.get(url);
		driver.scrollTop();
		return status;
	}

	public static boolean validateLibraryContent(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoLibrary(driver)) {
			boolean status = validatePerspective(driver) & validateSlide(driver);
			driver.get(url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'Press-library'. url: " + url);
			return true;
		}
	}

	public static boolean validateCalenderContent(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoCalender(driver)) {
			boolean status = Table.validateTable(driver);
			driver.get(url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'Press-Calender'. url: " + url);
			return true;
		}
	}

	public static boolean validateContactContent(Driver driver) {
		String url = driver.getCurrentUrl();
		if (gotoContacts(driver)) {
			boolean status = validateTimeline(driver) & validateForm(driver);
			driver.get(url);
			driver.scrollTop();
			return status;
		} else {
			driver.OutputStream("Could not navigate to 'Press-Contacts'. url: " + url);
			return true;
		}
	}

	private static boolean gotoLibrary(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 2) && isTabContentChanged(driver, url);
	}

	private static boolean gotoCalender(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 3) && isTabContentChanged(driver, url);
	}

	private static boolean gotoContacts(Driver driver) {
		String url = driver.getCurrentUrl();
		return clickTabNo(driver, 4) && isTabContentChanged(driver, url);
	}

	private static boolean validateTimeline(Driver driver) {
		if (isContactPressAvailable(driver)) {
			boolean status = validateContactPressData(driver);
			if (status)
				driver.OutputStream("Contact Sales data is validated. Test PASSED.");
			else
				driver.OutputStream("Contact sales data is not validated. Test FAILED.", false);
			return status;
		} else {
			driver.OutputStream("'Press-Contact' is not available in this page." + driver.getCurrentUrl(), false);
			return true;
		}
	}

	private static boolean validateForm(Driver driver) {
		if (isFormVisible(driver)) {
			boolean status = validateFormInfo(driver);
			if (status)
				driver.OutputStream("Form structure is validated. Test PASSED.");
			else
				driver.OutputStream("Form structure is not validated. Test FAILED.");
			return status;
		} else {
			driver.OutputStream("Form is not visible in this page. Skip Testing form.");
			return true;
		}
	}

	private static boolean isFormVisible(Driver driver) {
		Dimension size = driver.findElement("Xpath", ContactFormXpath, 5).getSize();
		return size.height > 0 && size.width > 0;
	}

	private static boolean validateFormInfo(Driver driver) {
		/*
		 * one title <h2> //h2 ---- condition text-Length pera one with intro
		 * <p> //p[not(@class)] ---- condition visible and text lenght form with
		 * //form[@id='customer-contact'] ---- available inside form submit
		 * button /input[@type='submit'] ---- visible size
		 */
		return driver.findElement("Xpath", ContactFormXpath + "//h2", 5).getText().length() > 0
				&& driver.isElementVissible("Xpath", ContactFormXpath + "//p[not(@class)]", 2)
				&& driver.findElement("Xpath", ContactFormXpath + "//p[not(@class)]", 2).getText().length() > 0
				&& driver.isElementVissible("Xpath", ContactFormXpath + "//form[@id='customer-contact']", 2)
				&& driver.isElementVissible("Xpath",
						ContactFormXpath + "//form[@id='customer-contact']/input[@type='submit']", 2);
	}

	private static boolean isContactPressAvailable(Driver driver) {
		return driver.isElementAvailable("Xpath", ContactPressXpath, 5);
	}

	private static boolean validateContactPressData(Driver driver) {
		return driver.findElement("Xpath", ContactPressXpath + "//h2", 5).getText().length() > 1
				& validateContacts(driver);
	}

	private static boolean validateContacts(Driver driver) {
		boolean status = false;
		List<WebElement> contacts = driver.findElements("Xpath", ContactPressXpath + "//p");
		if (contacts.size() > 0) {
			driver.OutputStream("There is " + contacts.size() + " contact information in this page. Continue Testing");
			status = true;
		} else {
			driver.OutputStream("There is no contact info in this page. Skip testing.", false);
			return false;
		}
		for (WebElement contact : contacts) {
			status = status & contact.getText().length() > 0;
			driver.OutputStream("Contact info" + contact.getText() + ".");
		}
		return status;
	}

	private static boolean validatePerspective(Driver driver) {
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		if (driver.isElementAvailable("Xpath", perspectiveXpath, 5)) {
			boolean status = validatePerspectivesColumn(driver) & validateLoadMore(driver)
					& validatePerspectiveFilter(driver);
			if (!url.contains(driver.getCurrentUrl())) {
				driver.get(url, title);
			}
			return status;
		} else {
			driver.OutputStream("There is no perspective in this page. Skip Test.");
			return false;
		}
	}

	private static boolean validatePerspectiveFilter(Driver driver) {
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		if (driver.isElementAvailable("Xpath", perspectiveFilterXpath, 2)) {
			driver.OutputStream("There is perspective filter in this page. Continue Testing.");
			int noOfPerspectiveBefore = getNumOfPerspectiveArticles(driver);
			boolean status = changePerspectiveFilter(driver);
			int noOfPerspectiveAfter = getNumOfPerspectiveArticles(driver);
			if (!url.contains(driver.getCurrentUrl())) {
				driver.get(url, title);
			}
			status = status & (noOfPerspectiveBefore != noOfPerspectiveAfter);
			if (status) {
				driver.OutputStream("Perspective filter tested. Test PASSED");
				return true;
			}
			driver.OutputStream("Perspective Filter tested but FAILED.");
			return false;
		} else {
			driver.OutputStream("There is no perspective filter in this page. Continue Testing");
			return true;
		}
	}

	private static boolean changePerspectiveFilter(Driver driver) {
		int pageOfset = HomePage.getPageOfset(driver);
		WebElement filterSelector = driver.findElement("Xpath", perspectiveFilterSelectXpath, 10);
		driver.scrollToElement(filterSelector, pageOfset);
		driver.click(filterSelector);
		driver.wait(100);
		driver.waitToLoad();
		int noOfFilterValues = driver.findElements("Xpath", perspectiveFilterContentXpath).size();
		if (!(noOfFilterValues > 1)) {
			driver.OutputStream("No perspective filter shown. Skip testing. Test Failed", false);
			driver.navigate().refresh();
			driver.wait(100);
			driver.waitToLoad();
			return false;
		}
		int valueNo = 0;
		if (noOfFilterValues > 2)
			valueNo = noOfFilterValues - 2;
		else
			valueNo = noOfFilterValues - 1;
		String expectedValue = driver.findElement("Xpath", perspectiveFilterContentXpath + "[" + valueNo + "]", 2)
				.getText();
		driver.click("Xpath", perspectiveFilterContentXpath + "[" + valueNo + "]");
		driver.wait(200);
		driver.waitToLoad();
		String accual = driver.findElement("Xpath", perspectiveFilterSelectXpath + "/div", 10)
				.getAttribute("data-value");
		if (accual.contains(expectedValue)) {
			driver.OutputStream("Year " + accual + " is selected from perspective filter.");
			return true;
		}
		return false;
	}

	private static boolean validateLoadMore(Driver driver) {
		if (!driver.isElementAvailable("Xpath", perspectiveXpath + "/*[@class='load']", 2)) {
			driver.OutputStream("There is no 'Load More' button in this page. Skip Testing.");
			return true;
		}
		int numberOfArticlesBefore = getNumOfPerspectiveArticles(driver);
		clickLoadMore(driver);
		int numberOfArticlesAfter = getNumOfPerspectiveArticles(driver);
		if (numberOfArticlesBefore != numberOfArticlesAfter) {
			driver.OutputStream("Perspective articles load more test PASSED");
			return loadMoreJSError(driver);
		} else {
			driver.OutputStream("Perspective articles load more test FAILED", false);
			return false;
		}
	}

	private static int getNumOfPerspectiveArticles(Driver driver) {
		return driver.findElements("Xpath", perspectiveXpath + "//article").size();
	}

	private static boolean loadMoreJSError(Driver driver) {
		boolean status = true;
		LogEntries logEntries = null;
		try {
			logEntries = driver.manage().logs().get(LogType.BROWSER);
			for (LogEntry entry : logEntries) {
				if (entry.getMessage().contains("PERSPECTIVE_WIDGET")) {
					driver.OutputStream(entry.getLevel() + " " + entry.getMessage(), false);
					status = false;
				}
			}
			logEntries.getAll().clear();
		} catch (Exception e) {
		}
		return status;
	}

	private static void clickLoadMore(Driver driver) {
		driver.click("Xpath", perspectiveXpath + "/*[@class='load']");
		driver.wait(100);
		driver.waitToLoad();
	}

	private static boolean validatePerspectivesColumn(Driver driver) {
		int size = driver.findElements("Xpath", perspectiveXpath + "/div[@class='column-article']").size();
		if (size == 2) {
			driver.OutputStream("There is 2 column in perspectives. As expected. Test PASSED.");
			return true;
		} else {
			driver.OutputStream("There is " + size + " column in perspectives. Expected is 2. Test FAILED", false);
			return false;
		}
	}

	private static boolean validateTab(Driver driver) {
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		if (isTabAvailable(driver)) {
			boolean status = true;
			int noOfTab = getNoOfTab(driver);
			driver.OutputStream("There is " + noOfTab + " tabs in this page. Continue testing.");
			for (int tabNo = 2; tabNo <= noOfTab; tabNo++) {
				String tabURL = driver.getCurrentUrl();
				status = status & clickTabNo(driver, tabNo);
				status = status & isTabContentChanged(driver, tabURL);
			}
			if (status)
				driver.OutputStream("Tab functionality tested and varrified. Test PASSED");
			else
				driver.OutputStream("Tab Functionality test FAILED.", false);
			// return to starting page
			driver.get(url);
			driver.wait(100);
			driver.waitToLoad(20, title);
			return status;
		} else {
			driver.OutputStream("There is no tab in this page.");
			return true;
		}
	}

	private static boolean isTabContentChanged(Driver driver, String oldUrl) {
		if (!driver.getCurrentUrl().equals(oldUrl)) {
			driver.OutputStream("Page URL changed. Tab Content changed and varrified.");
			return true;
		} else {
			driver.OutputStream("page URL did not change. Tab content is not varrified.");
			return false;
		}
	}

	private static boolean clickTabNo(Driver driver, int tabNo) {
		WebElement tab = driver.findElement("Xpath", tabbarList + "[" + tabNo + "]", 20);
		if (tab == null) {
			driver.OutputStream(
					"Tab no " + tabNo + " is not available. Element Identifier: " + tabbarList + "[" + tabNo + "]",
					false);
			// driver.OutputStream("");
			return false;
		}
		driver.click(tab);
		driver.waitToLoad();
		driver.wait(100);
		tab = driver.findElement("Xpath", tabbarList + "[" + tabNo + "]", 20);
		String className = tab.getAttribute("class");
		if (className.contains("selected")) {
			driver.OutputStream(tab.getText() + " is selected.");
			return true;
		} else {
			// driver.wait(100);
			driver.waitToLoad();
			tab = driver.findElement("Xpath", tabbarList + "[" + tabNo + "]", 20);
			className = tab.getAttribute("class");
			if (className.contains("selected")) {
				driver.OutputStream(tab.getText() + " is selected.");
				return true;
			} else {
				driver.OutputStream(tab.getText() + " is selected.");
				return false;
			}
		}
	}

	private static boolean isTabAvailable(Driver driver) {
		if (driver.isElementAvailable("Xpath", tabBarXpath, 5))
			return driver.findElements("Xpath", tabbarList).size() > 1;
		return false;
	}

	private static int getNoOfTab(Driver driver) {
		return driver.findElements("Xpath", tabbarList).size() - 1;
	}

	private static boolean validateSlide(Driver driver) {
		String locator = "Xpath";
		boolean status = true;
		int noOfSlides, noOfControllers;
		if (driver.isElementAvailable(locator, contentSlideListXpath, 2)) {
			int NoOfSlideContainer = getSlideContainerNo(driver);
			if (NoOfSlideContainer > 1) {
				driver.OutputStream(
						"There is " + NoOfSlideContainer + " slide container in this page. Continue testing.");
				for (int containerNo = 1; containerNo <= NoOfSlideContainer; containerNo++) {
					selectSlideContainerNo(containerNo);
					noOfSlides = driver.findElements(locator, contentSlideListXpath).size();
					noOfControllers = driver.findElements(locator, contentSlideControllerListXpath).size();
					driver.OutputStream("There is " + noOfSlides + " slides in this page. Continue with testing.");
					status = (noOfSlides > 0);
					if (noOfSlides > 1)
						status = status & (noOfSlides == noOfControllers);
					for (int slideNo = 1; slideNo <= noOfSlides; slideNo++)
						status = status & validateContentSlideNo(driver, slideNo);
					driver.OutputStream("Slide container no " + containerNo + " is Tested.");
				}
			} else {
				noOfSlides = driver.findElements(locator, contentSlideListXpath).size();
				noOfControllers = driver.findElements(locator, contentSlideControllerListXpath).size();
				driver.OutputStream("There is " + noOfSlides + " slides in this page. Continue with testing.");
				status = (noOfSlides > 0);
				if (noOfSlides > 1)
					status = status & (noOfSlides == noOfControllers);
				for (int slideNo = 1; slideNo <= noOfSlides; slideNo++)
					status = status & validateContentSlideNo(driver, slideNo);
			}
		}
		return status;
	}

	private static int getSlideContainerNo(Driver driver) {
		return driver.findElements("Xpath", slideContainerXpath).size();
	}

	private static void selectSlideContainerNo(int slideNo) {
		slideContainerXpath = pageContentXpath + "//div[@class='simple-slider']" + "[" + slideNo + "]";
		contentSlideListXpath = slideContainerXpath + "//*[@class='slides']/li";
		contentSlideControllerListXpath = slideContainerXpath + "//*[@class='pagination']/li";
	}

	private static boolean validateContentSlideNo(Driver driver, int slideNo) {
		/*
		 * Check for 4 conditions title contains text peragraph contains text
		 * one link is vissible one image is vissible
		 */
		int pageOfset = HomePage.getPageOfset(driver);
		String locator = "Xpath";
		boolean status = false;
		driver.OutputStream("Start Testing slide no " + slideNo);
		try {
			WebElement controller = driver.findElement(locator, contentSlideControllerListXpath + "[" + slideNo + "]",
					2);
			driver.scrollToElement(controller, pageOfset + 15);
			if (driver.isElementAvailable(locator, contentSlideControllerListXpath + "[" + slideNo + "]", 2))
				clickOnSlideController(driver, slideNo);
			status = driver.findElement(locator, contentSlideListXpath + "[" + slideNo + "]//*[@class='title']", 4)
					.getText().length() > 0
					& driver.findElement(locator, contentSlideListXpath + "[" + slideNo + "]//p", 1).getText()
							.length() > 0
					& driver.isElementAvailable(locator, contentSlideListXpath + "[" + slideNo + "]//a", 1);
			// & driver.isElementAvailable(driver, locator,
			// contentSlideListXpath+"["+slideNo+"]//img", 1);
			if (status)
				driver.OutputStream("PASSED");
			else
				driver.OutputStream("Slide no " + slideNo + " test FAILED", false);

		} catch (Exception e) {
			driver.OutputStream("Exception Triggered. During testing slide no " + slideNo);
		}
		return status;
	}

	private static boolean clickOnSlideController(Driver driver, int slideNo) {
		// driver.click(driver, "Xpath",
		// contentSlideControllerListXpath+"["+slideNo+"]");
		WebElement tab = driver.findElement("Xpath", contentSlideControllerListXpath + "[" + slideNo + "]", 5);
		if (tab == null) {
			driver.OutputStream("Slide controller no " + slideNo + " is not available. Element Identifier: "
					+ contentSlideControllerListXpath + "[" + slideNo + "]", false);
			return false;
		}
		driver.click(tab);
		driver.waitToLoad();
		driver.wait(100);
		tab = driver.findElement("Xpath", contentSlideControllerListXpath + "[" + slideNo + "]", 20);
		String className = tab.getAttribute("class");
		if (className.contains("selected")) {
			driver.OutputStream(tab.getText() + " is selected.");
			return true;
		} else {
			driver.waitToLoad();
			tab = driver.findElement("Xpath", contentSlideControllerListXpath + "[" + slideNo + "]", 20);
			className = tab.getAttribute("class");
			if (className.contains("current")) {
				driver.OutputStream(tab.getText() + " is selected.");
				return true;
			} else {
				driver.OutputStream(tab.getText() + " is selected.");
				return false;
			}
		}

	}

}
