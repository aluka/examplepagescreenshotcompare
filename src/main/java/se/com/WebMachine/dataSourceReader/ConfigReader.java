package se.com.WebMachine.dataSourceReader;

import java.io.FileInputStream;

import java.util.Properties;

/**
 * @author Hossain.
 * Read Properties file named "\\src\\test\\java\\se\\com\\WebMachine\\configFile\\" 
 */

public class ConfigReader {
	private static String fileName = "headerTest";

	/**Return String value of the property name in the properties file.*/
	public static String getPropValue(String FileName, String propName){
		fileName = FileName;
		return getPropValue(propName);
	}

	/**Return String value of the property name in the "headerTest" properties file.*/
	public static String getPropValue(String propName) {
		String value = null;
		FileInputStream inputStream;
		try {
			Properties prop = new Properties();
			String propFileName = System.getProperty("user.dir")
					+ "\\src\\test\\java\\se\\com\\WebMachine\\configFile\\"+fileName+".properties";
			inputStream = new FileInputStream(propFileName);
			prop.load(inputStream);
			value = prop.getProperty(propName);
			// System.out.println( propName+" : "+Driver.logTime()+", "+value +
			// " is been read.");
			inputStream.close();
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
		return value;
	}
}