package se.com.WebMachine.dataSourceReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * @author Hossain.
 * Read csv files in folder "\\testData\\dataSource\\"
 */
public class CSVReader {
	private static String fileName = "home-Link-Link";
	private static String folderPath = System.getProperty("user.dir") + "\\testData\\dataSource\\";
	private static String File = folderPath + fileName + ".csv";

/*	public static void main(String[] args) {
		System.out.println("There is " + numberOfRaw() + " sets of data in that file.");
		System.out.println("Each data set has " + numberOfData() + " data.");
		System.out.println("2nd data of 4th data set is: " + readData(4, 2));
	}*/
	/**return string data in raw and calumn of the specific filename
	 * */
	public static String readData(String filename, int raw, int column) {
		File = folderPath + filename + ".csv";
		return readData(raw, column);
	}
	/**return string data in raw and calumn of filename home-Link-Link.csv
	 * */
	public static String readData(int raw, int column) {
		String data = "";
		String line = "";
		String cvsSplitBy = ",";
		int rawCount = 1, colCount = 1;
		try (BufferedReader br = new BufferedReader(new FileReader(File))) {
			while ((line = br.readLine()) != null) {
				if (rawCount == raw) {
					String[] country = line.split(cvsSplitBy);
					if (country[colCount] != null)
						data = country[colCount];
				}
				rawCount++;
			}
		} catch (Exception e) {
		}
		return data;
	}
	/**Return int value of number of Raw in the file
	 * */
	public static int numberOfRaw(String filename) {
		File = folderPath + filename + ".csv";
		return numberOfRaw();
	}
	/**Return int value of number of Raw in the file name home-link-link.csv
	 * */
	public static int numberOfRaw() {
		int numberOfRaw = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(File))) {
			while ((br.readLine()) != null) {
				numberOfRaw++;
			}
		} catch (IOException e) {
		}
		return numberOfRaw;
	}
	/**Return int value of number of data in the Raw 1 in the file
	 * */
	public static int numberOfData(String filename) {
		File = folderPath + filename + ".csv";
		return numberOfData();
	}
	/**Return int value of number of data in the Raw 1 in the file name home-link-link.csv
	 * */
	public static int numberOfData() {
		String line = "";
		String cvsSplitBy = ",";
		int count = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(File))) {
			while ((line = br.readLine()) != null) {
				String[] country = line.split(cvsSplitBy);
				if (country[count] != null)
					count++;
				else
					return count;
			}
		} catch (Exception e) {
		}
		return count;
	}
}
