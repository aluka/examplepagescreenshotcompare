package se.com.WebMachine.Util;

import java.util.Hashtable;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
/** @author Hossain
 * this is a custom testng reporter that use extent reporter tool to
 * genarate html report to use this class as reporter tool just
 */
public class ExtentReporter implements ITestListener {
	private static String outputDirectory = System.getProperty("user.dir") + "\\target\\Extent-Report\\";
	protected static Hashtable<String, String> sysInfo = new Hashtable<String, String>();
	private static ExtentReports reporter = new ExtentReports(outputDirectory + "ExtentReport.html");;
	private static ExtentTest test;
	private static String description = "";

	/** Called when the test-method execution starts*/
	@Override
	public void onTestStart(ITestResult result) {
		if(result == null){
			return;
		}
		String testName = result.getName();
		if (reporter == null || testName == null) {
			System.out.println("Reporter is not cofigured properly ..... Test Start Error");
			return;
		}
		test = reporter.startTest(testName);
		test.assignAuthor("Hossain-QA-US");
		test.assignCategory("Regression");
		int peremeterNo = 0;
		for (Object perameter : result.getParameters()) {
			peremeterNo++;
			description = description + "Perameter no " + peremeterNo + ": " + perameter.toString() + "<p>";
		}
	}

	/** Called when the test-method execution is a success*/
	@Override
	public void onTestSuccess(ITestResult result) {
		if(result == null){
			return;
		}
		printReport(result);
		if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(LogStatus.PASS, result.getName() + " PASSED");
		}
	}

	/**Called when the test-method execution fails*/
	@Override
	public void onTestFailure(ITestResult result) {
		if(result == null){
			return;
		}
		printReport(result);
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(LogStatus.FAIL, result.getName() + " FAILED");
		}
	}

	/**Called when the test-method is skipped*/
	@Override
	public void onTestSkipped(ITestResult result) {
		if(result == null){
			return;
		}
		 if(result.getStatus()==ITestResult.SKIP){ test.log(LogStatus.SKIP,
		 result.getName()+" SKIPPED"); }
	}

	/** Called when the test-method fails within success percentage*/
	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		if(result == null){
			return;
		}
		// Leaving blank

	}

	/**Called when the test in xml suite starts*/
	@Override
	public void onStart(ITestContext context) {
		if(context == null){
			return;
		}
		/*Left Blank*/
	}

	/** Called when the test in xml suite finishes*/
	@Override
	public void onFinish(ITestContext context) {
		/* Craete the report*/
		if (reporter == null) {
			System.out.println("Reporter is not cofigured properly ..... Report End Error");
			return;
		}
		sysInfo.put("Author", "Hossain-QA-US");
		reporter.addSystemInfo(sysInfo);
		reporter.flush();
	}

	private static void printReport(ITestResult result) {
		/* Store test description */
		if (description.length() > 2)
			test.setDescription(description);
		else
			test.setDescription("This test is not perameterised.");
		description = "";
		if(result == null){
			return;
		}
		for (String logData : Reporter.getOutput(result)) {
			test.log(LogStatus.INFO, logData);
		}
	}

	public static void addTestDescription(String info) {
		description = description + info + "<p>";
	}

	public static void setupBrowser(String browser) {
		if (browser == null) {
			System.out.println("Reporter is not cofigured properly ..... Test Start Error");
			return;
		}
		sysInfo.put("Browser", browser);
	}

	public static void defineSystemInfo(String name, String value) {
		if (name == null || value == null) {
			System.out.println("Reporter is not cofigured properly ..... Test Start Error");
			return;
		}
		sysInfo.put(name, value);
	}
}
